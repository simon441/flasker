
from logging import getLogger
from operator import and_
from typing import Dict

import flask_sqlalchemy
import sqlalchemy
from flask import (Request, flash, json, redirect, render_template, request,
                   url_for)
from flask_babelplus import gettext
from flask_login import current_user, login_required

from ..app import app, db
from ..forms import PostForm, SearchForm
from ..models import Posts
from ..models.models import Users
from ..utils.external import Roles
from ..utils.functions import get_meta_for_form, transform_content


@app.route('/add-post', methods=['GET', 'POST'])
@login_required
def add_post():
    """ Add Post Page """

    form = PostForm(meta=get_meta_for_form())

    if form.validate_on_submit():
        poster = current_user.id
        post = Posts(
            title=form.title.data, content=form.content.data, poster_id=poster, slug=form.slug.data)
        #  Clear the form
        form.title.data = ''
        form.content.data = ''
        form.slug.data = ''

        # Add post data to the database
        db.session.add(post)
        db.session.commit()

        flash(gettext('Blog Post Submitted Successfully!'), 'success')
        return redirect(url_for('post', id=post.id))
        # return redirect('posts')
    elif request.method == 'POST' and form.is_submitted():
        flash(gettext(
            'There are errors in the form. Please correct them before submitting again.'), 'danger')

    # Redirect to the webpage
    return render_template('add_post.html', form=form)


@app.route('/posts')
def posts():
    """ List of Blog Posts """

    logger = getLogger()
    # Get current page
    page = request.args.get('page', 1, type=int)
    # Get current date_posted
    date_posted_input = request.args.get('cur', None, type=str)
    direction = request.args.get('cur_page', 'asc', type=str)

    # Offset value
    offset_value = (page - 1) * app.config['ROWS_PER_PAGE']

    # Get date and id when offset is > 0
    print('offset_value', offset_value)
    skipped_page = []
    if offset_value > 0:
        skipped_paged_query = Posts.query.with_entities(Posts.date_posted, Posts.id).order_by(
            Posts.date_posted, Posts.id).limit(1).offset(offset_value)
        logger.info(str(skipped_paged_query))
        skipped_page = skipped_paged_query.all()[0]
        logger.debug(skipped_page[0])

    posts_filtered_2: sqlalchemy.orm.Query = Posts.query
    if offset_value > 0:
        posts_filtered_2 = posts_filtered_2.filter(
            Posts.date_posted >= skipped_page[0])
        # posts_filtered_2 = posts_filtered_2.filter(and_(Posts.date_posted > skipped_page[0], not_(and_(
        #     Posts.date_posted == skipped_page[0], Posts.id < skipped_page[1]))))
        print('offsetttt')
        print(posts_filtered_2)
    posts_filtered_2 = posts_filtered_2.options(
        sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
    ).order_by(Posts.date_posted.desc(), Posts.id).limit(
        app.config['ROWS_PER_PAGE'])
    print('posts_filtered_2')
    # logger.info(posts_filtered_2)
    # logger.info(posts_filtered_2.all())

    # Grab all the posts from the database where the date is equal or greater than the current and limit the results
    # posts_filtered: sqlalchemy.orm.Query = Posts.query
    # if offset_value > 0 or date_posted_input is not None:
    #     # print("--------------------------------")
    #     # print(date_posted_input)
    #     # print("----------------------")
    #     if direction == 'asc':
    #         posts_filtered = posts_filtered.filter(
    #             Posts.date_posted > date_posted_input)
    #     else:
    #         posts_filtered = posts_filtered.filter(
    #             Posts.date_posted <= date_posted_input)

    # # Load Users data (id and name) then ORDER BY, LIMIT, OFFSET and get all results as a Python list
    # posts_filtered = posts_filtered.options(
    #     sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
    # ).order_by(Posts.date_posted, Posts.id).limit(
    #     app.config['ROWS_PER_PAGE'])
    # if offset_value > 0 and date_posted_input is None:
    #     posts_filtered = posts_filtered.offset(
    #         (page - 1) * app.config['ROWS_PER_PAGE'])
    # logger.debug(posts_filtered)
    # posts = posts_filtered.all()

    posts = posts_filtered_2.all()

    # Total number of Posts
    from sqlalchemy import func
    t = db.session.query(func.count(Posts.id))
    total = t.first()[0]

    next_post: Posts = posts[-1] if len(posts) > 0 else Posts()

    logger.info(posts_filtered_2)
    logger.info(posts)
    logger.info(next_post)
    logger.info(next_post.date_posted)
    logger.info(total)
    logger.info(t)

    # Pagination
    posts = flask_sqlalchemy.Pagination(query=None,
                                        page=page, per_page=app.config['ROWS_PER_PAGE'], total=total, items=posts)

    # Last updated date in the list of filtered Posts
    date_posted_input = next_post.date_posted

    return render_template('posts.html', posts=posts, date_posted_input=date_posted_input)


@app.route('/posts/<int:id>')
def post(id: int):
    """ Show a Post """
    # Options
    the_post = Posts.query.options(
        sqlalchemy.orm.joinedload(Posts.poster, innerjoin=True)
        .load_only(Users.id, Users.about_author, Users.date_added, Users.email, Users.name, Users.profile_picture, Users.role)
    )
    # Get the Post if it exists
    post = the_post.get_or_404(id)
    return render_template('post.html', post=post, prev_page=request.args.get('cur', None, type=str))


@app.route('/posts/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_post(id: int):
    post: Posts = Posts.query.get_or_404(id)
    form = PostForm(meta=get_meta_for_form())
    # If current user is the one who create this post is current user is an admin
    if current_user.id == post.poster_id or Users.query.get(current_user.id).role == Roles.admin.value:
        # Method is POST and form is valid
        if form.validate_on_submit():
            post.title = form.title.data
            post.content = form.content.data
            post.slug = form.slug.data

            # Update database
            db.session.add(post)
            db.session.commit()
            flash(gettext('Post Has Been Updated!'), category='success')
            return redirect(url_for('post', id=post.id))
        # errors in form
        elif form.is_submitted():
            flash(gettext(
                'There are errors in the form. Please correct them before submitting again.'), 'danger')
        # request is GET
        else:
            form.title.data = post.title
            form.content.data = post.content
            form.slug.data = post.slug

        return render_template('edit_post.html', form=form)

    else:
        flash(gettext("You aren't authorized to edit this post..."), 'danger')
        posts = Posts.query.order_by(Posts.date_posted)
        return redirect(url_for('posts'))


@app.route('/posts/delete/<int:id>', methods=['POST'])
@login_required
def delete_post(id):
    """ Delete a Post """
    post_to_delete = Posts.query.get_or_404(id)
    user_id = current_user.id

    user: Users = Users.query.get(user_id)
    # Current user created the post
    if user_id == post_to_delete.poster.id or user.role == Roles.admin.value:
        try:
            db.session.delete(post_to_delete)
            db.session.commit()

            # Return a message
            flash(gettext('Blog Post Has Been Deleted!'), category='success')
            return redirect(url_for('posts'))
        except Exception:
            flash(gettext('Whoops! There was a problem deleting post, try again...'),
                  category='danger')
            # Grab all the posts from the database
            posts = Posts.query.order_by(Posts.date_posted)
            return render_template('posts.html', posts=posts)
    else:
        # Return a message
        flash(gettext('You are not authorized to delete this post!'),
              category='danger')
        posts = Posts.query.order_by(Posts.date_posted)
        return redirect(url_for('posts'))


@app.route('/search', methods=['GET', 'POST'])
def search():
    """Search for a post"""
    form = SearchForm(meta=get_meta_for_form())
    posts = Posts.query
    if form.validate_on_submit() or request.args.get('page', 1, type=int) > 0:
        # Get the submitted search in the q field
        post = Posts()
        if form.validate_on_submit():
            post.q = form.q.data
        else:
            post.q = request.args.get('q', '', type=str)
        # Query the database
        logger = getLogger()
        # Get current page
        page = request.args.get('page', 1, type=int)
        # Get current date_posted
        date_posted_input = request.args.get('cur', None, type=str)
        direction = request.args.get('cur_page', 'asc', type=str)

        # Grab all the posts from the database where the date is equal or greater than the current and limit the results
        posts_filtered: sqlalchemy.orm.Query = Posts.query
        posts_filtered = Posts.query.filter(
            Posts.content.ilike('%' + post.q + '%'))
        logger.debug(posts_filtered)

        # if date_posted_input is not None:
        #     # print("--------------------------------")
        #     # print(date_posted_input)
        #     # print("----------------------")
        #     if direction == 'asc':
        #         posts_filtered = posts_filtered.filter(
        #             Posts.date_posted > date_posted_input)
        #     else:
        #         posts_filtered = posts_filtered.filter(
        #             Posts.date_posted <= date_posted_input)

        # # Load Users data (id and name) then ORDER BY, LIMIT, OFFSET and get all results as a Python list
        # posts_filtered = posts_filtered.options(
        #     sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        # ).order_by(Posts.title).limit(
        #     app.config['ROWS_PER_PAGE'])
        # if date_posted_input is None:
        #     posts_filtered = posts_filtered.offset(
        #         (page - 1) * app.config['ROWS_PER_PAGE'])
        # logger.debug(posts_filtered)

        posts_filtered = posts_filtered.order_by(Posts.title)
        posts_filtered = posts_filtered.limit(
            app.config['ROWS_PER_PAGE'])
        posts_filtered = posts_filtered.offset(
            (page - 1) * app.config['ROWS_PER_PAGE'])

        print(',,,,,,,,,,,,,,,,,,,', posts_filtered)
        posts = posts_filtered.all()

        # Total number of Posts
        from sqlalchemy import func
        t = Posts.query.filter(
            Posts.content.like('%' + post.q + '%')).count()
        total = t  # .first()[0]
        # total = len(posts)

        next_post: Posts = posts[-1] if len(posts) > 0 else Posts()

        logger.info(posts_filtered)
        logger.info(posts)
        logger.info(next_post)
        logger.info(next_post.date_posted)
        logger.info(total)
        logger.info(t)

        # posts = posts.order_by(Posts.title).all()
        # Pagination
        posts = flask_sqlalchemy.Pagination(query=posts_filtered,
                                            page=page, per_page=app.config['ROWS_PER_PAGE'], total=total, items=posts)

        print(" request.args",  request.args)
        print(" request.args.get('ajax')",  request.args.get('ajax'))
        if request.args.get('ajax', 0, type=int) == 1:
            data = []
            for f in posts.items:
                print('f', f, f.poster, type(f.poster), f.poster.name)
                # f.content = transform_content(f.content)
                p: Dict = f.as_json()
                p.update({'poster': dict(id=f.poster.name, name=f.poster.name)})
                data.append(p)
            dic = dict(q=post.q,
                       posts=data,
                       next_page=posts.next_num,
                       has_next=posts.has_next,
                       total=total,
                       date_posted_input=next_post.date_posted
                       )
            return json.dumps(dic)
        return render_template('search.html', form=form, q=post.q, posts=posts, date_posted_input=next_post.date_posted, total=total)
    return render_template('search.html', form=form, q='', posts={})


def get_posts(date_posted_input: str, direction='asc', page: int = 1, additional_query_params: dict = None):
    logger = getLogger()
    # Grab all the posts from the database where the date is equal or greater than the current and limit the results
    posts_filtered: sqlalchemy.orm.Query = Posts.query

    if additional_query_params is not None:
        for i, val in additional_query_params.items():
            posts_filtered = posts_filtered.filter(i, val)

    if date_posted_input is not None:
        # print("--------------------------------")
        # print(date_posted_input)
        # print("----------------------")
        if direction == 'asc':
            posts_filtered = posts_filtered.filter(
                Posts.date_posted > date_posted_input)
        else:
            posts_filtered = posts_filtered.filter(
                Posts.date_posted <= date_posted_input)

    # Load Users data (id and name) then ORDER BY, LIMIT, OFFSET and get all results as a Python list
    posts_filtered = posts_filtered.options(
        sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
    ).order_by(Posts.date_posted).limit(
        app.config['ROWS_PER_PAGE'])
    if date_posted_input is None:
        posts_filtered = posts_filtered.offset(
            (page - 1) * app.config['ROWS_PER_PAGE'])
    logger.debug(posts_filtered)
    posts = posts_filtered.all()

    # Total number of Posts
    from sqlalchemy import func
    t = db.session.query(func.count(Posts.id))
    total = t.first()[0]

    next_post: Posts = posts[-1] if len(posts) > 0 else Posts()

    logger.info(posts_filtered)
    logger.info(posts)
    logger.info(next_post)
    logger.info(next_post.date_posted)
    logger.info(total)
    logger.info(t)
    return dict(posts=posts, total=total, next_post=next_post)
