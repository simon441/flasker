import os
import re
import uuid

from flask import flash, redirect, render_template, request, url_for
from flask_babelplus import gettext
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.datastructures import FileStorage
from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.utils import secure_filename

from ..app import app, db
from ..forms.webforms import LoginForm, PasswordForm, UserForm
from ..models.models import Users
from ..utils import get_meta_for_form
from ..utils.functions import allowed_file, is_admin
from ..utils.jinja_helpers import get_role


@app.route('/test_pwd', methods=['GET', 'POST'])
def test_pwd():
    """ Create Password Test Page """

    email = None
    password_hash = None
    user_to_check = None
    passed = None

    form = PasswordForm(meta=get_meta_for_form())  # form

    # Validate form
    if form.validate_on_submit():
        email = form.email.data
        password_hash = form.password_hash.data

        # Lookup User by Email Address
        user_to_check = Users.query.filter_by(email=email).first()

        # Check password entered against the hashed password
        if user_to_check is not None:
            # remove form data
            form.email.data = ''
            form.password_hash.data = ''
            # check password is ok
            passed = check_password_hash(
                user_to_check.password_hash, password_hash)

            flash(gettext('Form submitted successfully'), 'success')
        else:
            passed = False
            # flash(gettext('No user exists'), 'danger')

    return render_template('test_pwd.html',
                           email=email,
                           password_hash=password_hash,
                           user_to_check=user_to_check,
                           passed=passed,
                           form=form)


@app.route('/user/add', methods=['GET', 'POST'])
def add_user():
    """ Add a user """

    form: UserForm = UserForm(request.form, meta=get_meta_for_form())
    print(request.form)
    name = None
    if form.validate_on_submit():
        # Email must be unique so user must be None if not in db
        user = Users.query.filter_by(email=form.email.data).first()
        if user is None:
            # Hash the password
            hashed_pwd = generate_password_hash(
                form.password_hash.data, "sha256")
            user = Users(name=form.name.data, email=form.email.data,
                         favorite_color=form.favorite_color.data, password_hash=hashed_pwd, username=form.username.data)
            db.session.add(user)
            db.session.commit()

            # Reset form fields
            form.name.data = ""
            form.username.data = ""
            form.email.data = ""
            form.favorite_color.data = ""
            form.password_hash.data = ""
            form.password_confirm.data = ""
            flash(gettext("User Added Successfully"), category="success")
        else:
            flash('User already exists', 'danger')
        name = form.name.data
    elif request.method == 'POST':
        flash('Error', 'danger')

    our_users = Users.query.order_by(Users.date_added).all()

    return render_template("add_user.html",
                           form=form,
                           name=name,
                           our_users=our_users)


@app.route('/update/<int:id>', methods=['GET', 'POST'])
@login_required
def update(id: int):
    """ Update Database Record """

    form = UserForm(meta=get_meta_for_form())
    name_to_update = Users.query.get_or_404(id)
    if request.method == 'POST':
        name_to_update.name = request.form['name']
        name_to_update.email = request.form['email']
        name_to_update.favorite_color = request.form['favorite_color']
        name_to_update.username = request.form['username']
        if request.form['role'] in get_role():
            name_to_update.role = request.form['role']
        try:
            db.session.commit()
            flash(gettext('User Updated Successfully'), category='success')
            return render_template('update.html',
                                   form=form,
                                   name_to_update=name_to_update, id=id)
        except Exception:
            flash(
                gettext('Error! Looks like there was a problem updating... try again!'), category='danger')
            return render_template('update.html',
                                   form=form,
                                   name_to_update=name_to_update, id=id)
    else:
        # Update the user role in the form for the default selected option
        form.role.data = name_to_update.role

        return render_template('update.html',
                               form=form,
                               name_to_update=name_to_update, id=id)


@app.route('/delete/<int:id>')
@login_required
def delete(id: int):
    # If the logged in user is the same as the one who's account to be deleted
    if id == current_user.id:

        users_to_delete = Users.query.get_or_404(id)

        name = None
        form = UserForm(meta=get_meta_for_form())
        our_users = Users.query.order_by(Users.date_added).all()

        try:
            db.session.delete(users_to_delete)
            db.session.commit()
            flash(gettext("User Deleted Successfully!!"), 'success')

            return redirect('/user/add')
        except Exception:
            flash(
                gettext("Whoops! There was a problem deleting user, try again..."), "danger")

            return render_template("add_user.html", form=form, name=name,
                                   our_users=our_users)
    else:
        flash(gettext("Sorry, you can't delete that user"), "danger")

        return redirect(url_for('dashboard'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    """ Login to site"""
    form = LoginForm(meta=get_meta_for_form())

    if form.validate_on_submit():
        user: Users = Users.query.filter_by(
            username=form.username.data).first()
        if user:
            # Check the hash
            if check_password_hash(user.password_hash, form.password.data):
                login_user(user)
                flash(gettext('Login successful!'), 'success')
                return redirect(url_for('dashboard'))
        flash(gettext('Invalid credentials. Try Again'), 'danger')
    return render_template('login.html', form=form)


@app.route('/logout', methods=['POST'])
@login_required
def logout():
    """ Logout user """
    logout_user()
    flash(gettext('You have been logged out!\nThanks for stopping by...'), 'success')
    return redirect(url_for('login'))


@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    """ User dashboard """

    form = UserForm(meta=get_meta_for_form())
    id = current_user.id
    name_to_update: Users = Users.query.get_or_404(id)

    if request.method == 'POST':
        try:
            name_to_update.name = request.form['name']
            name_to_update.email = request.form['email']
            name_to_update.favorite_color = request.form['favorite_color']
            name_to_update.username = request.form['username']
            name_to_update.about_author = request.form['about_author']
            file: FileStorage = request.files['profile_picture']
            # File is not empty and has an allowed extension
            if file and allowed_file(file.filename):
                # Grab image name
                filename = secure_filename(file.filename)
                # Set UUID to make filename unique
                filename_unique = f"{uuid.uuid4()}_{filename}"

                # check if file already exists
                regex = r"^([a-z0-9\-]+)_(.*)"

                matches = re.findall(
                    regex, name_to_update.profile_picture, re.MULTILINE)
                # file exists skip
                print("os.path.join(app.config['UPLOAD_FOLDER'], name_to_update.profile_picture)",
                      matches, name_to_update.profile_picture)
                if len(matches) == 0 or matches[1] != filename or name_to_update.profile_picture == 'NULL':
                    if name_to_update.profile_picture == 'NULL':
                        # insert filename to database
                        name_to_update.profile_picture = filename_unique

                        # Save file to disk
                        file.save(os.path.join(
                            app.config['UPLOAD_FOLDER'], filename_unique))

            db.session.commit()
            flash(gettext('User Updated Successfully'), category='success')
            return render_template('dashboard.html',
                                   form=form,
                                   name_to_update=name_to_update, id=id)
        except Exception as e:
            print(e)
            flash(
                gettext('Error! Looks like there was a problem updating... try again!'), category='danger')
            return render_template('dashboard.html',
                                   form=form,
                                   name_to_update=name_to_update, id=id)
    elif request.method == 'GET':
        # Populate textarea form
        form.about_author.data = name_to_update.about_author
    return render_template('dashboard.html',
                           form=form,
                           name_to_update=name_to_update,
                           id=id
                           )


@app.route('/admin')
@login_required
def admin():
    """Admin Page"""
    user_id = current_user.id
    if is_admin(current_user):
        return render_template('admin.html')
    else:
        flash(gettext('Sorry you must be the Admin to access the Admin Page...'), 'danger')
        return redirect(url_for('dashboard'))
