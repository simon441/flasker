import flask
import flask_babelplus
from flask import (flash, redirect, render_template, request, session,
                   sessions, url_for)
from flask_babelplus import gettext, lazy_gettext

from ..app import app, get_locale
from ..forms.webforms import LanguageSelectForm, NamerForm
from ..utils.functions import add_cookie, get_meta_for_form


@app.route('/')  # Create a route decorator
def index():
    """ Home Page """
    stuff = gettext("This is <strong>Bold</strong> Text")
    first_name = "Simon"
    favorite_pizza = ['Pepperoni', gettext('Cheese'), gettext('Mushrooms'), 42]
    return render_template('index.html', first_name=first_name, stuff=stuff, favorite_pizza=favorite_pizza, s=session["lang"])


@app.route('/user/<name>')
def user(name: str):
    """ Show a User by entered name """
    return render_template('user.html', user_name=name)
    # return '<h1>Hello {}!!!</h1>'.format(name.title())


@app.route('/name', methods=['GET', 'POST'])
def name():
    """ Create Name Page """

    name = None
    form = NamerForm(meta=get_meta_for_form())  # form
    # Validate form
    if form.validate_on_submit():
        name = form.name.data
        form.name.data = ''
        flash(gettext('Form submitted successfully'), 'success')

    return render_template('name.html', name=name, form=form)


@app.route('/json')
def get_json():
    """ Data as JSON """
    favorite_pizza = {
        "Jane": "Pepperonni",
        "Mary": lazy_gettext("Cheese"),
        "Stuart": lazy_gettext("Veggie"),
        "Tim": lazy_gettext("Mushroom")
    }
    # return {"Date":  date.today()}
    return favorite_pizza


@app.route('/lang', methods=['GET', 'POST'])
def change_language():
    next = '/'
    if request.method == 'POST':
        form = LanguageSelectForm(meta=get_meta_for_form())
        print(form.language_selection.data, form.language_selection.choices)
        print(request)
        next = form.next.data or '/'
        session['lang'] = form.language_selection.data
        get_locale()
        flask_babelplus.refresh()

        value = {'lang': session['lang']}

        # set the lang to the flasker cookie
        flask.after_this_request(lambda response: add_cookie(response, value))

    print('flask.Request.form', flask.Request.form)
    return redirect(next)
