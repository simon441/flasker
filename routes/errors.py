from flask import render_template

from ..app import app

############# Custom error pages ############


@app.errorhandler(404)
def page_not_found(e):
    # Invalid URL
    print(e)
    return render_template('404.html', e=e), 404


@app.errorhandler(500)
def page_server_error(e):
    # server error page
    print(e)
    return render_template('500.html', e=e), 500


@app.errorhandler(405)
def page_user_error(e):
    # server error page
    print(e)
    return render_template('405.html', e=e), 405
