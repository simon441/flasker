import datetime
import os
import random
from typing import List
from flask_seeder import Faker, Seeder, generator, faker
from faker import Faker
from faker.providers import date_time, lorem, profile, python
from collections import defaultdict
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from slugify import slugify


def sluglify_title(title: str) -> str:
    my_date = datetime.datetime.now()
    print('slugified slug',
          f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}")
    return f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}"


# from ..app import app, db

# import factory
# from ..models import Posts
app = Flask(__name__)
u = True
# Configuration from env.py
if u:
    # app.config['DB_URI'] = 'mysql+pymysql://root:root@localhost/flask_users?charset=utf8mb4'
    app.config['DB_URI'] = 'postgresql://simon:jumopom!@localhost/flask_users'
    # app.config['DB_URI'] = "mssql+pyodbc://?odbc_connect=DRIVER={ODBC Driver 17 for SQL Server};SERVER=(localdb)\MSSQLLocalDB;DATABASE=flask_users;"
    # app.config['DB_URI'] = 'sqlite+pysqlite:///' + \
    # os.path.join(app.root_path, '..', 'db/flasker.sqlite3')

    app.config['SQLALCHEMY_DATABASE_URI'] = app.config['DB_URI']
    if os.environ.get('SQL_DATABASE_ALCHEMY'):
        print("os.environ.get('SQL_DATABASE_ALCHEMY')",
              os.environ.get('SQL_DATABASE_ALCHEMY'))
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
            'SQL_DATABASE_ALCHEMY')

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


session_options = {}
db = SQLAlchemy(app, session_options=session_options)
if app.config['SQLALCHEMY_DATABASE_URI'] == ''.startswith('sqlite'):
    # Batch mode for sqlite3
    migrate = Migrate(app, db, render_as_batch=True)
else:
    migrate = Migrate(app, db, render_as_batch=True)

# # class
# from lib2to3.pgen2.pgen import DFAState
# from ..app import db
# from ..models import Posts


class Posts(db.Model):  # type: ignore
    """ Blog Post Model """
    __tablename__ = 'posts'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    content = db.Column(db.Text)
    # author = db.Column(db.String(255))
    date_posted = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    slug = db.Column(db.String(255), unique=True)
    # Foreign key to links Users (refer to the primary key of the User)
    poster_id = db.Column(db.Integer)

    date_posted_idx = db.Index('idx_date_created', date_posted)

    def __init__(self, *args, **kwargs) -> None:
        if 'title' in kwargs:
            kwargs['slug'] = sluglify_title(kwargs.get('title', ''))
        super().__init__(*args, **kwargs)

    # @sqlalchemy.orm.validates('title')
    # def verify_title(self, key, title: str) -> bool:
    #     if title is None or title == '':
    #         return False
    #     return True


print(db.engine)
# exit(0)

faker = Faker(locale='fr')
posts = Posts.query.filter(
    # Posts.date_posted == '2022-01-20 12:00:00'
    Posts.id > 4
)\
    .all()
# .limit(100
# )\

# lorem.Provider.words(nb=10)
query = ''
faker.random.seed()
fake_data = defaultdict(list)
for i in range(2000):
    query_u = 'UPDATE posts '
    content = ''
    # print(p.content)
    i = 0
    has_words = random.randint(0, 100)
    if has_words > 60:
        words = ['post', 'posts', 'new post',
                 'dolor consectetur elite']
        where = int(has_words/10)
    else:
        words = []
        where = -1
    cnt = random.randint(1, 5)
    # print('cnt', cnt, where)
    done = False
    while i <= cnt:
        content += "<p>"
        if not done and where > 7 and i != 0 and i != cnt-1:
            content += words[random.randint(0, len(words)-1)]
            content += faker.paragraph(nb_sentences=1,
                                       variable_nb_sentences=True)
            content += words[random.randint(0, len(words)-1)]
            content += faker.paragraph(nb_sentences=1,
                                       variable_nb_sentences=True)
            done = True
        else:
            content += faker.paragraph(nb_sentences=5,
                                       variable_nb_sentences=True)
        content += "</p>"

        # print(content)

        i += 1

    content = content.replace("'", "''")
    # content = f'<p>{content}</p>'

    # post = Posts.query.get(p.id)
    title = faker.sentence(nb_words=10, variable_nb_words=True)
    f = faker.date_time()
    date_posted = f + \
        datetime.timedelta(milliseconds=random.randint(0, 999))
    post = Posts(title=title,
                 content=content,
                 poster_id=random.randint(1, 2),
                 slug=sluglify_title(title),
                 date_posted=date_posted
                 )
    db.session.add(post)
    db.session.commit()
    print(i)

    # if post.slug:
    #     pass
    # else:
    #     post.slug = sluglify_title(post.title)

    # # tzinfo=datetime.timezone.utc)
    # # .strftime(
    # # '%Y-%m-%d %H:%M:%S.%f')[:-3].replace('000',  str(random.randint(0, 999)))
    # db.session.add(post)
    # db.session.commit()
    # the_post = Posts.query.get(post.id)
    # print(post.content)
    # print(the_post.content)
    # exit(0)
    # query_u += f" SET content='{content}' WHERE id = {p.id}"
    # db.engine.execute(query_u)
    query += query_u + ','

# the_post = Posts.query.get(5)
# print(the_post.content)
# exit(0)

query = query.rstrip(',') + ';'
# print('query', query)
# with open('q.sql', encoding='utf-8', mode='w') as f:
#     f.write(query)

# All seeders inherit from Seeder


# class DemoSeeder(Seeder):

#     # run() will be called by Flask-Seeder
#     def run(self):
#         # Create a new Faker and tell it how to create User objects
#         faker = Faker(
#             cls=Posts,
#             init={
#                 "id_num": generator.Sequence(),
#                 "name": generator.Name(),
#                 "age": generator.Integer(start=20, end=100)
#             }
#         )

#         # Create 5 users
#         for user in faker.create(5):
#             print("Adding user: %s" % user)
#             db.session.add(user)
