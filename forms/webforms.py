from flask_babelplus import get_locale, lazy_gettext
from flask_ckeditor import CKEditorField
from flask_wtf.form import FlaskForm
from wtforms.fields.core import SelectField, StringField
from wtforms.fields.simple import (FileField, HiddenField, PasswordField,
                                   SubmitField, TextAreaField)
from wtforms.validators import DataRequired, EqualTo

from ..utils.external import LANGUAGES, Roles
from ..utils.functions import get_current_lang_in_session

# FORM
# Available Form Fields
# 'CheckboxInput', 'FileInput', 'HiddenInput', 'ListWidget', 'PasswordInput','RadioInput', 'Select', 'SubmitInput', 'TableWidget', 'TextArea','TextInput', 'Option', 'HTMLString', 'escape_html',
#     'DateField', 'DateTimeField', 'DateTimeLocalField', 'DecimalField',   'DecimalRangeField', 'EmailField', 'IntegerField', 'IntegerRangeField',    'SearchField', 'TelField', 'TimeField', 'URLField',
#  'BooleanField', 'TextAreaField', 'PasswordField', 'FileField', 'MultipleFileField','HiddenField', 'SubmitField', 'TextField'

# Validators
# 'DataRequired', 'data_required', 'Email', 'email', 'EqualTo', 'equal_to',
# 'IPAddress', 'ip_address', 'InputRequired', 'input_required', 'Length',
# 'length', 'NumberRange', 'number_range', 'Optional', 'optional',
# 'Required', 'required', 'Regexp', 'regexp', 'URL', 'url', 'AnyOf',
# 'any_of', 'NoneOf', 'none_of', 'MacAddress', 'mac_address', 'UUID',
# 'ValidationError', 'StopValidation'


class MyBaseForm(FlaskForm):
    class Meta:
        pass
        # locales = get_current_lang_in_session()


class LoginForm(MyBaseForm):
    """Form definition for Login."""

    username = StringField(lazy_gettext('User name'),
                           validators=[DataRequired()])
    password = PasswordField(lazy_gettext('Password'),
                             validators=[DataRequired()])
    submit_login = SubmitField(lazy_gettext('Log in'))


class NamerForm(MyBaseForm):
    """
    Form Class for Name
    """
    name = StringField(label=lazy_gettext(
        "What's your name?"), validators=[DataRequired()])
    submit = SubmitField(lazy_gettext("Submit"))


class PasswordForm(MyBaseForm):
    """
    Form Class for Password
    """
    email = StringField(label=lazy_gettext("What's your Email?"),
                        validators=[DataRequired()])
    password_hash = PasswordField(label=lazy_gettext("What's your Password?"),
                                  validators=[DataRequired()])
    submit = SubmitField(lazy_gettext("Submit"))


class PostForm(MyBaseForm):
    """ Post Form """
    title = StringField(lazy_gettext('Title'), validators=[DataRequired(), ])
    # content = StringField('Content', validators=[
    #                       DataRequired(), ], widget=TextArea())
    content = CKEditorField(lazy_gettext('Content'),
                            validators=[DataRequired(), ])
    slug = HiddenField(lazy_gettext('Slug'), validators=[])
    submit = SubmitField(lazy_gettext('Submit'))


class UserForm(MyBaseForm):
    """
    Form Class
    """
    name = StringField(label=lazy_gettext("Name"), validators=[DataRequired()])
    username = StringField(label=lazy_gettext(
        "Username"), validators=[DataRequired()])
    email = StringField(label=lazy_gettext("Email"),
                        validators=[DataRequired()])
    favorite_color = StringField(label=lazy_gettext("Favorite Color"))
    about_author = TextAreaField(lazy_gettext("About Author"))
    password_hash = PasswordField(label=lazy_gettext("Password"), validators=[DataRequired(), EqualTo(
        'password_confirm', message=lazy_gettext('Passwords must match!'))])
    password_confirm = PasswordField(
        label=lazy_gettext("Confirm password"), validators=[DataRequired()])
    profile_picture = FileField(lazy_gettext("Profile Picture"))
    d = Roles.user.value
    role = SelectField(label=lazy_gettext('User Role'),
                       choices=Roles.get_roles(), validators=[], default=d)
    submit_register = SubmitField(lazy_gettext("Register now"))


class SearchForm(MyBaseForm):
    """
    Search Form Class
    """
    q = StringField(lazy_gettext('Searched'), validators=[DataRequired()])
    submit = SubmitField(lazy_gettext('Search'))


class LanguageSelectForm(MyBaseForm):
    """
    Change language Class
    """
    next = HiddenField('next')
    language_selection = SelectField(lazy_gettext('language'), choices=LANGUAGES,
                                     validators=[DataRequired()], id='language_selection')
    submit = SubmitField(lazy_gettext('Change'),)
