import os
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_webtest import get_scopefunc
import sqlalchemy
from flask import Flask


def get_db(app: Flask) -> SQLAlchemy:
    session_options = {}
    if app.testing:
        # app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQL_DATABASE_ALCHEMY')
        session_options['scopefunc'] = get_scopefunc()
    db = SQLAlchemy(app, session_options=session_options)
    if app.config['SQLALCHEMY_DATABASE_URI'] == ''.startswith('sqlite'):
        # Batch mode for sqlite3
        migrate = Migrate(app, db, render_as_batch=True)
    else:
        migrate = Migrate(app, db, render_as_batch=True)

    return db
