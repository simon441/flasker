// document.querySelector('#js-demo').innerHTML = 'This was created with Javascript'
/** 
 * Confirm delete action
 * use "delete-js" class to target if confirmation is wanted
 */
const deleteButtons = document.querySelectorAll('.delete-js')
deleteButtons.forEach(btn => {
    btn.addEventListener('click', e => {
        e.preventDefault()

        // get the text from the button if it exists
        const btnText = btn.getAttribute('data-delete-message')
        const text = (btnText !== null) ? btnText :'Delete post?'

        const action = confirm(text)
        if (action) {
            e.target.parentElement.submit()
        }
        return false;
    })
})
 /** @type {HTMLInputElement} */
var profileImage =  document.getElementById('profile_image')
profileImage && profileImage.addEventListener('change', () => {
    const profile_image_preview = document.getElementById('profile_image_preview')
   
   if (profileImage.files && profileImage.files[0]) {
        profile_image_preview.classList.remove('pt-3')
        const reader = new FileReader()

        reader.onload = e => {
            profile_image_preview.setAttribute('src', e.target.result)
            profile_image_preview.classList.add('pt-3')
        }

        reader.readAsDataURL(profileImage.files[0])
    }
})

/** @type {HTMLButtonElement} Get the button */
const backToTopButton = document.getElementById("btn-back-to-top");

if ("showBackToTop" in window) {

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 20 ||
    document.documentElement.scrollTop > 20
  ) {
    backToTopButton.style.display = "block";
  } else {
    backToTopButton.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
backToTopButton.addEventListener('click', backToTop);


function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
} else {
    console.log(backToTopButton);
    backToTopButton.style.display = 'none'
}
