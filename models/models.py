from __future__ import annotations

import os
from datetime import datetime

import sqlalchemy
from flask_babelplus import gettext, ngettext
from flask_login import UserMixin
from slugify import slugify
from sqlalchemy import *
from sqlalchemy import event
from sqlalchemy.ext.indexable import index_property
from sqlalchemy.orm import Session, declarative_base, relationship
from werkzeug.security import check_password_hash, generate_password_hash

from ..utils.external import Roles
from ..utils.jinja_helpers import get_role

# can import Flask instance?
import_app = True

DB_URI = os.environ.get('SQL_DATABASE_ALCHEMY')
TESTING_MODELS = os.environ.get('TESTING_MODELS')
print('TESTING_MODELS', TESTING_MODELS)
if DB_URI is None:
    try:
        from ..env import Configuration
    except ImportError:
        from env import Configuration
        import_app = False
    DB_URI = Configuration.DB_URI

if import_app and TESTING_MODELS is None:
    from ..app import db

if not import_app or TESTING_MODELS == 'SQLALCHEMY':
    # Disable PyODBC's internal pooling by default behavior
    # @see https://docs.sqlalchemy.org/en/13/dialects/mssql.html#module-sqlalchemy.dialects.mssql.pyodbc
    if DB_URI.startswith('msssql'):
        import pyodbc
        pyodbc.pooling = False

    engine = sqlalchemy.create_engine(DB_URI)
    # # db = sqlalchemy.orm.Load
    Base = declarative_base()
    session = Session()
else:
    Base = db.Model
    session = db.session


def sluglify_title(title: str) -> str:
    my_date = datetime.now()
    print('slugified slug',
          f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}")
    return f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}"


class Posts(Base):
    """ Blog Post Model """
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    content = Column(Text)
    # author = Column(String(255))
    date_posted = Column(DateTime, default=datetime.utcnow)
    slug = Column(String(255), unique=True)
    # Foreign key to links Users (refer to the primary key of the User)
    poster_id = Column(Integer, ForeignKey('users.id'))

    date_posted_idx = Index('idx_date_created', date_posted.desc())
    date_id_idx = Index('date_id_idx', date_posted.desc(), id.asc())

    def __init__(self, *args, **kwargs) -> None:
        if 'title' in kwargs:
            kwargs['slug'] = sluglify_title(kwargs.get('title', ''))
        super().__init__(*args, **kwargs)

    # @sqlalchemy.orm.validates('title')
    # def verify_title(self, key, title: str) -> bool:
    #     if title is None or title == '':
    #         return False
    #     return True
    def as_json(self):
        return dict(
            id=self.id,
            title=self.title,
            content=self.content,
            date_posted=self.date_posted,
            slug=self.slug,
            poster_id=self.poster_id
        )


@event.listens_for(Posts, "before_insert")
@event.listens_for(Posts, "before_update")
def before_insert(mapper, connection, post: Posts):
    print(f"before insert: {post.title}")
    if post.title:
        slug = sluglify_title(post.title)
        print(f'the new slug is: {slug}')
        post.slug = slug
    if post.poster_id is not None:
        user: Users = Users.query.get(post.poster_id)
        if user is None:
            raise Exception(f'Poster id {post.poster_id} does not exist.')


class Users(Base, UserMixin):
    """ User model """
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(20), nullable=False, unique=True)
    name = Column(String(200), nullable=False)
    email = Column(String(120), nullable=False, unique=True)
    favorite_color = Column(String(120))
    about_author = Column(Text(), nullable=True)
    date_added = Column(DateTime, default=datetime.utcnow)
    profile_picture = Column(String(200), nullable=True)
    # add some password stuff!
    password_hash = Column(String(128))
    # User can have many Posts
    posts = relationship('Posts', backref='poster')
    role = Column(String(255), default='user', nullable=True)

    @property
    def password(self):
        raise AttributeError(gettext('Password is not a readable attribute!'))

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password: str):
        return check_password_hash(self.password_hash, password)

    # @sqlalchemy.orm.validates('email')
    # def validate_email(self, key, email):
    #     if email is not None and '@' not in email:
    #         raise ValueError("failed simple email validation")
    #     return email

    def is_admin(self) -> bool:
        """Is user an admin"""
        return self.role == Roles.admin.value

    def __repr__(self):
        return '<Name %r>' % self.name

    def as_json(self):
        return dict(
            id=self.id,
            name=self.name,
            username=self.username,
            about_author=self.about_author,
            email=self.email,
            date_added=self.date_added
        )


if not import_app and TESTING_MODELS is None:
    # dbsession = sessionmaker(bind=engine)
    # session = dbsession()
    Base.metadata.create_all(engine)

    engine.dispose()
