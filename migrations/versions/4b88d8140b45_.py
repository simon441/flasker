"""empty message

Revision ID: 4b88d8140b45
Revises: c9d6582db80f
Create Date: 2022-03-23 15:59:08.027488

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4b88d8140b45'
down_revision = 'c9d6582db80f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.create_index('date_id_idx', [sa.text('date_posted DESC'), sa.text('id ASC')], unique=False)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.drop_index('date_id_idx')

    # ### end Alembic commands ###
