import logging
import sys
import pytest
import pytest_flask
import flask
import click
from flask.testing import FlaskCliRunner
from click.testing import CliRunner
from ..env import Configuration

from ..cli import clear, run as run_wsgi
from ..app import app


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()


def test_init_env():
    # runner = FlaskCliRunner()
    pass


def test_clear_pycache():
    app.testing = True
    runner = app.test_cli_runner()

    result = runner.invoke(clear, ['--pycache', ''])


def test_clear_tests_trash():
    app.testing = True
    runner = app.test_cli_runner()

    # result = runner.invoke(test_clear)


def test_run():
    app.testing = True
    runner = app.test_cli_runner()
    logger.info(Configuration.SERVER_WSGI)
    logger.info(Configuration.SERVER_OPTIONS)
    logger.info(Configuration.PORT)
    # result = runner.invoke(run_wsgi)
    # print('output', result.output)
    # assert 'Serving on http://0.0.0.0:80' in result.output
