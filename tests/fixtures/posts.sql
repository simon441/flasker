
INSERT INTO posts (
                      id,
                      title,
                      content,
                      date_posted,
                      slug,
                      poster_id
                  )
                  VALUES (
                      1,
                      'Post 1 Updated1',
                      '<p>My first post has been updated<br />',
                      '2021-09-24 14:56:08',
                      'post-1-modified',
                      1
                  ), (
                      2,
                      'Tim''s first post',
                      '<p>My first post</p>

',
                      '2021-09-24 15:24:41',
                      'tim-post-1--',
                      2
                  ), (
                      3,
                      'Post rich text',
                      '<h3><strong>Post title</strong></h3>',
                      '2021-11-12 20:13:11',
                      'test-1',
                      1
                  );
