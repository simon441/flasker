
INSERT INTO users (
                      id,
                      username,
                      name,
                      email,
                      favorite_color,
                      about_author,
                      date_added,
                      profile_picture,
                      password_hash,
                      role
                  )
                  VALUES (
                      1,
                      'simon',
                      'Simon',
                      'simon@test.com',
                      '',
                      NULL,
                      '2022-02-21 14:14:13.228307',
                      NULL,
                      'sha256$ebWv3uDA$6c9286d8e2ad28828a107f325c37e6c59b942fc9bc724b65d11c6d274086f15b',
                      'admin'
                  ), (
                      2,
                      'tim',
                      'tim',
                      'tim@tim.tim',
                      'DArk Red',
                      NULL,
                      '2022-02-26 17:33:35.520354',
                      NULL,
                      'sha256$qpzuMInv$5952793e627714fec8db57eb1a10e62789b88995bfe4159baa5dbf336c9d4788',
                      'user'
                  );
