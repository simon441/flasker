--
-- Fichier généré par SQLiteStudio v3.2.1 sur dim. mars 6 18:22:47 2022
--
-- Encodage texte utilisé : System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table : alembic_version
DROP TABLE IF EXISTS alembic_version;
CREATE TABLE alembic_version (
	version_num VARCHAR(32) NOT NULL, 
	CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num)
);
INSERT INTO alembic_version (version_num) VALUES ('a90d9258a14a');

-- Table : posts
DROP TABLE IF EXISTS posts;
CREATE TABLE posts (
    id          INTEGER       NOT NULL,
    title       VARCHAR (255),
    content     TEXT,
    date_posted DATETIME,
    slug        VARCHAR (255),
    poster_id   INTEGER,
    PRIMARY KEY (
        id
    ),
    UNIQUE (
        slug
    ),
    FOREIGN KEY (
        poster_id
    )
    REFERENCES users (id) 
);
INSERT INTO posts (id, title, content, date_posted, slug, poster_id) VALUES (1, 'My test post', '<p>My test post content</p>

', '2022-02-27 22:17:27.112830', 'my-test-post-2022-2-27', 1);
INSERT INTO posts (id, title, content, date_posted, slug, poster_id) VALUES (2, 'fff', '<p>Tim''s test posting already\nnew times</p><p>
Lorem ipsum dolor sit, amet consectetur adipisicing elit. Id, corrupti iure! Omnis eveniet reiciendis consequatur placeat non tempore, temporibus voluptas numquam cupiditate vel esse vero exercitationem eum eos fuga possimus?</p>',
'2022-02-28 13:36:31.129863', 'fff-2022-2-28', 2);
INSERT INTO posts (id, title, content, date_posted, slug, poster_id) VALUES (3, 'fff', '<p>xdwscvfds</p>

', '2022-03-02 20:59:52.881487', 'fff-2022-3-2', 1);
INSERT INTO posts (id, title, content, date_posted, slug, poster_id) VALUES (4, 'My test post', '<p>dfesf</p>

', '2022-03-02 21:21:35.499804', 'my-test-post-2022-3-2', 3);

-- Table : users
DROP TABLE IF EXISTS users;
CREATE TABLE users (
	id INTEGER NOT NULL, 
	username VARCHAR(20) NOT NULL, 
	name VARCHAR(200) NOT NULL, 
	email VARCHAR(120) NOT NULL, 
	favorite_color VARCHAR(120), 
	about_author TEXT, 
	date_added DATETIME, 
	profile_picture VARCHAR(200), 
	password_hash VARCHAR(128), 
	role VARCHAR(255), 
	PRIMARY KEY (id), 
	UNIQUE (username), 
	UNIQUE (email)
);
INSERT INTO users (id, username, name, email, favorite_color, about_author, date_added, profile_picture, password_hash, role) VALUES (1, 'simon', 'Simon', 'simon@test.com', '', NULL, '2022-02-21 14:14:13.228307', NULL, 'sha256$ebWv3uDA$6c9286d8e2ad28828a107f325c37e6c59b942fc9bc724b65d11c6d274086f15b', 'admin');
INSERT INTO users (id, username, name, email, favorite_color, about_author, date_added, profile_picture, password_hash, role) VALUES (2, 'tim', 'tim', 'tim@tim.tim', 'DArk Red', NULL, '2022-02-26 17:33:35.520354', NULL, 'sha256$qpzuMInv$5952793e627714fec8db57eb1a10e62789b88995bfe4159baa5dbf336c9d4788', 'user');
-- INSERT INTO users (id, username, name, email, favorite_color, about_author, date_added, profile_picture, password_hash, role) VALUES (3, 'narunaru2', 'Naru update', 'naru2@invalid.tld', '', NULL, '2022-02-27 19:25:05.221343', NULL, 'sha256$1JmCRA0p$79525cf53f165cd7b2e27a0ef3ed6e4bdec7a95c954ae30f4208ada40c005423', 'user');

-- Index : idx_date_created
DROP INDEX IF EXISTS idx_date_created;
CREATE INDEX idx_date_created ON posts (date_posted DESC);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
