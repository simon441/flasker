

import logging
import re
import secrets
import sys
from unittest import TestCase
from ...app import app
from ...cli.environement import ConfigureEnv
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()


class CliEnvTest(TestCase):
    def setUp(self) -> None:
        self.env = ConfigureEnv()
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_init_class(self):
        the_env: ConfigureEnv = ConfigureEnv()
        self.assertEqual(the_env.DEFAULT_DATABASE_NAME, the_env.database_name)
        self.assertEqual(the_env.DEFAULT_GENERATED_FILE_NAME, the_env.file_name)
        self.assertEqual(the_env.DEFAULT_WSGI_HOST, the_env.wsgi_host)
        self.assertEqual(the_env.wsgi_port, the_env.wsgi_port)
        self.assertListEqual([], the_env.servers_list)
        self.assertEqual('', the_env.env_contents)

        the_env: ConfigureEnv = ConfigureEnv('myfile')
        self.assertEqual('myfile', the_env.file_name)

    def test_generate_secret_key(self):
        the_env: ConfigureEnv = ConfigureEnv()
        the_env.generate_secret_key()
        regex = r"([0-9]|[a-z])+"

        t = re.search(regex, the_env.secret_key)
        self.assertRegex(the_env.secret_key, regex)
        self.assertEqual(len(the_env.secret_key), 64)
        secret_key = the_env.secret_key

        t = the_env.generate_secret_key()
        self.assertRegex(the_env.secret_key, regex)
        self.assertEqual(len(the_env.secret_key), 64)
        self.assertNotEqual(the_env.secret_key, secret_key)

    def test_input_database(self):
        the_env: ConfigureEnv = ConfigureEnv()
        runner = app.test_cli_runner()
        the_env.input_database()
