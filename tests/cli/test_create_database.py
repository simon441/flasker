

import os
from pathlib import Path
import sqlite3
from unittest import TestCase

import pytest
from pytest import TempPathFactory

from ...cli.create_database.create_database import CreateDatabase, create_database, DatabaseManager
from ...app import app
from ..config import SQL_DB_PATH_TEST


class CreateDatabaseTest(TestCase):

    tmp_dir = ''
    tmp_path = Path()
    db_name = 'mydbtest.sqlite3'
    last_update = "5fac7a1155ca"

    def setUp(self) -> None:
        # self.tmp_dir = TempPathFactory(os.path.join(app.instance_path, 'tests', 'cli'), )
        # self.tmp_path = self.tmp_dir.mktemp('/database', numbered=True)
        self.tmp_path = os.path.join(SQL_DB_PATH_TEST, 'fixtures', 'tmp')
        url = os.path.join(self.tmp_path, self.db_name)
        if not os.path.exists(self.tmp_path):
            os.makedirs(self.tmp_path)

        app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite+pysqlite:///" + url
        return super().setUp()

    def tearDown(self) -> None:
        os.unlink(os.path.join(self.tmp_path, self.db_name))
        os.removedirs(self.tmp_path)
        return super().tearDown()

    def test_create_database(self):
        url = os.path.join(self.tmp_path, self.db_name)
        print('app root path', app.root_path)
        create_database(app.root_path, 'sqlite+pysqlite:///' + url)
        self.assertTrue(os.path.exists(url))

        # Check the newly created database
        url_sqlite = f'{url}'
        print('url_sqlite', url_sqlite)
        conn = sqlite3.connect(url_sqlite)
        req = conn.execute('SELECT * FROM alembic_version;')
        res = req.fetchone()
        print(res)
        self.assertEqual(res[0], self.last_update)
        req = conn.execute("""SELECT name FROM sqlite_schema 
            WHERE type IN ('table','view') 
            AND name NOT LIKE 'sqlite_%'
            ORDER BY 1;""")
        res = req.fetchall()

        # Get all tables names in newly created database
        tables = [i[0] for i in res]
        self.assertListEqual(tables, ['alembic_version', 'posts', 'users'])
        # for i in res:
        #     print(i)
        # print(res)
        conn.close()

    def test_test_configuree(self):
        url = os.path.join(self.tmp_path, self.db_name)
        print('app root path', app.root_path)
        db_url = 'sqlite+pysqlite:///' + url
        db = CreateDatabase(app.root_path, db_url, DatabaseManager(db_url))
        db.configure()

    def test_create_database_database_exists_true(self):
        url = os.path.join(self.tmp_path, self.db_name)
        print('app root path', app.root_path)
        db_url = 'sqlite+pysqlite:///' + url
        if (os.path.exists(url)):
            os.unlink(url)
        db = CreateDatabase(app.root_path, db_url, DatabaseManager(db_url))
        conn = sqlite3.connect(url)
        conn.execute('CREATE TABLE test_table(id INTEGER);')
        conn.close()
        self.assertTrue(db.database_exists())
        self.assertTrue(os.path.exists(url))

    def test_create_database_database_exists_false(self):
        url = os.path.join(self.tmp_path, self.db_name)
        print('app root path', app.root_path)
        db_url = 'sqlite+pysqlite:///' + url
        db = CreateDatabase(app.root_path, db_url, DatabaseManager(db_url))
        self.assertFalse(db.database_exists())
