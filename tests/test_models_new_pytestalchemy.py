# from sqlalchemy.schema import Table

import re
from typing import Dict
import flask_sqlalchemy
import flask_migrate
# def test_set_name(db_session):
#     row = db_session.query(Table).get(1)
#     row.set_name('testing')
#     assert row.name == 'testing'


# def test_transaction_doesnt_persist(db_session):
#     row = db_session.query(Table).get(1)
#     assert row.name != 'testing'

from datetime import datetime
import json
import os
import sys
from flask_testing import TestCase
from unittest import TestCase as tx, mock

import pytest
from pytest_mock import pytest_configure
import sqlalchemy
import flask
from slugify import slugify

from .config import SQL_DB_PATH_TEST

from ..app import app
# from ..models import Posts, Users


# from ..database import get_db
from ..app import db

from ..models import Posts, Users, sluglify_title
import logging

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()

FIXTURES_PATH = os.path.join(SQL_DB_PATH_TEST, 'fixtures')


@pytest.fixture(scope='session')
def mock_env():
    with mock.patch.dict(os.environ, {'TESTING_MODELS': 'True'}):
        yield


class MyTest(TestCase, tx):

    # SQLALCHEMY_DATABASE_URI = "sqlite://"
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(SQL_DB_PATH_TEST, 'db_models.sqlite3')
    TESTING = True

    def create_app(self):
        return app
    #     # pass in test configuration
    #     app.testing = True
    #     app.config['ENV'] = 'testing'
    #     app.config["TESTING"] = True
    #     app.config["DEBUG"] = True
    #     app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite+pysqlite:///' + \
    #         self.SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///')
    #     logger.info(app.config.items())
    #     return app

    def setUp(self):
        app.testing = True
        app.config['ENV'] = 'testing'
        app.config["TESTING"] = True
        app.config["DEBUG"] = True
        flask.session['lang'] = 'en'
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite+pysqlite:///' + \
            self.SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///')
        # logger.info(app.config.items())

        self.app = app
        # self.db = get_db(self.app)
        # self.db = flask_sqlalchemy.SQLAlchemy()
        # logger.debug(self.app)
        # logger.debug(self.app.config.items())
        # self.db.init_app(self.app)

        # if app.config['SQLALCHEMY_DATABASE_URI'] == ''.startswith('sqlite'):
        # Batch mode for sqlite3
        # migrate = flask_migrate.Migrate(app, self.db, render_as_batch=True)
        self.db = db
        self.db.create_engine(engine_opts={}, sa_url=self.SQLALCHEMY_DATABASE_URI)
        logger.info(self.db.engine)
        self.db.create_all()
        self.models = self._populate_db()

    def tearDown(self):

        self.db.session.remove()
        self.db.drop_all()

    @classmethod
    def tearDownClass(cls):
        pass
        # if MyTest.SQLALCHEMY_DATABASE_URI.count('flasker.sqlite3') == 0:
        #     os.remove(MyTest.SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///'))

    def _populate_db(self):
        from ..models import Posts, Users
        users = []
        with open(os.path.join(FIXTURES_PATH, 'users.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data[0]['records']
            logger.info(data)
            for i in data:
                i['date_added'] = datetime.fromisoformat(i['date_added'])
                u = Users(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                users.append(u)
        posts = []
        with open(os.path.join(FIXTURES_PATH, 'posts.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data[0]['records']
            logger.info(data)
            for i in data:
                i['date_posted'] = datetime.fromisoformat(i['date_posted'])
                u = Posts(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                posts.append(u)
        return dict(users=users, posts=posts)
        # return u

    def test_get_all_users(self):
        from ..models import Users
        users = Users.query.all()
        print('{', users, '}')
        self.assertIsInstance(users[0], Users)
        self.assertEqual(len(users), len(self.models['users']))

    def test_create_user_with_all_fields_pass(self):
        new_user = Users()
        new_user.email = 'e@ff.fr'
        new_user.username = 'my_username'
        new_user.password = 'password'
        new_user.name = 'ff'
        self.db.session.add(new_user)
        self.db.session.commit()

        user: Users = Users.query.filter_by(email='e@ff.fr').first()
        self.assertNotEqual(user.password_hash, 'password')

    def test_create_user_with_missing_fields(self):
        new_user = Users()
        new_user.email = 'e@ff.fr'
        new_user.username = 'my_username'
        new_user.password = 'password'
        new_user.name = None
        with pytest.raises(sqlalchemy.exc.IntegrityError) as exc_info:
            self.db.session.add(new_user)
            self.db.session.commit()
        exception_raised = exc_info.value
        self.assertIsInstance(exception_raised, sqlalchemy.exc.IntegrityError)
        self.assertIn('NOT NULL constraint failed: users.name', str(exception_raised))
        self.db.session.rollback()

        new_user = Users()
        new_user.email = None
        new_user.username = 'my_username'
        new_user.password = 'password'
        new_user.name = 'zdeszd'
        with pytest.raises(sqlalchemy.exc.IntegrityError) as exc_info:
            self.db.session.add(new_user)
            self.db.session.commit()
        exception_raised = exc_info.value
        self.assertIsInstance(exception_raised, sqlalchemy.exc.IntegrityError)
        self.assertIn('NOT NULL constraint failed: users.email', str(exception_raised))
        self.db.session.rollback()

        new_user = Users()
        new_user.email = 'fesdfes@gxdfvfdr.gg'
        # new_user.username =
        new_user.password = 'password'
        new_user.name = 'zdeszd'
        with pytest.raises(sqlalchemy.exc.IntegrityError) as exc_info:
            self.db.session.add(new_user)
            self.db.session.commit()
        exception_raised = exc_info.value
        self.assertIsInstance(exception_raised, sqlalchemy.exc.IntegrityError)
        self.assertIn('NOT NULL constraint failed: users.username', str(exception_raised))
        self.db.session.rollback()

    def test_create_user_already_exists(self):
        new_user = Users()
        new_user.email = 'e@ff.fr'
        new_user.username = 'simon'
        new_user.password = 'password'
        new_user.name = 'name'
        with pytest.raises(sqlalchemy.exc.IntegrityError) as exc_info:
            self.db.session.add(new_user)
            self.db.session.commit()
        exception_raised = exc_info.value
        self.assertIsInstance(exception_raised, sqlalchemy.exc.IntegrityError)
        self.assertIn('UNIQUE constraint failed: users.username', str(exception_raised))
        self.db.session.rollback()

        new_user = Users()
        new_user.email = 'simon@test.com'
        new_user.username = 'simon'
        new_user.password = 'password'
        new_user.name = 'eeee'
        with pytest.raises(sqlalchemy.exc.IntegrityError) as exc_info:
            self.db.session.add(new_user)
            self.db.session.commit()
        exception_raised = exc_info.value
        self.assertIsInstance(exception_raised, sqlalchemy.exc.IntegrityError)
        self.assertIn('UNIQUE constraint failed: users.email', str(exception_raised))
        self.db.session.rollback()

    def test_user_password_is_read_only(self):
        with self.app.test_client() as client:
            selected_user: Users = Users.query.get(1)
            with pytest.raises(AttributeError) as exc_info:
                selected_user.password
            exception_raised = exc_info.value
            self.assertIsInstance(exception_raised, AttributeError)
            self.assertIn('Password is not a readable attribute!', str(exception_raised))
            self.db.session.rollback()

    def test_user_verify_password(self):
        with self.app.test_client() as client:
            selected_user: Users = Users.query.get(1)
            self.assertTrue(selected_user.verify_password('p'))
            self.assertFalse(selected_user.verify_password('notmypassword'))
        self.db.session.rollback()

    def test_user_delete(self):
        with self.app.test_client() as client:
            selected_user: Users = Users.query.get(1)
            self.db.session.delete(selected_user)
            self.db.session.commit()
            self.assertIsNone(Users.query.get(1))
            posts: Posts = Posts.query.filter_by(poster_id=1).all()
            self.assertEqual(len(posts), 0)
        self.db.session.rollback()

    def test_posts(self):
        posts = Posts.query.all()
        self.assertEqual(len(posts), len(self.models['posts']))

    def test_add_post_with_constructor(self):
        posts = Posts.query.all()
        post = Posts(
            content="""<h1>In sunt rerum ea dolores velit! </h1><p>Lorem ipsum dolor sit amet. Ullam expedita et omnis minus quo omnis neque. A quaerat officia <strong>Qui voluptas et sint consequuntur vel animi nemo</strong> aut harum suscipit. At voluptatibus repudiandae eos deserunt eligendiest repellat et voluptas voluptatum. Est quod cupiditate cum porro voluptates ab minus internos vel quia illo eos laboriosam consectetur. </p><ul><li>Eum explicabo optio ut vero aliquid qui nobis rerum. </li><li>Ut aliquam voluptatum ab culpa provident et laudantium nulla cum Quis architecto. </li></ul><h2>Qui quam veniam rem quam molestias. </h2><p>Et nihil ullam <a href="https://www.loremipzum.com" target="_blank">Ut corrupti et facere labore qui voluptas libero</a> et suscipit deserunt ex magnam unde sed numquam soluta. Non odit voluptatibus <em>Ad perferendis et adipisci quas a incidunt nihil</em> et galisum neque? Quo enim repellendus eum assumenda voluptates sed amet perspiciatis sed rerum tempora sit ducimus voluptatem id quam quasi. Eos obcaecati culpa et eaque quam sit laborum voluptas? </p><h3>Sed voluptatum similique qui totam animi  suscipit obcaecati. </h3><p>Ad sunt galisum <em>Eos velit</em> qui eveniet laudantium ea tempore recusandae. Et pariatur possimus quo molestias provident qui iste dolorem aut consequatur ducimus  dignissimos exercitationem vel cupiditate impedit ex exercitationem voluptas. Aut voluptatibus possimus <strong>Aut tenetur et sunt quod aut perspiciatis saepe eos odio voluptatem</strong>. Qui quas eveniet eum iusto laborum aut quae voluptatibus sed dolorem optio nam Quis deleniti non rerum molestiae. </p>""",
            title='my-title-ipsum',
            poster_id=2)
        self.db.session.add(post)
        self.db.session.commit()
        new_post: Posts = Posts.query.filter_by(title='my-title-ipsum').first()
        self.assertEqual(new_post.title, post.title)
        self.assertEqual(new_post.content, post.content)
        print(new_post.slug)
        self.assertTrue(new_post.slug.startswith('my-title-ipsum'))
        regex = r"(\d{4}-\d{2}-\d{2})$"
        logger.info(['new_post.slug', new_post.slug])
        matches = re.finditer(regex, new_post.slug, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            print("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum=matchNum,
                  start=match.start(), end=match.end(), match=match.group()))
        reg = re.findall(regex, new_post.slug, re.MULTILINE)
        self.assertEqual(len(reg), 1)

    def test_add_post_adding_fields_after__init__class(self):
        posts = Posts.query.all()
        post = Posts(
            content="""<h1>In sunt rerum ea dolores velit! </h1><p>Lorem ipsum dolor sit amet. Ullam expedita et omnis minus quo omnis neque. A quaerat officia <strong>Qui voluptas et sint consequuntur vel animi nemo</strong> aut harum suscipit. At voluptatibus repudiandae eos deserunt eligendiest repellat et voluptas voluptatum. Est quod cupiditate cum porro voluptates ab minus internos vel quia illo eos laboriosam consectetur. </p><ul><li>Eum explicabo optio ut vero aliquid qui nobis rerum. </li><li>Ut aliquam voluptatum ab culpa provident et laudantium nulla cum Quis architecto. </li></ul><h2>Qui quam veniam rem quam molestias. </h2><p>Et nihil ullam <a href="https://www.loremipzum.com" target="_blank">Ut corrupti et facere labore qui voluptas libero</a> et suscipit deserunt ex magnam unde sed numquam soluta. Non odit voluptatibus <em>Ad perferendis et adipisci quas a incidunt nihil</em> et galisum neque? Quo enim repellendus eum assumenda voluptates sed amet perspiciatis sed rerum tempora sit ducimus voluptatem id quam quasi. Eos obcaecati culpa et eaque quam sit laborum voluptas? </p><h3>Sed voluptatum similique qui totam animi  suscipit obcaecati. </h3><p>Ad sunt galisum <em>Eos velit</em> qui eveniet laudantium ea tempore recusandae. Et pariatur possimus quo molestias provident qui iste dolorem aut consequatur ducimus  dignissimos exercitationem vel cupiditate impedit ex exercitationem voluptas. Aut voluptatibus possimus <strong>Aut tenetur et sunt quod aut perspiciatis saepe eos odio voluptatem</strong>. Qui quas eveniet eum iusto laborum aut quae voluptatibus sed dolorem optio nam Quis deleniti non rerum molestiae. </p>""",
            poster_id=2)
        post.title = 'my-title-ipsum'
        self.db.session.add(post)
        self.db.session.commit()
        new_post: Posts = Posts.query.filter_by(title='my-title-ipsum').first()
        self.assertEqual(new_post.title, post.title)
        self.assertEqual(new_post.content, post.content)
        print(new_post.slug)
        self.assertTrue(new_post.slug.startswith('my-title-ipsum'))
        regex = r"(\d{4}-\d{2}-\d{2})$"
        # re.finditer(regex, new_post.slug, re.MULTILINE)
        reg = re.findall(regex, new_post.slug, re.MULTILINE)
        self.assertEqual(len(reg), 1)

    def test_add_post_adding_fieldsmissing_title(self):
        posts = Posts.query.all()
        content = """<h1>In sunt rerum ea dolores velit! </h1><p>Lorem ipsum dolor sit amet. Ullam expedita et omnis minus quo omnis neque. A quaerat officia <strong>Qui voluptas et sint consequuntur vel animi nemo</strong> aut harum suscipit. At voluptatibus repudiandae eos deserunt eligendiest repellat et voluptas voluptatum. Est quod cupiditate cum porro voluptates ab minus internos vel quia illo eos laboriosam consectetur. </p><ul><li>Eum explicabo optio ut vero aliquid qui nobis rerum. </li><li>Ut aliquam voluptatum ab culpa provident et laudantium nulla cum Quis architecto. </li></ul><h2>Qui quam veniam rem quam molestias. </h2><p>Et nihil ullam <a href="https://www.loremipzum.com" target="_blank">Ut corrupti et facere labore qui voluptas libero</a> et suscipit deserunt ex magnam unde sed numquam soluta. Non odit voluptatibus <em>Ad perferendis et adipisci quas a incidunt nihil</em> et galisum neque? Quo enim repellendus eum assumenda voluptates sed amet perspiciatis sed rerum tempora sit ducimus voluptatem id quam quasi. Eos obcaecati culpa et eaque quam sit laborum voluptas? </p><h3>Sed voluptatum similique qui totam animi  suscipit obcaecati. </h3><p>Ad sunt galisum <em>Eos velit</em> qui eveniet laudantium ea tempore recusandae. Et pariatur possimus quo molestias provident qui iste dolorem aut consequatur ducimus  dignissimos exercitationem vel cupiditate impedit ex exercitationem voluptas. Aut voluptatibus possimus <strong>Aut tenetur et sunt quod aut perspiciatis saepe eos odio voluptatem</strong>. Qui quas eveniet eum iusto laborum aut quae voluptatibus sed dolorem optio nam Quis deleniti non rerum molestiae. </p>"""
        post = Posts(
            content=content,
            poster_id=2)
        self.db.session.add(post)
        self.db.session.commit()
        new_post: Posts = Posts.query.filter_by(content=content).first()
        self.assertIsNone(post.title)
        self.assertEqual(new_post.content, post.content)
        print(new_post.slug)
        self.assertIsNone(new_post.slug)

    def test_add_post_adding_fields_poster_id_not_exists(self):
        posts = Posts.query.all()
        post = Posts(
            content="""<h1>In sunt rerum ea dolores velit! </h1><p>Lorem ipsum dolor sit amet. Ullam expedita et omnis minus quo omnis neque. A quaerat officia <strong>Qui voluptas et sint consequuntur vel animi nemo</strong> aut harum suscipit. At voluptatibus repudiandae eos deserunt eligendiest repellat et voluptas voluptatum. Est quod cupiditate cum porro voluptates ab minus internos vel quia illo eos laboriosam consectetur. </p><ul><li>Eum explicabo optio ut vero aliquid qui nobis rerum. </li><li>Ut aliquam voluptatum ab culpa provident et laudantium nulla cum Quis architecto. </li></ul><h2>Qui quam veniam rem quam molestias. </h2><p>Et nihil ullam <a href="https://www.loremipzum.com" target="_blank">Ut corrupti et facere labore qui voluptas libero</a> et suscipit deserunt ex magnam unde sed numquam soluta. Non odit voluptatibus <em>Ad perferendis et adipisci quas a incidunt nihil</em> et galisum neque? Quo enim repellendus eum assumenda voluptates sed amet perspiciatis sed rerum tempora sit ducimus voluptatem id quam quasi. Eos obcaecati culpa et eaque quam sit laborum voluptas? </p><h3>Sed voluptatum similique qui totam animi  suscipit obcaecati. </h3><p>Ad sunt galisum <em>Eos velit</em> qui eveniet laudantium ea tempore recusandae. Et pariatur possimus quo molestias provident qui iste dolorem aut consequatur ducimus  dignissimos exercitationem vel cupiditate impedit ex exercitationem voluptas. Aut voluptatibus possimus <strong>Aut tenetur et sunt quod aut perspiciatis saepe eos odio voluptatem</strong>. Qui quas eveniet eum iusto laborum aut quae voluptatibus sed dolorem optio nam Quis deleniti non rerum molestiae. </p>""",
            title='my-title-ipsum2',
            poster_id=1000)
        with pytest.raises(Exception) as ex:
            self.db.session.add(post)
            self.db.session.commit()
        ex_msg = ex.value
        self.assertEqual(str(ex.getrepr(style='value')), 'Poster id 1000 does not exist.')
        self.db.session.rollback()

    def test_update_post(self):
        post: Posts = Posts.query.get(1)
        post_data = dict(
            title='My New title',
            content='my new content',
            poster_id=2
        )
        post.title = post_data['title']
        post.content = post_data['content']
        post.poster_id = post_data['poster_id']
        self.db.session.add(post)
        self.db.session.commit()

        # Verify updated post data
        new_post: Posts = Posts.query.get(1)
        self.assertEqual(new_post.title, post_data['title'])
        self.assertEqual(new_post.content, post_data['content'])
        slug = sluglify_title(post_data['title'])
        self.assertEqual(new_post.slug, slug)
        self.assertEqual(new_post.id, new_post.id)
        self.assertEqual(new_post.poster_id, post.poster_id)
        self.assertEqual(str(post.date_posted), str(new_post.date_posted))
        self.db.session.rollback()

    def test_delete_post(self):
        post: Posts = Posts.query.get(1)

        self.db.session.delete(post)
        self.db.session.commit()
        self.assertIsNone(Posts.query.get(1))
