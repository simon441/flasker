from ..models import Users
from .config import ROOT_DIR_TEST, SQL_DB_PATH_TEST
import json
import logging
import os
import re
import sqlite3
import tempfile
from pathlib import Path

import flask
import flask_login
import pytest
import sqlalchemy
from flask import session
from flask.testing import FlaskClient
from flask_babelplus import Babel
from flask_env import MetaFlaskEnv
from flask_login import login_required, login_user
from flask_sqlalchemy import SQLAlchemy
pytestmark = pytest.mark.skip("all tests still WIP")


# import app

# from routes import *


# class FlaskLoginClient(flask.testing.):
#     """
#     A Flask test client that knows how to log in users
#     using the Flask-Login extension.
#     """

#     def __init__(self, *args, **kwargs):
#         user = kwargs.pop("user", None)
#         fresh = kwargs.pop("fresh_login", True)

#         super(FlaskLoginClient, self).__init__(*args, **kwargs)

#         if user:
#             with self.session_transaction() as sess:
#                 print('user exists', user)
#                 sess["_user_id"] = user.get_id()
#                 sess["_fresh"] = fresh

def _remove_db():
    os.remove(os.path.join(SQL_DB_PATH_TEST, database_name))


def _get_csrf_value(data: str) -> str:
    return data.split('type="hidden" value="')[1].split('">')[0]


def _get_csrf_from_route(client, route_name: str, fields: dict) -> str:
    """get_csrf_from_route Get a CSRF token from a route

    Arguments:
        route_name -- The route url

    Returns:
        The CSRF token
    """
    # try:
    response = client.post(route_name, data=fields, follow_redirects=True)
    data = response.get_data(as_text=True)
    # print(data)
    # print(_get_csrf_value(data))
    return _get_csrf_value(data)

    # except AttributeError as e:
    #     print(e)


def _get_sql() -> str:
    # read in SQL for populating test data
    with open(os.path.join(ROOT_DIR_TEST, 'db',  "sqlschema_sqlite.sql"), "rb") as f:
        _data_sql = f.read().decode("utf8")
        return _data_sql


# @pytest.fixture
def gettext(string):
    return string


def set_session_cookie(client):
    val = app.session_interface.get_signing_serializer(app).dumps(dict(session))
    client.set_cookie('127.0.0.1', app.session_cookie_name, val)


class Config(MetaFlaskEnv):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'eeeee'


database_name = 'db_test_3.sqlite3'
db_full_path = os.path.join(SQL_DB_PATH_TEST, database_name)


def get_locale():
    return 'en'


def get_babel(app):
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        return 'en'
    return babel


# @pytest.fixture()
def app():
    from ..models import Users
    db_full_path = os.path.join(SQL_DB_PATH_TEST, database_name)

    """Create and configure a new app instance for each test."""
    # create a temporary file to isolate the database for each test
    # db_fd, db_path = tempfile.mkstemp(prefix='test_', dir=os.path.curdir)
    # db_path = os.path.join(os.path.curdir, 'db_test.sqlite3')
    # # create the app with common test config
    app = flask.Flask(__name__)
    app.config.from_object(Config)
    # app.config.clear()
    app.config.update({"TESTING": True, 'SQLALCHEMY_DATABASE_URI': 'sqlite:///'+db_full_path})
    # app.config['SQLALCHEMY_DATABASE_URI'] =
    app.config.setdefault('SQLALCHEMY_DATABASE_URI', 'sqlite:///'+db_full_path)

    # app.config[] = 'sqlite:///'+db_path
    # app.config['WTF_CSRF_ENABLED'] = False  # no CSRF during tests
    app.config['DEBUG_TB_ENABLED'] = False
    app.config['WTF_I18N_ENABLED'] = False

    # db_u = sqlite3.connect(db_path)
    # db_u.executescript(_get_sql())
    # db_u.commit()
    # db_u.close()
    app.testing = True
    # babel = get_babel(app)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_full_path
    # db_u = sqlite3.connect(db_path)
    # db_u.executescript(_get_sql())
    # db_u.commit()
    # db_u.close()

    # db = SQLAlchemy()

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_full_path

    db = get_db(app)
    # db.init_app(app)
    # with app.app_context():
    #     db = get_db(app)    #     db.engine.executescript(get_sql())

    # with app.app_context():
    #     db.engine.executescript(get_sql())

    # create the database and load test data
    # with app.app_context():
    #     db.engine.execute(get_sql())

    login_manager = flask_login.LoginManager()
    login_manager.init_app(app)

    # app.config['SESSION_PROTECTION'] = None
    app.config['LOGIN_DISABLED'] = True

    @login_manager.user_loader
    def load_user(user_id):
        """ Load user when we log in """
        return Users.query.get(int(user_id))

    login_manager.session_protection = None
    yield app

    # close and remove the temporary database
    # os.close(db_fd)

    db.session.remove()
    db.engine.dispose()
    os.unlink(db_full_path)
    _remove_db()
    os.unlink(db_full_path)
    print('db file exists', os.path.exists(db_full_path))


def get_db(app: flask.Flask):
    app.config['DEBUG_TB_ENABLED'] = False
    app.config['WTF_I18N_ENABLED'] = False

    db_u = sqlite3.connect(db_full_path)
    db_u.executescript(_get_sql())
    db_u.commit()
    db_u.close()

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_full_path
    db = flask.g.db = SQLAlchemy(app)

    # with app.app_context():
    # db.engine.executescript(get_sql())

    return db


def populate_db():
    """Create if not exists then Populate the database"""
    db_u = sqlite3.connect(db_full_path)
    db_u.executescript(_get_sql())
    db_u.commit()
    db_u.close()


@pytest.fixture
def client(app: flask.Flask):
    """A test client for the app."""
    app.testing = True
    with app.test_request_context() as ctx:
        client = app.test_client()
        return client


@pytest.fixture
def runner(app):
    """A test runner for the app's Click commands."""
    return app.test_cli_runner()


def test_index(app: flask.Flask):
    # app.config['WTF_I18N_ENABLED'] = False
    # app.config['WTF_CSRF_ENABLED'] = False
    # app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:

        babel = Babel(app)

        @babel.localeselector
        def get_locale():
            return 'en'
        response = client.get('/')
        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        data: str = response.get_data(as_text=True)
        list_to_find: list[list] = [['Pepperoni', 4], ['Cheese', 3], ['Mushrooms', 3], [42, 3], ["Favorite Pizza", 2]]
        for i in list_to_find:
            print(i)
            assert data.count(f"{i[0]}") == i[1]


def test_index_fr(client):
    # encoder = flask.json.JSONEncoder()
    # l = encoder.encode({'lang': 'fr'})
    # client.set_cookie('flasker', l, httponly=True, samesite='Lax')

    flask.session['lang'] = 'fr'
    response: flask.Response = client.get('/')
    assert response.status_code == 200
    assert isinstance(response.get_data(as_text=True), str)
    # assert response.headers['Cookie']['flasker']['lang'] == 'fr'
    assert flask.session['lang'] == 'fr'


def test_namer(client):
    # encoder = flask.json.JSONEncoder()
    # l = encoder.encode({'lang': 'fr'})
    # client.set_cookie('flasker', l, httponly=True, samesite='Lax')

    response: flask.Response = client.get('/name')
    assert response.status_code == 200
    data = response.get_data(as_text=True)
    assert isinstance(data, str)
    assert data.count("What's your name?") == 2
    for i in ['name', 'submit']:
        assert i in data

    # response = client.post('/name', data=dict(name='simon', submit='Submit'), follow_redirects=True)
    # data = response.get_data(as_text=True)
    token = _get_csrf_from_route(client, '/name', dict(name='simon', submit='Submit'))
    response = client.post('/name', data=dict(
        name='simon', submit='Submit', csrf_token=token),
        follow_redirects=True)
    data = response.get_data(as_text=True)
    assert 'Form submitted successfully' in data


def test_lang_change_fr(client):
    from werkzeug.http import parse_cookie

    # encoder = flask.json.JSONEncoder()
    # l = encoder.encode({'lang': 'fr'})
    # client.set_cookie('flasker', l, httponly=True, samesite='Lax')
    # flask.session['lang'] = 'fr'
    response: flask.Response = client.post('/lang', data={
        "language_selection": 'fr',
        "next": "/"
    })
    assert response.status_code == 302
    assert isinstance(response.get_data(as_text=True), str)
    d = flask.json.JSONDecoder()
    cookie = parse_cookie(str(response.headers.get('Set-Cookie')))['flasker']

    assert d.decode(cookie)['lang'] == 'fr'


def test_login_get(app: flask.Flask):
    from ..forms.webforms import LoginForm
    form = LoginForm()
    # get_db(app)
    print(app.config)
    with app.test_client() as client:

        response: flask.Response = client.get('/login')
        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        assert 'Login' in response.get_data(as_text=True)
        val = 'value=""'
        data = response.get_data(as_text=True)
        assert 'value=""' in data
        assert 'name="username"' in response.get_data(as_text=True)
        assert 'value=""' in response.get_data(as_text=True)
        assert 'name="password"' in response.get_data(as_text=True)


def test_login_post(app: flask.Flask):
    db = get_db(app)
    app.testing = True

    app.config['WTF_I18N_ENABLED'] = False
    # app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True

    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:

        response: flask.Response = client.post('/login',
                                               #                                        data={
                                               #     "username": "sssimon",
                                               #     "password": "aaa",
                                               #     "submit_login": "Log In"
                                               # }
                                               data=dict(
                                                   # 'IjkyYjhmMzRhZDU4ZjAwMjU0OWZlZGRlOTc4MThmMzY3ZTU2ZjhjNWQi.YhthWA.FPgV1prxSXnl4Bho5INiSSAZlVg',
                                                   csrf_token="eff",
                                                   username='sssimon',
                                                   password='fdrdr',
                                                   submit_login='Log in'), follow_redirects=True)
        assert response.status_code == 200
        # assert response. == '/user/add'  # redirected to login
        assert isinstance(response.get_data(as_text=True), str)
        data = response.get_data(as_text=True)
        print(data)
        response: flask.Response = client.post('/login', data=dict(
            # 'IjkyYjhmMzRhZDU4ZjAwMjU0OWZlZGRlOTc4MThmMzY3ZTU2ZjhjNWQi.YhthWA.FPgV1prxSXnl4Bho5INiSSAZlVg',
            csrf_token=data.split('type="hidden" value="')[1].split('">')[0],
            username='sssimon',
            password='fdrdr',
            submit_login='Log in'


            # "username": "simon",
            # "password": "p",
            # "submit_login": "Log in"
        ), follow_redirects=True)

        # response: flask.Response = client.post('/login', data={
        #     "username": "sssimon",
        #     "password": "aaa",
        #     "submit_login": "Log In",
        #     "csrf": data.split('type="hidden" value="')[1].split('">')[0]
        # }, follow_redirects=True)
        data = response.get_data(as_text=True)
        assert 'Invalid credentials. Try Again' in data
        # assert flask.session['_flashes']
        assert 'value="sssimon"' in data

        response: flask.Response = client.post('/login', data=dict(
            # 'IjkyYjhmMzRhZDU4ZjAwMjU0OWZlZGRlOTc4MThmMzY3ZTU2ZjhjNWQi.YhthWA.FPgV1prxSXnl4Bho5INiSSAZlVg',
            csrf_token=data.split('type="hidden" value="')[1].split('">')[0],
            username='simon',
            password='p',
            submit_login='Log in')        # "username": "simon",
            # "password": "p",
            # "submit_login": "Log in"
            , follow_redirects=True)
        assert response.status_code == 200
        # assert response. == '/user/add'  # redirected to login
        assert isinstance(response.get_data(as_text=True), str)
        data = response.get_data(as_text=True)
        print(data)
        print(data.split('type="hidden" value="')[1].split('">')[0])
        # response: flask.Response = client.post('/login', data={
        #     "username": "sssimon",
        #     "password": "aaa",
        #     "submit_login": "Log In",
        #     "csrf": data.split('type="hidden" value="')[1].split('">')[0]
        # }, follow_redirects=True)
        # print(response.get_data())
        # assert 'Invalid credentials. Try Again' in flask.session['_flashes']
        # assert flask.session['_flashes']
        print(dict(response.headers.items()))
        print(response.content_location, response.location)
        assert 'Login successful!' in data
        assert '<h1 class="display-5 mt-3 text-black text-black-50">Dashboard</h1>' in data
        assert '<strong>User Id: </strong>1' in data

    db.session.remove()
    db.engine.dispose()


def test_logout(app: flask.Flask):
    app.testing = True

    app.config['WTF_I18N_ENABLED'] = False
    # app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:
        response = client.get('/login')
        data = response.get_data(as_text=True)
        response: flask.Response = client.post('/login', data=dict(
            csrf_token=data.split('type="hidden" value="')[1].split('">')[0],
            username='simon',
            password='p',
            submit_login='Log in'), follow_redirects=True)
        data = response.get_data(as_text=True)
        assert '<h1 class="display-5 mt-3 text-black text-black-50">Dashboard</h1>' in data

        response2: flask.Response = client.post('/logout', follow_redirects=True)
        data = response2.get_data(as_text=True)
        print(data)
        assert 'You have been logged out!\nThanks for stopping by...' in data


def test_posts_list(app: flask.Flask):
    from ..models import Posts

    app.testing = True
    # db = get_db(app)

    with app.test_client() as c:
        response = c.get('/posts')
        data = response.get_data(as_text=True)

        posts: list[Posts] = Posts.query.order_by(Posts.date_posted)

        for post in posts:
            uri = flask.url_for('post', id=post.id)
            assert f'<h2><a href="{uri}">{post.title}</a></h2>'
            assert f'<time datetime="{post.date_posted}'
            assert post.content in data

        # Remove all posts from database
        db = sqlite3.connect(db_full_path)
        db.execute('DELETE FROM posts')
        db.commit()
        db.close()

        # No posts are available
        response = c.get('/posts')
        data = response.get_data(as_text=True)
        assert '<p class="text-center">No post</p>' in data

        populate_db()


def test_post(app: flask.Flask):

    from ..models import Posts

    app.testing = True

    # db = get_db(app)

    print(app.config)

    with app.test_client() as c:
        response: flask.Response = c.get('/posts/100')

        assert response.status_code == 404

        response: flask.Response = c.get('/posts/1')

        data = response.get_data(as_text=True)

        post: Posts = Posts.query.get(1)

        uri = flask.url_for('post', id=post.id)
        assert f'<h2>{post.title}</h2>'
        assert f'By: {post.poster.name}'
        assert f'datetime="{ post.date_posted }"' in data
        assert post.content in data


def test_register_get(app: flask.Flask):
    from ..forms.webforms import UserForm
    form = UserForm()

    app.config['WTF_I18N_ENABLED'] = False
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:

        response: flask.Response = client.get('/user/add')
        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        assert 'Register' in response.get_data(as_text=True)
        val = 'name=""'
        data = response.get_data(as_text=True)
        assert 'value=""' in data
        assert 'name="name"' in response.get_data(as_text=True)
        assert 'name="username"' in response.get_data(as_text=True)
        assert 'name="email"' in response.get_data(as_text=True)
        assert 'name="username"' in response.get_data(as_text=True)
        assert 'name="favorite_color"' in response.get_data(as_text=True)
        assert 'name="password_hash"' in response.get_data(as_text=True)
        assert 'name="password_confirm"' in response.get_data(as_text=True)
        assert 'name="submit_register"' in response.get_data(as_text=True)
        assert '"Register now"' in response.get_data(as_text=True)


def test_register_post(app: flask.Flask):
    # db = get_db(app)
    import logging

    from flask_babelplus import Babel
    from werkzeug.security import generate_password_hash

    from ..models import Users
    from ..forms.webforms import UserForm
    form = UserForm()
    app.config['WTF_I18N_ENABLED'] = False
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:

        babel = Babel(app)

        @babel.localeselector
        def get_locale():
            return 'en'

        user_list = Users.query.all()
        assert len(user_list) == 2

        flask.session['lang'] = 'en'

        token = _get_csrf_from_route(client, '/user/add', dict(
            csrf_token='',
            name='us',
            username='',
            email='',
            favorite_color='',
            password_hash='',
            password_confirm='',
            profile_picture='',
            about_author='',
            submit_register='Register now',
            role='user'
        ))

        user_data = dict(csrf_token=token,
                         name='Naru',
                         username='narunaru',
                         email='naru@invalid.tld',
                         favorite_color='Red',
                         password_hash='password',
                         password_confirm='password',
                         submit_register='Register now')
        response: flask.Response = client.post('/user/add', data=user_data,
                                               content_type='application/x-www-form-urlencoded', follow_redirects=True)

        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        data = response.get_data(as_text=True)
        assert f"<td>{user_data['username']}</td>" in data

        hashed_pwd = Users.query.filter_by(email=user_data['email']).first().password_hash
        assert hashed_pwd in data

        # User already exists
        response: flask.Response = client.post('/user/add', data=user_data,
                                               content_type='application/x-www-form-urlencoded', follow_redirects=True)

        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        data = response.get_data(as_text=True)
        assert f"User already exists" in data

        # # db.engine.execute('SELECT * FROM users;')

        logging.debug(user_list)

        assert Users.query.count() == 3


def test_search(app: flask.Flask):
    # db = get_db(app)
    import logging

    from flask_babelplus import Babel
    from werkzeug.security import generate_password_hash

    from ..models import Users
    from ..forms.webforms import UserForm
    form = UserForm()
    app.config['WTF_I18N_ENABLED'] = False
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:
        # empty search
        response: flask.Response = client.post('/search', data=dict(q='', submit=''))
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert 'You searched for: <em></em>' in data
        assert '0 post found' in data

        # search for
        response: flask.Response = client.post('/search', data=dict(q='first', submit='Search'))
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert 'You searched for: <em>first</em>' in data
        assert '2 posts found' in data


def test_update_user(app: flask.Flask):
    import logging

    from flask_babelplus import Babel
    from werkzeug.security import generate_password_hash

    from ..models import Users
    from ..forms.webforms import UserForm
    form = UserForm()
    app.config['WTF_I18N_ENABLED'] = False
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    with app.test_client() as client:

        babel = Babel(app)

        @babel.localeselector
        def get_locale():
            return 'en'

        user_list = Users.query.all()

        flask.session['lang'] = 'en'

        token = _get_csrf_from_route(client, '/login', dict(
            csrf_token='',
        ))

        user_data = dict(csrf_token=token,
                         username='narunaru',
                         password='password',
                         email='naru@invalid.tld',
                         submit_login='Log in'
                         )
        response: flask.Response = client.post('/login', data=user_data,
                                               content_type='application/x-www-form-urlencoded', follow_redirects=True)

        assert response.status_code == 200
        assert isinstance(response.get_data(as_text=True), str)
        data = response.get_data(as_text=True)
        user: Users = Users.query.filter_by(email=user_data['email']).first()

        # Not the current user -> refuse
        response: flask.Response = client.get(f'/update/1')
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert "Sorry, you can't edit this user." in data

        # Correct user -> can edit
        response: flask.Response = client.get(f'/update/{user.id}')
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert 'Update User' in data
        assert 'role' not in data
        assert 'value="Update"' in data

        # Edit user
        user_data = dict(csrf_token=token,
                         name='Naru update',
                         username='narunaru2',
                         email='naru2@invalid.tld',
                         favorite_color='',
                         password_hash='password2',
                         password_confirm='password2',
                         submit_register='Update',
                         role='user'
                         )

        response: flask.Response = client.post(f'/update/{user.id}', data=user_data, follow_redirects=True)

        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert 'User Updated Successfully' in data

        # # Test with admin user (change role from user to admin)
        db = sqlite3.connect(db_full_path)
        print(db_full_path)
        db.execute("UPDATE users SET role='admin' WHERE id=3;")
        db.commit()
        cur = db.cursor()
        results = cur.execute("SELECT * FROM users;").fetchall()
        db.close()
        print(results)
        db = sqlite3.connect(db_full_path)
        print(db_full_path)
        db.execute("UPDATE users SET role='admin' WHERE id=3;")
        db.commit()
        cur = db.cursor()
        results = cur.execute("SELECT * FROM users;").fetchall()
        db.close()
        print(results)

        user.role = 'admin'

        # # Correct user -> can edit
        response: flask.Response = client.get(f'/update/{user.id}')
        users = Users.query.all()
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        # print(data)
        assert 'Update User' in data
        assert 'User Role' in data
        assert 'admin' in data
        assert 'value="admin"' in data
        assert 'value="Update"' in data

        user_data_new = user_data.copy()
        user_data_new['role'] = 'user'
        response: flask.Response = client.post(f'/update/{user.id}', data=user_data_new, follow_redirects=True)
        assert response.status_code == 200
        data = response.get_data(as_text=True)
        assert 'User Role' not in data


def test_get_json(app: flask.Flask):
    """ Test Get Data as JSON """
    with app.test_client() as client:
        response: flask.Response = client.get('/json')

        favorite_pizza = {
            "Jane": "Pepperonni",
            "Mary": "Cheese",
            "Stuart": "Veggie",
            "Tim": "Mushroom"
        }
        assert favorite_pizza == json.loads(response.get_data(as_text=True))


@pytest.fixture
def as_admin(app, client):
    """Log in as admin, use this fixture to test protected views."""
    user = Users.query.get(3)
    return user


def _test_add_post(app: flask.Flask, as_admin):

    import logging

    from flask_babelplus import Babel
    from werkzeug.security import generate_password_hash

    # app.config['LOGIN_DISABLED'] = True
    from ..models import Posts, Users
    from ..forms.webforms import PostForm
    form = PostForm()
    app.config['WTF_I18N_ENABLED'] = False
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = True
    logger = logging.basicConfig(level=logging.DEBUG)

    app.test_client_class = flask_login.FlaskLoginClient

    user: Users = Users.query.filter_by(name="Naru update").first()
    print(user, Users.query.all())
    # user.is_active = True
    # user.is_authenticated = True
    is_logged_in = login_user(user, force=True, fresh=True, remember=True)
    assert True == is_logged_in

    @app.route('/add-post', methods=['GET', 'POST'])
    @login_required
    def add_post():
        """ Add Post Page """

        form = PostForm()

        if form.validate_on_submit():
            poster = user.id
            post = Posts(
                title=form.title.data, content=form.content.data, poster_id=poster, slug=form.slug.data)
            #  Clear the form
            form.title.data = ''
            form.content.data = ''
            form.slug.data = ''

            # Add post data to the database
            flask.flash('Blog Post Submitted Successfully!', 'success')
            return flask.redirect(flask.url_for('post', id=post.id))
            # return redirect('posts')

        # Redirect to the webpage
        return flask.render_template('add_post.html', form=form)

    with app.app_context(), app.test_client() as client:
        if user:
            with client.session_transaction() as session:
                print('user exists', user)
                # sess["_user_id"] = user.get_id()
                # sess["_fresh"] = True

                print(flask.session, session)
                babel = Babel(app)

                @babel.localeselector
                def get_locale():
                    return 'en'

                posts = Posts.query.all()

                flask.session['lang'] = 'en'
                print(flask.request, flask.session, posts, is_logged_in, session)

                # token = _get_csrf_from_route(client, '/login', dict(
                #     csrf_token='',
                # ))

                # user_data = dict(csrf_token=token,
                #                  username='narunaru',
                #                  password='password',
                #                  email='naru@invalid.tld',
                #                  submit_login='Log in'
                #                  )
                # response: flask.Response = client.post('/login', data=user_data,
                #                                        content_type='application/x-www-form-urlencoded', follow_redirects=True)

                # assert response.status_code == 200
                # assert isinstance(response.get_data(as_text=True), str)
                # data = response.get_data(as_text=True)

                # Add a post -> POST
                post_data = dict(
                    title='test post',
                    content='test post content',
                    slug='text-post-content',
                )

                is_logged_in = login_user(user, force=True, fresh=True)
                assert True == is_logged_in

                response: flask.Response = client.post('/add-post', data=post_data)

                print(flask.request, flask.session, posts, is_logged_in, session)
                print(response.get_data(as_text=True))

                assert response.status_code == 200

                print('770flask_login.current_user', flask_login.current_user)
                assert 'Blog Post Submitted Successfully!' in response.get_data(as_text=True)
                assert post_data['title'] in response.get_data(as_text=True)

        #         with app.test_request_context('/add-post', method='GET') as req:
        #             # is_logged_in = login_user(user, force=True, fresh=True)
        #             # assert True == is_logged_in

        #             # set_session_cookie(client)
        #             # assert int(session['_user_id']) == user.id
        #             print(session)
        # #             # Add a post -> GET
        #             # response: flask.Response = client.get()
        #             print(req.session)

        #             print('flask_login.current_user', flask_login.current_user)

        #             # Add a post -> POST
        #             post_data = dict(
        #                 title='test post',
        #                 content='test post content',
        #                 slug='text-post-content',
        #             )
        #             response: flask.Response = client.post('/add-post', data=post_data)

        #             print(response.get_data(as_text=True))

        #             assert response.status_code == 200
        #             print(req.session)
        #             print('770flask_login.current_user', flask_login.current_user)
        #             assert 'Blog Post Submitted Successfully!' in response.get_data(as_text=True)
        #             assert post_data['title'] in response.get_data(as_text=True)

        #             is_logged_in = login_user(user, force=True, fresh=True)
        #             assert True == is_logged_in

        #             # set_session_cookie(client)
        #             assert int(session['_user_id']) == user.id
        # #             # Add a post -> GET
        #             # response: flask.Response = client.get()
        #             print(req.session)

        #             print('744flask_login.current_user', flask_login.current_user)

        #             # assert 'Add Blog Post...' in response.get_data(as_text=True)

        # #             # Add a post -> POST
        #             post_data = dict(
        #                 title='test post',
        #                 content='test post content',
        #                 slug='text-post-content',
        #             )

        #             # Missing Fields
        #             # post_data_fail = post_data.copy()
        #             # post_data_fail['title'] = ''
        #             # post_data_fail['content'] = ''
        #             # response: flask.Response = client.post('/add-post', data=post_data_fail, follow_redirects=True)
        #             # print(response.get_data(as_text=True))
        #             # assert response.status_code == 200
        #             # assert 'Add Blog Post...' in response.get_data(as_text=True)
        #             # assert 'This field is required.' in response.get_data(as_text=True)

        #             # Post OK
        #             is_logged_in = login_user(user, force=True, fresh=True)

        #             assert True == is_logged_in
        #             print('client.cookie_jar', client.cookie_jar)
        #             session['current_user'] = user

        #             response: flask.Response = client.post('/add-post', data=post_data)

        #             print(response.get_data(as_text=True))

        #             assert response.status_code == 200
        #             print(req.session)
        #             print('770flask_login.current_user', flask_login.current_user)
        #             assert 'Blog Post Submitted Successfully!' in response.get_data(as_text=True)
        #             assert post_data['title'] in response.get_data(as_text=True)


# # import os
# # import tempfile

# # import pytest


# # # from app import app


# # # @pytest.fixture
# # # def client():
# # #     db_fd, app.config['DATABASE'] = tempfile.mkstemp()
# # #     app.config['TESTING'] = True

# # #     with app.test_client() as client:
# # #         with app.app.app_context():
# # #             app.init_db()
# # #         yield client

# # #     os.close(db_fd)
# # #     os.unlink(app.app.config['DATABASE'])

# # from flask_sqlalchemy import SQLAlchemy

# # db = SQLAlchemy()
# # os.unlink(os.path.join(str(Path(__file__).parent.resolve()), db_path))
# os.unlink(db_full_path)
