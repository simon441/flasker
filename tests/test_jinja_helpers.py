from unittest import TestCase

import pytest
from markupsafe import Markup

from ..app import app
from ..utils.external import Roles
from ..utils.jinja_helpers import get_role, form_error_class, nl2br

# @pytest.fixture
# def client():
#     app.testing=True

#     with app.test_client() as client:


# class TestFilter(TestCase):
#     def setUp(self) -> None:
#         self.app = app

#         return super().setUp()

@pytest.mark.parametrize(
    ('klass, element_type, form_errors, request_method, expect'),
    (
        ('', '', dict(), '', ''),  # nothing

        # GET
        ('test-class', '', dict(), '', 'test-class'),  # just the klass name
        ('test-class', '', dict(test=''), 'GET', 'test-class'),  # class name + GET
        ('test-class', 'test', dict(test=''), 'GET', 'test-class'),  # class name + invalid element + GET
        ('test-class', 'element', dict(test=''), 'GET', 'test-class'),  # class name + valid element + GET + errors
        ('test-class', 'help', dict(test=''), 'GET', 'test-class'),  # class name + valid element + GET + errors

        # POST
        ('', '', dict(), 'POST', ''),  # no  klass name + POST + no element (no errors)
        ('', 'invalid-element', dict(), 'POST', ''),  # no  klass name + POST + invalid element (no errors)
        ('', 'element', dict(), 'POST', ' is-valid'),  # no class name + valid element + POST
        ('', 'help', dict(), 'POST', ' valid-feedback'),  # no class name + valid element + POST
        ('test-class', 'element', dict(), 'POST', 'test-class is-valid'),  # class name + valid element + POST
        ('test-class', 'help', dict(), 'POST', 'test-class valid-feedback'),  # class name + valid element + POST
        ('test-class', 'element', dict(test=''), 'POST', 'test-class is-invalid'),   # class name + element + POST + errors
        ('test-class', 'help', dict(test=''), 'POST', 'test-class invalid-feedback'),  # class name + help + POST + errors
    ),
)
def test_form_error_class_form_element(klass: str, element_type: str, form_errors: dict, request_method: str, expect: str):
    app.testing = True
    response = form_error_class(klass=klass, element_type=element_type,
                                form_errors=form_errors, request_method=request_method)
    assert response == expect


@pytest.fixture(scope='session')
def env():
    """returns a new environment."""
    return app.create_jinja_environment()


@pytest.mark.parametrize(
    'value,expect',
    (
        ('my test', Markup('<p>my test</p>'),),
        ('my test\nmy test', Markup('<p>my test<br/>my test</p>'),),
        ('my testé\n#%ù*$°i\n\r\n&z', Markup('<p>my testé<br/>#%ù*$°i</p><p>&amp;amp;z</p>',)),
    ),
)
def test_nl2br(env, value, expect):
    app.testing = True
    with app.app_context():
        assert expect == nl2br(env, value).replace('\n', '')


def test_get_role():
    assert Roles.admin.name in get_role().keys()
    assert Roles.user.name in get_role().keys()
    assert Roles.admin.value in get_role().values()
    assert Roles.user.value in get_role().values()
