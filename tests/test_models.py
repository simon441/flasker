from .config import SQL_DB_PATH_TEST
import datetime
import json
import logging
import os
import sqlite3
import sys

import pytest
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import Model, SQLAlchemy
from flask_testing import TestCase
import sqlalchemy
from sqlalchemy.orm.decl_api import declarative_base

pytestmark = pytest.mark.skip("all tests still WIP")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()

FIXTURES_PATH = os.path.join(SQL_DB_PATH_TEST, 'fixtures')
SQL_PATH = os.path.join(SQL_DB_PATH_TEST, '..', 'db',  "sqlschema_sqlite.sql")
SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(SQL_DB_PATH_TEST, "test_models.sqlite3")
os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI)
# if 1:
# from ..app import app, db
# from ..models.models import Posts, Users
# logger.debug(app)
# logger.debug(db)

logger.info(os.environ.get('FLASK_ENV'))
# logger.info(db)


class MyTest(TestCase):
    # SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(SQL_DB_PATH_TEST, "test.db")
    TESTING = True
    # Specify the fixtures file(s) you want to load.
    # Change the list below to ['authors.yaml'] if you created your fixtures
    # file using YAML instead of JSON.
    fixtures = ['users.json', 'posts.json']

    def create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        # app.config['FIXTURES_DIRS'] = FIXTURES_PATH
        # self.app = app
        return app

    def create_db(self):
        os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI)
        if self.app.testing:
            self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite+pysqlite:///' + \
                SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///') + '.dffrd.sqlite3'
        session_options = {}
        os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI)
        os.environ.setdefault('TESTING_MODELS', 'SQLALCHEMY')
        TESTING_MODELS = True
        engine = sqlalchemy.create_engine(
            'sqlite+pysqlite:///'+SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///') + '.sqlite3')
        from ..models import Users, Posts, db
        logger.info(db)

        print(type(Users), Posts)
        print('type')

        migrate = Migrate(self.app, db)
        db.create_all()
        logger.debug(db.engine)
        print('##################', '{{{{{{{{{{')
        # for i in Users.query.all():
        #     print(i)
        db.create_all()
        self.db = db
        return db

        # self.populate_db()
        with open(os.path.join(FIXTURES_PATH, 'users.json')) as f:
            data = json.loads(f.read())
            for i in data[0]['records']:
                print(i)
                i['date_added'] = datetime.datetime.fromisoformat(i['date_added'])
                db_user = Users(**i)
                self.db.session.add(db_user)
                logger.info(f'Adding user {db_user}')

            self.db.session.commit()
        logger.info('Added Users')

        with open(os.path.join(FIXTURES_PATH, 'posts.json')) as f:
            data = json.loads(f.read())
            for i in data[0]['records']:
                print(i)
                db_post = Posts(**i)
                self.db.session.add(db_post)
                logger.info(f'Adding post {db_post}')

            self.db.session.commit()
        logger.info('Added Posts')
        return self.db

    def populate_db(self):
        """Populate the Database"""
        print('populate_db')
        from ..models import Users, Posts
        # load_fixtures(self.db, os.path.join(FIXTURES_PATH, 'users.json'))

        with open(os.path.join(FIXTURES_PATH, 'users.json')) as f:
            data = json.loads(f.read())
            for i in data[0]['records']:
                print(i)
                i['date_added'] = datetime.datetime.fromisoformat(i['date_added'])
                db_user = Users(**i)
                self.db.session.add(db_user)
                logger.info(f'Adding user {db_user}')

            self.db.session.commit()
            logger.info('Added Users')

        with open(os.path.join(FIXTURES_PATH, 'posts.json')) as f:
            data = json.loads(f.read())
            for i in data[0]['records']:
                print(i)
                db_post = Posts(**i)
                self.db.session.add(db_post)
                logger.info(f'Adding post {db_post}')

            self.db.session.commit()
            logger.info('Added Posts')
        for i in Users.query.all():
            print('~~~~', i)
        logger.info(Users.query.all())

        # load_fixtures(self.db, os.path.join(FIXTURES_PATH, 'posts.json'))
        # with open(os.path.join(FIXTURES_PATH, 'users.sql'), encoding='utf8') as f:
        #     self.db.engine.execute(f.read())
        # with open(SQL_PATH, encoding='utf8') as f:
        #     db_u = sqlite3.connect(self.SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///'))
        #     db_u.executescript(f.read())
        #     db_u.commit()
        #     db_u.close()

    def setUp(self):
        # self.app = self.create_app()
        # app = Flask(__name__)
        # app.config['TESTING'] = True
        # app.config['SQLALCHEMY_DATABASE_URI'] = self.SQLALCHEMY_DATABASE_URI
        # app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        # app.config['FIXTURES_DIRS'] = FIXTURES_PATH
        # self.app = app
        # self.app = self.create_app()
        logging.info(self.app)
        # db = SQLAlchemy(self.app, session_options={})
        # logger.info(db)
        # migrate = Migrate(self.app, db)
        # logger.info(db.engine)
        # db.create_all()
        self.db = self.create_db()
        # db = self.create_db()
        # self.populate_db()

    def tearDown(self):
        self.db.session.remove()
        self.db.drop_all()

    @classmethod
    def tearDownClass(cls):
        # if os.path.exists(SQLALCHEMY_DATABASE_URI.removeprefix('sqlite:///')):
        logger.info(SQLALCHEMY_DATABASE_URI.removeprefix("sqlite:///"))
        os.remove(SQLALCHEMY_DATABASE_URI.removeprefix("sqlite:///"))

    def test_users(self):
        """Test Users Model"""
        from ..models import Posts, Users
        # users = Users.query.all()
        # assert len(users) == Users.query.count() == 2
