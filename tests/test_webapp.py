# import os
# import unittest
# from flask import current_app
# import flask
# from flask_sqlalchemy import SQLAlchemy
# path = os.path.join(os.path.dirname(__file__), '..', 'db',  "sqlschema_sqlite.sql")


# class TestWebApp(unittest.TestCase):
#     def _get_sql(self) -> str:
#         # read in SQL for populating test data
#         print('path', path)
#         with open(path, "rb") as f:
#             _data_sql = f.read().decode("utf8")
#             return _data_sql

#     def setUp(self):
#         app = flask.Flask(__name__)
#         app.config.update({"TESTING": True, 'SQLALCHEMY_TRACK_MODIFICATIONS': False})
#         app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///::memory::'
#         app.config['WTF_CSRF_ENABLED'] = False  # no CSRF during tests
#         self.app = app
#         self.appctx = self.app.app_context()
#         db = SQLAlchemy(app)
#         print(self._get_sql())
#         db.engine.execute(self._get_sql())
#         self.appctx.push()
#         self.client = self.app.test_client()

#     def tearDown(self):
#         self.appctx.pop()
#         self.app = None
#         self.appctx = None

#     def test_app(self):
#         assert self.app is not None
#         assert current_app == self.app

#     def test_registration_form(self):
#         response = self.client.get('/user/add')
#         assert response.status_code == 200
#         html = response.get_data(as_text=True)

#         # make sure all the fields are included
#         assert 'name="username"' in html
#         assert 'name="email"' in html
#         assert 'name="password_hash"' in html
#         assert 'name="confirm_password"' in html
#         assert 'name="password_confirm"' in html
#         assert 'name="submit_register"' in html
