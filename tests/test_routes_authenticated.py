import logging
from .config import ROOT_DIR_TEST, SQL_DB_PATH_TEST
from ..utils.functions import get_meta_for_form
from ..utils.external import Roles
from ..models import Posts, Users
from ..forms.webforms import PostForm
# from ..app import app, db
import json
import os
import sqlite3
import sys
import unittest
from datetime import datetime

import flask
import pytest
from flask import Flask
from flask_login import LoginManager, current_user, login_required, login_user
from flask_sqlalchemy import SQLAlchemy

pytestmark = pytest.mark.skip("all tests still WIP")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()


def login(client, username, password):
    return client.post('/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)


def logout(client):
    return client.post('/logout', follow_redirects=True)


def get_current_user():
    """Get the current user"""
    # def decorator(user_id):
    print(flask.request.headers, flask.request.headers.get('Authorization'))
    current_user = Users.query.get(int(flask.request.headers.get('Authorization')))
    print('cur', current_user)
    return current_user
    # return decorator


def encode(post: Posts) -> str:
    return f"id:<{post.id}>, title:<{post.title}>,content:<{post.content}>, date_posted:<{post.date_posted}>, slug:<{post.slug}>, poster_id:<{post.poster_id}"


# @pytest.fixture(scope='module', autouse=True)
# def finalize():
#     yield "a"
#     # if os.path.exists(RoutesAuthenticatedTestCase.DATABASE_PATH):
#     os.unlink(RoutesAuthenticatedTestCase.DATABASE_PATH)


class RoutesAuthenticatedTestCase(unittest.TestCase):
    ''' Tests for results of authenticated routes '''

    DATABASE_PATH = os.path.join(SQL_DB_PATH_TEST, 'db_test_auth.sqlite3')
    path = os.path.join(SQL_DB_PATH_TEST,  'fixtures',  "sqlschema_sqlite.sql")
    FIXTURES_PATH = os.path.join(SQL_DB_PATH_TEST, 'fixtures')

    def _get_sql(self) -> str:
        # read in SQL for populating test data
        print('path', self.path, os.path.exists(self.path))
        with open(self.path, "rb") as f:
            _data_sql = f.read().decode("utf8")
            return _data_sql

    def get_db(self):

        db_u = sqlite3.connect(os.path.join(SQL_DB_PATH_TEST, self.DATABASE_PATH))
        db_u.executescript(self._get_sql())
        db_u.commit()
        db_u.close()

    def setUp(self):
        self.app = flask.Flask(__name__)
        # self.app = app
        self.app.config['SECRET_KEY'] = 'deterministic'
        self.app.config['SESSION_PROTECTION'] = None
        self.remember_cookie_name = 'remember'
        self.app.config['REMEMBER_COOKIE_NAME'] = self.remember_cookie_name
        self.app.config['LOGIN_DISABLED'] = False
        self.app.config['WTF_CSRF_ENABLED'] = False  # no CSRF during tests
        self.app.config['DEBUG_TB_ENABLED'] = False
        self.app.config['WTF_I18N_ENABLED'] = False

        self.login_manager = LoginManager(self.app)

        @self.login_manager.user_loader
        def user_loader():
            return Users.query.get(1)

        # Db
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + self.DATABASE_PATH
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        # self.get_db()
        # self.db = db
        self.db = SQLAlchemy(self.app)

        # self.db.session.expunge_all()
        self.db.create_engine(engine_opts={}, sa_url='sqlite:///' + self.DATABASE_PATH)
        print('self.db.engine', self.db.engine)
        # Testing mode
        self.app.testing = True
        self.db.create_all()

        self._populate_db()

        self.user = Users.query.get(1)

    def tearDown(self) -> None:

        # if os.path.exists(self.DATABASE_PATH):
        #     os.unlink(self.DATABASE_PATH)
        self.db.session.remove()
        self.db.drop_all()

        return super().tearDown()

    # @classmethod
    # def tearDownClass(cls):
    #     if os.path.exists(os.path.join(SQL_DB_PATH_TEST, RoutesAuthenticatedTestCase.DATABASE_PATH)):
    #         os.unlink(os.path.join(SQL_DB_PATH_TEST, RoutesAuthenticatedTestCase.DATABASE_PATH))

    def _populate_db(self):
        from ..models import Posts, Users
        users = []
        with open(os.path.join(self.FIXTURES_PATH, 'users_new.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data
            logger.info(data)
            for i in data:
                print('i', i)
                print(i['date_added'])
                i['date_added'] = datetime.fromisoformat(i['date_added'])
                u = Users(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                users.append(u)
        posts = []
        with open(os.path.join(self.FIXTURES_PATH, 'posts.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data[0]['records']
            logger.info(data)
            for i in data:
                i['date_posted'] = datetime.fromisoformat(i['date_posted'])
                u = Posts(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                posts.append(u)
        return dict(users=users, posts=posts)
        # return u

    def test_add_post(self):
        @self.app.route('/add-post2', methods=['GET', 'POST'])
        def add_post():
            """ Add Post Page """

            form = PostForm()
            print('current_user', current_user, type(current_user), form.content.data, form.validate_on_submit())

            if form.validate_on_submit():
                poster = self.user.id
                post = Posts(
                    title=form.title.data, content=form.content.data, poster_id=poster, slug=form.slug.data)
                #  Clear the form
                form.title.data = ''
                form.content.data = ''
                form.slug.data = ''

                # Add post data to the database
                self.db.session.add(post)
                self.db.session.commit()
                return 'Blog Post Submitted Successfully!'
                return f'/post/{post.id}'

                flask.flash('Blog Post Submitted Successfully!', 'success')
                return flask.redirect(flask.url_for('post', id=post.id))
                # return redirect('posts')

            # Redirect to the webpage
            return 'add_post webpage'

        print(self.user)

        # login_user(user=self.user)
        with self.app.test_client() as client:
            with self.app.app_context():
                login(client, 'simon', 'p')
                post_data = dict(
                    title='test post',
                    content='test post content',
                    slug='text-post-content',
                )
                response: flask.Response = client.post('/add-post2', data=post_data, follow_redirects=True)

                print(response.get_data(as_text=True))

                assert response.status_code == 200

                # print('770flask_login.current_user', self.app.current_user)
                self.assertEqual('Blog Post Submitted Successfully!', response.get_data(as_text=True))

    def test_edit_post(self):
        """Edit a post"""

        @self.app.route('/posts/edit2/<int:id>', methods=['GET', 'POST'])
        # @get_current_user
        def edit_post(id: int):
            current_user = get_current_user()
            print('current_user', current_user)
            post: Posts = Posts.query.get_or_404(id)
            form = PostForm(meta=get_meta_for_form())
            # If current user is the one who create this post is current user is an admin
            if current_user.id == post.poster_id or Users.query.get(current_user.id).role == Roles.admin.value:
                # Method is POST  and form is valid
                if form.validate_on_submit():
                    post.title = form.title.data
                    post.content = form.content.data
                    post.slug = form.slug.data

                    # Update database
                    # self.db.session.add(post)
                    self.db.session.commit()
                    flask.flash('Post Has Been Updated!')
                    return encode(post)

                form.title.data = post.title
                form.content.data = post.content
                form.slug.data = post.slug

                return 'edit_post.html'

            else:
                return "You aren't authorized to edit this post..."

        with self.app.test_client() as client:
            with self.app.app_context():
                login(client, 'simon', 'p')

                post_data = dict(id=1,
                                 title='Post 1',
                                 content='<p>My first post<br />',
                                 date_posted='2021-09-24 14:56:08',
                                 slug='post-1',
                                 poster_id=1
                                 )
                print(post_data['id'])

                the_post = Posts.query.get(1)

                # the_post = Posts()
                the_post.title = post_data['title']
                the_post.poster_id = post_data['poster_id']
                the_post.slug = post_data['slug']
                the_post.content = post_data['content']
                # the_post.date_posted

                response: flask.Response = client.post(
                    f'/posts/edit2/{post_data["id"]}', data=post_data, headers={'Authorization': '1'}, follow_redirects=True)

                # print(response.get_data(as_text=True))
                self.assertIn('Post Has Been Updated!', flask.get_flashed_messages())

                assert response.status_code == 200

                print('770flask_login.current_user', current_user)
                print(encode(the_post), response.get_data(as_text=True), '#')
                self.assertEqual(encode(the_post), response.get_data(as_text=True), '#')

                # The user wanting to edit is not authorized
                response: flask.Response = client.post(
                    f'/posts/edit2/{post_data["id"]}', data=post_data, headers={'Authorization': '2'}, follow_redirects=True)
                print(response.get_data(as_text=True))
                self.assertEqual("You aren't authorized to edit this post...", response.get_data(as_text=True))
                self.db.session.expunge_all()

    def test_delete_post(self):
        """Delete a post"""

        @self.app.route('/posts/delete2/<int:id>', methods=['POST'])
        def delete_post(id):
            """ Delete a Post """
            current_user = get_current_user()
            post_to_delete: Posts = Posts.query.get_or_404(id)
            print(post_to_delete.poster_id, current_user.id)
            user_id = current_user.id

            user: Users = Users.query.get(user_id)
            print(user, user.role)
            # Current user created the post
            if user_id == post_to_delete.poster.id or user.role == Roles.admin.value:
                # try:
                self.db.session.delete(post_to_delete)
                self.db.session.commit()
                self.db.session.expunge_all()
                self.db.session.close()

                # Return a message
                return 'Blog Post Has Been Deleted!'
                # except Exception:
                #     return 'Whoops! There was a problem deleting post, try again...'
            else:
                # Return a message
                return 'You are not authorized to delete this post!'

        with self.app.test_client() as client:
            # with self.app.app_context():
            login(client, 'simon', 'p')
            self.db.session.close()

            # All ok
            # response: flask.Response = client.post(
            #     f'/posts/delete/2',  headers={'Authorization': '2'}, follow_redirects=True)

            # self.assertEqual('Blog Post Has Been Deleted!', response.get_data(as_text=True))

            # assert response.status_code == 200

            # Post does not exist
            response: flask.Response = client.post(
                f'/posts/delete2/0',  headers={'Authorization': '1'}, follow_redirects=True)
            self.assertEqual(404, response.status_code)

            # The user wanting to delete is not authorized
            response: flask.Response = client.post(
                f'/posts/delete2/1', headers={'Authorization': '2'}, follow_redirects=True)
            print(response.get_data(as_text=True))
            self.assertEqual('You are not authorized to delete this post!', response.get_data(as_text=True))
            # self.db.session.expunge_all()
