from slugify import slugify
from collections import defaultdict
from datetime import datetime
import json
import os
from random import random
import sqlite3
import sys
from unittest import TestCase
from urllib.parse import urlencode
from faker import Faker
import flask_login
import pytest
from werkzeug.urls import url_encode
from flask_webtest import TestApp, TestResponse, get_scopefunc, SessionScope
import webtest
import logging
from flask_sqlalchemy import SQLAlchemy, BaseQuery
import flask

from .config import ROOT_DIR_TEST, SQL_DB_PATH_TEST

# from flask_fixtures import FixturesMixin, load_fixtures, load_fixtures_from_file

FIXTURES_PATH = os.path.join(SQL_DB_PATH_TEST, 'fixtures')
SQL_PATH = os.path.join(ROOT_DIR_TEST, 'db',  "sqlschema_sqlite.sql")
UPLOAD_PATH = os.path.join(FIXTURES_PATH,  'uploads')


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()

path = SQL_DB_PATH_TEST
# SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(FIXTURES_PATH, "test.db")
# "sqlite:///" + os.path.join(path, 'fixtures', "db_test.sqlite3")
SQLALCHEMY_DATABASE_URI_ = f"sqlite:///{SQL_DB_PATH_TEST}/db_test_0002.sqlite3"

SQL_DB_PATH = SQL_DB_PATH_TEST

ROWS_PER_PAGE = 13

logger.debug(dict(SQL_DB_PATH=SQL_DB_PATH))


def remove_db_files():
    pass
# for i in os.listdir(SQL_DB_PATH_TEST):
#     if i.endswith('_test.sqlite3'):
#         os.remove(os.path.join(SQL_DB_PATH_TEST, i))


if True:

    # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    #     os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))

    os.environ.setdefault('test', 'True')
    os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI_)
    SQL_DATABASE_ALCHEMY = SQLALCHEMY_DATABASE_URI_

    from ..app import app

    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI_
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['FIXTURES_DIRS'] = FIXTURES_PATH
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['DEBUG_TB_ENABLED'] = False
    app.config['UPLOAD_FOLDER'] = UPLOAD_PATH
    app.config['ROWS_PER_PAGE'] = ROWS_PER_PAGE
    from ..app import db


def gettext(text: str) -> str:
    return text


def make_db(app):
    session_options = {}
    if app.testing:
        session_options['scopefunc'] = get_scopefunc()
    return SQLAlchemy(app, session_options=session_options)


def get_current_lang_in_session():
    return 'en'


class ExampleTest(TestCase):
    def setUp(self):
        app.config['ROWS_PER_PAGE'] = ROWS_PER_PAGE
        self.app = app
        self.w = TestApp(self.app, db=db, use_session_scopes=True)
        self.app_context = app.app_context()
        self.app_context.push()
        # db.create_all()

        self.db = db
        self.db.create_engine(engine_opts={}, sa_url=SQL_DATABASE_ALCHEMY)
        logger.info(self.db.engine)
        self.db.create_all()
        self.models = self._populate_db()
        # with open(SQL_PATH, encoding='utf8') as f:
        #     db_u = sqlite3.connect(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
        #     db_u.executescript(f.read())
        #     db_u.commit()
        #     db_u.close()
        #     logger.info('Created db')

    def tearDown(self):
        db.drop_all()
        self.app_context.pop()
        db.session.remove()
        db.drop_all()

    def populate_db(self):
        '''Populate the database
        '''
        with open(SQL_PATH, encoding='utf8') as f:
            db_u = sqlite3.connect(
                SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
            db_u.executescript(f.read())
            db_u.commit()
            db_u.close()

    # @classmethod
    # def tearDownClass(cls):
        # if SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///').count('flasker.sqlite3') == 0:
        #     os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///').removeprefix('sqlite:///'))

    def _populate_db(self):
        from ..models import Posts, Users
        users = []
        with open(os.path.join(FIXTURES_PATH, 'users.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data[0]['records']
            logger.info(data)
            for i in data:
                i['date_added'] = datetime.fromisoformat(i['date_added'])
                u = Users(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                users.append(u)
        posts = []
        with open(os.path.join(FIXTURES_PATH, 'posts.json'), encoding='utf-8') as f:
            file_data = json.loads(f.read())
            data = file_data[0]['records']
            logger.info(data)
            for i in data:
                i['date_posted'] = datetime.fromisoformat(i['date_posted'])
                u = Posts(**i)
                self.db.session.add(u)
                self.db.session.commit()
                logger.info(i)
                logger.info(u)
                posts.append(u)
        return dict(users=users, posts=posts)
        # return u

    def test_0index(self):

        logger.info(self.w.cookies)
        r: TestResponse = self.w.get('/')
        encoder = flask.Flask.json_encoder()
        # r.set_cookie('flasker', encoder.encode({"lang": "fr"}), httponly=True, domain='127.0.0.1')
        logger.info(r.headers)
        r.session['lang'] = 'en'
        logger.info(r.session)
        # Assert there was no messages flushed:
        self.assertFalse(r.flashes)
        # Access and check any variable from template context...
        logger.info(r.context['s'])
        self.assertEqual(r.context['first_name'], 'Simon')
        self.assertEqual(r.context['favorite_pizza'], [
                         'Pepperoni', gettext('Cheese'), gettext('Mushrooms'), 42])
        self.assertEqual(r.context['first_name'], 'Simon')
        self.assertEqual(r.template, 'index.html')
        # ...and from session
        self.assertNotIn('user_id', r.session)

    def test_1login_passed_with_good_credentials(self):
        from ..models import Users
        from ..forms.webforms import LoginForm
        logger.info(db.engine)
        logger.info(SQLALCHEMY_DATABASE_URI_)
        logger.info(path)
        logger.info(SQL_DB_PATH)
        # self.assertFalse(True)
        # Get login form
        r = self.w.get('/login')
        f: webtest.Form = r.forms['login']
        csrf_token = f.fields['csrf_token']
        logger.debug(dir(csrf_token))
        logger.info(Users.query.all())
        f['username'] = 'simon'
        f['password'] = 'p'

        form = LoginForm()
        # r = self.w.post('/login', dict(username='simon', password='p', submit_login="Log in", csrf_token=csrf_token))
        r: TestResponse = f.submit(form.submit_login.data)
        # Assert there was no messages flushed:
        # self.assertEqual([('success', 'Login successful!')], r.flashes)
        logging.debug(r.flashes)
        logging.info(r)
        # Access and check any variable from template context...
        res = r.follow()
        self.assertIn('Dashboard', str(res))
        # ...and from session
        self.assertIn('_user_id', r.session)

    def test_login_fail_with_wrong_username(self):
        r = self.login(username='noone')
        # Assert there was no messages flushed:
        self.assertEqual(
            [('danger', ('Invalid credentials. Try Again'))], r.flashes)
        self.assertEqual(r.template, 'login.html')

    def test_login_fail_with_wrong_password(self):
        r = self.login(username='simon', password='notherightpassword')
        # Assert there was no messages flushed:
        self.assertEqual(
            [('danger', ('Invalid credentials. Try Again'))], r.flashes)
        self.assertEqual(r.template, 'login.html')

    def test_logout(self):
        r = self.login()
        r = self.w.post('/logout', status=302)
        # Assert there was no messages flushed:
        self.assertEqual(
            [('success', ('You have been logged out!\nThanks for stopping by...'))], r.flashes)
        res = r.follow()
        self.assertEqual(res.template, 'login.html')
        self.assertIn('Login', str(res))

    def test_posts(self):
        import sqlalchemy
        from ..models import Posts
        r = self.w.get('/posts')
        posts = Posts.query.options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(sqlalchemy.desc(Posts.date_posted)).limit(ROWS_PER_PAGE).all()
        for post in posts:
            logger.info(post)
        self.assertEqual(r.context['posts'].items[0].id, posts[0].id)
        for i, v in enumerate(r.context['posts'].items):
            self.assertEqual(v.id, posts[i].id)
            self.assertEqual(v.title, posts[i].title)
            self.assertEqual(v.content, posts[i].content)
            self.assertEqual(v.slug, posts[i].slug)
            self.assertEqual(v.date_posted, posts[i].date_posted)
        self.assertEqual(r.template, 'posts.html')

    def test_posts_first_page_only(self):
        import sqlalchemy
        from ..models import Posts
        r = self.w.get('/posts?p=1')
        posts = Posts.query.options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(sqlalchemy.desc(Posts.date_posted)).limit(ROWS_PER_PAGE).all()
        for post in posts:
            logger.info(post)
        self.assertEqual(r.context['posts'].items[0].id, posts[0].id)
        self.assertEqual(r.template, 'posts.html')

    def test_posts_second_page_with_page_num(self):
        import sqlalchemy
        from ..models import Posts
        self.app.config['ROWS_PER_PAGE'] = 13
        self._populate_records(50)
        r = self.w.get('/posts?page=2')
        date_ = self._get_skipped_page(2)
        posts = Posts.query\
            .filter(Posts.date_posted >= date_)\
            .options(
                sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
            ).order_by(Posts.date_posted.desc(), Posts.id).limit(self.app.config['ROWS_PER_PAGE']).offset(0).all()
        logger.info(str(posts))
        logger.info({14, ROWS_PER_PAGE})
        logger.info(posts)
        logger.info(r.context['posts'].items)
        self.assertEqual(r.context['posts'].items[0].id, posts[0].id)
        self.assertEqual(r.template, 'posts.html')

    def test_posts_secondpage_page_with_date(self):
        import sqlalchemy
        from ..models import Posts
        self._populate_records(50)
        posts = Posts.query.options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(Posts.date_posted.desc()).limit(ROWS_PER_PAGE).all()
        r: TestResponse = self.w.get(
            '/posts', params=dict(page=2, cur=posts[-1].date_posted))
        posts = Posts.query.filter(
            Posts.date_posted > r.request.params.get('cur')
        ).options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(Posts.date_posted.desc()).limit(ROWS_PER_PAGE).all()
        for post in posts:
            logger.info(post)
        self.assertEqual(r.context['posts'].items[0].id, posts[0].id)
        self.assertEqual(r.template, 'posts.html')

    def test_posts_secondpage_page_from_third(self):
        import sqlalchemy
        from ..models import Posts
        self._populate_records(50)
        selected = Posts.query.with_entities(
            Posts.date_posted).limit(1).offset(3)
        posts = Posts.query.options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(Posts.date_posted.desc(), Posts.id).limit(26).all()
        r: TestResponse = self.w.get(
            '/posts', params=dict(page=3, cur=posts[-1].date_posted, direction='asc'))

        selected = Posts.query.with_entities(Posts.date_posted, Posts.id).order_by(
            Posts.date_posted, Posts.id).limit(1).offset((int(r.request.params.get('page'))-1) * ROWS_PER_PAGE).all()[0]
        posts = Posts.query.filter(
            Posts.date_posted >= selected[0]
        ).options(
            sqlalchemy.orm.joinedload(Posts.poster).load_only('id', 'name')
        ).order_by(Posts.date_posted.desc(), Posts.id).limit(ROWS_PER_PAGE)
        logger.info(r.request.params.get('page'))
        logger.info(posts)
        posts = posts.all()
        logger.debug(r.context['posts'].items)
        logger.debug(selected)
        for post in posts:
            logger.info(post)
        self.assertEqual(r.context['posts'].items[0].id, posts[0].id)
        self.assertEqual(r.template, 'posts.html')

    def test_post_does_not_exists_404(self):
        r = self.w.get('/posts/0', status=404, expect_errors=True)
        self.assertEqual(404, r.status_code)

    def test_post_exists(self):
        from ..models import Posts
        r = self.w.get('/posts/1')
        post = Posts.query.get(1)
        # Same query
        self.assertEqual(str(r.context['post']), str(post))
        self.assertEqual(r.template, 'post.html')

    def test_json(self):
        r = self.w.get('/json')
        self.assertEqual(200, r.status_code)
        favorite_pizza = {
            "Jane": "Pepperonni",
            "Mary": "Cheese",
            "Stuart": "Veggie",
            "Tim": "Mushroom"
        }
        logger.info(str(r))
        logger.info(r.body.decode('UTF-8'))
        self.assertEqual(favorite_pizza, json.loads(r.body.decode('UTF-8')))

    def test_add_post_get_failed_without_login(self):
        from ..forms.webforms import PostForm
        r: TestResponse = self.w.get('/add-post', expect_errors=True)
        logger.info(r.location)
        self.assertEqual(
            f'http://localhost/login?{urlencode({"next": "/add-post"})}', r.location)

        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)

    def test_add_post_get_success_with_correct_login(self):
        from ..forms.webforms import PostForm
        r = self.login()
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        form = PostForm()
        self.assertEqual(
            repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(
            repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(
            repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(
            repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_add_post_get_and_post(self):
        from ..models import Posts
        from ..forms.webforms import PostForm

        # Get all Posts
        posts: Posts = Posts.query.all()
        # with self.app.test_request_context() as request:
        #     # The post form
        #     form = PostForm()

        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        form = PostForm()

    #     # Form content
        form_content = dict(
            title='Form title test',
            # slug='form-test-one',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        f: webtest.Form = r.forms['add-post']
        for i in f.fields:
            print(i)
        f['title'] = form_content['title']
        # f['slug'] = form_content['slug']
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        # Get the last inserted post
        post: Posts = Posts.query.filter_by(
            title=form_content['title']).first()

        # Check flash messages
        logger.info(f'location type: {r.location}')
        self.assertTrue(r.flashes)
        # There is a redirect to the @post page
        self.assertEqual(r.status_code, 302)

        # Follow redirect
        r = r.follow()
        # Post Page
        self.assertEqual(r.status_code, 200)
        self.assertFalse(r.flashes)
        self.assertIn('Blog Post Submitted Successfully!', r.text)

        # Get all new Posts
        new_posts = Posts.query.all()

        # The is one more new post
        self.assertEqual(len(posts)+1, len(new_posts))

        self.assertIn(form_content['title'], r.text)

    def test_add_post_no_all_fields_ok(self):
        """Try to post a form with missing content returns
        an error message
            """
        from ..forms.webforms import PostForm

        # Login
        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        #  Post Form
        form = PostForm()

        # Form content
        form_content = dict(
            title='Form title test',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        logger.info(r.text)

        # Missing content
        f: webtest.Form = r.forms['add-post']
        f['title'] = form_content['title']
        # f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        self.assertEqual(r.context['form'].content.data, '')
        self.assertEqual(r.context['form'].title.data, form_content['title'])

        self.assertIn(
            'There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check the title is back in the form
        self.assertIn(form_content['title'], r.text)

        # Missing title
        f: webtest.Form = r.forms['add-post']
        f['slug'] = ''
        f['title'] = ''
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        self.assertEqual(
            r.context['form'].content.data, form_content['content'])
        self.assertEqual(r.context['form'].title.data, '')
        self.assertGreater(len(r.context['form'].title.errors), 0)

        self.assertIn(
            'There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check error field on title
        # self.assertIn('"validationTitleFeedback" class=" invalid-feedback"', r.text)

    def test_edit_post_get_failed_without_login(self):
        from ..forms.webforms import PostForm
        r: TestResponse = self.w.get('/posts/edit/1', expect_errors=True)
        logger.info(r.location)
        logger.info(r)
        self.assertEqual(
            f'http://localhost/login?{urlencode({"next": "/posts/edit/1"})}', r.location)

        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)
        # self.assertEqual(r.template, 'edit_login.html')
        # form = PostForm()
        # self.assertEqual(r.context['form'], form)

    def test_edit_post_get_success_with_correct_login_and_correct_user(self):
        from ..models import Posts
        from ..forms.webforms import PostForm
        logger.debug(
            'test_edit_post_get_success_with_correct_login_and_correct_user')
        self._get_posts()
        r = self.login(username='tim', password='password')
        r: TestResponse = self.w.get('/posts/edit/2', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        form = PostForm()
        post: Posts = Posts.query.get(2)
        form.slug.data = post.slug
        form.title.data = post.title
        form.content.data = post.content
        # print(app.login_manager)
        # logger.info(flask.session)
        # assert app.login_manager.current_user.id == post.poster_id
        self.assertEqual(
            repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(
            repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(
            repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(
            repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_edit_post_get_success_with_correct_login_user_id_admin(self):
        from ..models import Posts
        from ..forms.webforms import PostForm

        # initialize variables
        post_id = 2
        post_url = f'/posts/edit/{post_id}'

        # login
        r = self.login()

        # GET Post
        r: TestResponse = self.w.get(post_url, status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        form = PostForm()
        post: Posts = Posts.query.get(post_id)
        form.slug.data = post.slug
        form.title.data = post.title
        form.content.data = post.content
        self.assertEqual(
            repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(
            repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(
            repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(
            repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_edit_post_does_not_exists_404(self):
        r = self.login()
        r: TestResponse = self.w.get('/posts/edit/0', status=404)
        self.assertEqual(404, r.status_code)

    def test_edit_post_get_and_post(self):
        from ..forms.webforms import PostForm
        from ..models import Posts

        # initialize variables
        post_id = 2
        post_url = f'/posts/edit/{post_id}'

        # Get all Posts
        post: Posts = Posts.query.get(2)

        # Login
        r = self.login(username='tim', password='password')

        # Add a post (GET)
        r: TestResponse = self.w.get(post_url, status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        #  Post Form
        form = PostForm()

        # Form content
        form_content_replace = dict(
            id=post.id,
            title='Form title test',
            slug='form-test-one',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        f: webtest.Form = r.forms['edit-post']
        for i in f.fields:
            print(i)
        f['title'] = form_content_replace['title']
        f['slug'] = form_content_replace['slug']
        f['content'] = form_content_replace['content']

        r: TestResponse = f.submit(form.submit.name)

        # Get the last inserted post
        # post: Posts = Posts.query.filter_by(title=form_content['title']).first()

        # Check flash messages
        logger.info(f'location type: {r.location}')
        self.assertTrue(r.flashes)
        # There is a redirect to the @post page
        self.assertEqual(r.status_code, 302)

        # Follow redirect
        r = r.follow()
        # Post Page
        self.assertEqual(r.status_code, 200)
        self.assertFalse(r.flashes)
        self.assertIn('Post Has Been Updated!', r.text)

        # Get all new Posts
        new_post: Posts = Posts.query.get(2)

        # The is one more new post
        self.assertEqual(form_content_replace['content'], new_post.content)
        self.assertEqual(sluglify_title(
            form_content_replace['title']), new_post.slug)
        self.assertEqual(form_content_replace['title'], new_post.title)
        # self.assertEqual(post.date_posted, new_post.date_posted)
        self.assertEqual(post.id, new_post.id)

        self.assertIn(form_content_replace['title'], r.text)
        self.assertIn(form_content_replace['content'], r.text)
        # self.assertIn(form_content_replace['slug'], r.text)
        self.assertIn(f"/posts/edit/{form_content_replace['id']}", r.text)

    def test_edit_post_no_all_fields_ok(self):
        """Try to post a form with missing content returns
        an error message
            """
        from ..forms.webforms import PostForm
        from ..models import Posts

        # initialize variables
        post_id = 1
        post_url = f'/posts/edit/{post_id}'

        # Get all Posts
        post: Posts = Posts.query.get(post_id)

        # Login
        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get(post_url, status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        #  Post Form
        form = PostForm()

        # Form content
        form_content = dict(
            title='Form title test',
            content="""Test post <p>Content</p>""",
            slug="my-slug-test",
            submit=form.submit.data,
        )

        # Get the post form on the page
        logger.info(r.text)

        # Missing content
        f: webtest.Form = r.forms['edit-post']
        f['title'] = form_content['title']
        f['slug'] = form_content['slug']
        f['content'] = ''

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        self.assertEqual(r.context['form'].content.data, '')
        self.assertEqual(r.context['form'].title.data, form_content['title'])
        self.assertEqual(r.context['form'].slug.data, form_content['slug'])

        self.assertIn(
            'There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check the title is back in the form
        self.assertIn(form_content['title'], r.text)

        # Missing title
        f: webtest.Form = r.forms['edit-post']
        f['slug'] = 'test-meemmdrggd'
        f['title'] = ''
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        self.assertEqual(
            r.context['form'].content.data, form_content['content'])
        self.assertEqual(r.context['form'].title.data, '')
        self.assertGreater(len(r.context['form'].title.errors), 0)

        self.assertIn(
            'There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check error field on title
        # self.assertIn('"validationTitleFeedback" class=" invalid-feedback"', r.text)

    def test_delete_post_get_failed_without_login(self):
        r: TestResponse = self.w.post('/posts/delete/1', expect_errors=True)
        logger.info(r.location)
        logger.info(r)
        self.assertEqual(
            f'http://localhost/login?{urlencode({"next": "/posts/delete/1"})}', r.location)

        # self.assertIn([('danger', flask_login.LOGIN_MESSAGE)], r.flashes)
        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)

    def test_delete_post_get_success_with_correct_login_and_correct_user(self):
        from ..models import Posts
        # initialize variables
        post_id = 2
        post_url = f'/posts/delete/{post_id}'

        # Login
        r = self.login(username='tim', password='password')
        posts = Posts.query.all()

        for post in posts:
            logger.debug([post, post.id])

        post: Posts = Posts.query.get(post_id)

        # scope db session for isolating deletion
        with SessionScope(self.w.db):
            r: TestResponse = self.w.post(post_url, status=302)

            # Post does not exists
            post = Posts.query.get(post_id)
            self.assertIsNone(post)

        self.assertIn('/posts', r.text)

        self.assertEqual(302, r.status_code)
        self.assertIn('Blog Post Has Been Deleted!', r.flashes[0])

    def test_delete_post_get_success_with_correct_login_user_id_admin(self):
        # initialize variables
        post_id = 2
        post_url = f'/posts/delete/{post_id}'

        # Login
        r = self.login()

        # scope db session for isolating deletion
        with SessionScope(self.w.db):
            r: TestResponse = self.w.post(post_url, status=302)
            self.assertIn('Blog Post Has Been Deleted!', r.flashes[0])
            r.follow()

            self.assertIn('"/posts"', r.text)

    def test_delete_post_does_not_exists_404(self):

        # initialize variables
        post_id = 0
        post_url = f'/posts/delete/{post_id}'

        # Login
        r = self.login()

        r: TestResponse = self.w.post(post_url, status=404)
        self.assertEqual(404, r.status_code)

    def test_delete_post_get_is_405(self):
        r = self.login()
        r: TestResponse = self.w.get('/posts/delete/1', status=405)

    def test_search_empty(self):
        from ..forms.webforms import SearchForm
        url_path = '/search'

        r: TestResponse = self.w.get('/')
        f: webtest.Form = r.forms['search-form']

        f['q'] = ''
        # search
        r = f.submit(name='search')
        form = SearchForm()

        # data
        self.assertIsNone(form.q.data)
        self.assertEqual(r.context['q'], '')
        self.assertDictEqual(r.context['posts'], {})

        # template
        self.assertEqual(r.template, 'search.html')
        self.assertIn('0 post found', r.text)
        self.assertIn('<h2 class="">You searched for: <em></em></h2>', r.text)

    def test_search_empty(self):
        from ..models import Posts
        from ..forms.webforms import SearchForm
        url_path = '/search'

        r: TestResponse = self.w.get('/')
        f: webtest.Form = r.forms['search-form']
        csrf_token = f['csrf_token']

        f['q'] = 'post'
        # search
        r = f.submit()
        form = SearchForm()
        form.q.data = r.forms['search-form']['q']
        print(form.q)

        posts: BaseQuery = Posts.query.filter(Posts.content.like(f'%post%'))
        logger.info(Posts.content.like(f'%{form.q.data}%'))
        logger.info(r.forms['search-form']['q'])
        logger.info(f"%{form.q.data}%")

        # data
        self.assertEqual(r.context['q'], f['q'].value)

        self.assertEqual(len(r.context['posts'].items), len(posts.all()))
        r_context_posts = r.context['posts'].items
        # r_context_posts.sort()
        r_posts = posts.all()

        r_context_posts = sorted([i.id for i in r_context_posts])
        r_posts = sorted([i.id for i in r_posts])

        # r_posts.sort()
        self.assertListEqual(r_context_posts, r_posts)

        # template
        self.assertEqual(r.template, 'search.html')
        self.assertIn(f'{len(posts.all())} posts found', r.text)
        self.assertIn(
            '<h2 class="">You searched for: <em>post</em></h2>', str(r.body))

    def test_change_language_get_no_change(self):
        from ..forms.webforms import LanguageSelectForm

        this_page = '/login'
        r: TestResponse = self.w.get(this_page)
        logger.info(r.session)
        f: webtest.Form = r.forms['change_language']
        form = LanguageSelectForm()
        f[form.language_selection.id] = 'fr'
        self.assertIn('/login', f['next'].value)
        f.method = 'GET'
        r = f.submit()
        logger.info(r.session)
        r = r.follow()

        self.assertEqual(r.status_code, 200)
        self.assertEqual('en', r.session['lang'])

    def test_change_language_fr(self):
        from ..forms.webforms import LanguageSelectForm, LANGUAGES
        this_page = '/login'
        r: TestResponse = self.w.post(this_page)
        f: webtest.Form = r.forms['change_language']
        form = LanguageSelectForm()
        f[form.language_selection.id] = 'fr'
        self.assertIn('/login', f['next'].value)

        r = f.submit()

        self.assertEqual('fr', r.session['lang'])
        logging.info(r.headers)
        for i in enumerate(r.headerlist):
            if 'Cookie' in i:
                self.assertIn('flasker="{\\"lang\\": \\"fr\\"}', i[1])

        r = r.maybe_follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn('Connexion', r.text)

    def test_change_language_en(self):
        from ..forms.webforms import LanguageSelectForm, LANGUAGES
        self.test_change_language_fr()
        this_page = '/posts/1'
        r: TestResponse = self.w.get(this_page)
        f: webtest.Form = r.forms['change_language']
        form = LanguageSelectForm()
        f[form.language_selection.id] = 'en'
        self.assertIn('/posts/1', f['next'].value)

        r = f.submit()

        self.assertEqual('en', r.session['lang'])
        logging.info(r.headers)
        for i in enumerate(r.headerlist):
            if 'Cookie' in i:
                self.assertIn('flasker="{\\"lang\\": \\"en\\"}', i[1])

        r = r.maybe_follow()
        self.assertEqual(r.template, 'post.html')
        self.assertIn('Back To Blog', r.text)

    def test_add_user_ok(self):
        from ..models import Users
        from ..forms.webforms import UserForm

        # GET add user page
        r: TestResponse = self.w.get('/user/add')

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))
        self.assertIsNone(r.context['name'])

        f: webtest.Form = r.forms['add-user']
        f['name'] = 'me'
        f['username'] = 'narunaru'
        f['email'] = 'naru@test.tld'
        f['favorite_color'] = ''
        f['password_hash'] = 'wqaze'
        f['password_confirm'] = 'wqaze'

        # POST add user page
        r = f.submit()

        users = Users.query.all()

        self.assertEqual(r.status_code, 200)
        self.assertIn("User Added Successfully", r.flashes[0])

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))
        new_user: Users = Users.query.filter_by(username='narunaru').first()

        self.assertIsInstance(new_user, Users)
        self.assertEqual(new_user.role, 'user')

        # Submit when user already exists
        f['name'] = 'mqqe'
        f['username'] = 'narqqunaru'
        f['email'] = 'naru@test.tld'
        f['favorite_color'] = 'ferezr'
        f['password_hash'] = 'wqaze'
        f['password_confirm'] = 'wqaze'

        # POST add user page
        r = f.submit()

        self.assertEqual(r.status_code, 200)
        self.assertIn('User already exists', r.flashes[0])

        self.assertEqual(r.template, 'add_user.html')

    def test_add_user_fields_with_errors_missing(self):
        from ..models import Users
        from ..forms.webforms import UserForm

        # GET add user page
        r: TestResponse = self.w.get('/user/add')

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))
        self.assertIsNone(r.context['name'])

        f: webtest.Form = r.forms['add-user']
        f['name'] = ''
        f['username'] = ''
        f['email'] = ''
        f['favorite_color'] = ''
        f['password_hash'] = ''
        f['password_confirm'] = ''

        # POST add user page
        r = f.submit()

        users = Users.query.all()

        self.assertEqual(r.status_code, 200)
        self.assertIn("Error", r.flashes[0])

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))

        for i in r.context['form']:
            logger.warning([i, i.data, i.errors])

        f: webtest.Form = r.forms['add-user']
        f['name'] = ''
        f['username'] = ''
        f['email'] = ''
        f['favorite_color'] = ''
        f['password_hash'] = ''
        f['password_confirm'] = ''

        # POST add user page
        r = f.submit()

        users = Users.query.all()

        self.assertEqual(r.status_code, 200)
        form: UserForm = r.context['form']
        self.assertIn('This field is required.', form.username.errors)
        self.assertIn('This field is required.', form.name.errors)
        self.assertIn('This field is required.', form.email.errors)
        self.assertIn('This field is required.', form.password_hash.errors)
        self.assertIn('This field is required.', form.password_confirm.errors)
        self.assertIn("Error", r.flashes[0])

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))

        f: webtest.Form = r.forms['add-user']
        f['name'] = 'tim'
        f['username'] = 'tim'
        f['email'] = 'simon@test.com'
        f['favorite_color'] = ''
        f['password_hash'] = 'pass'
        f['password_confirm'] = 'notthesamepassword'

        # POST add user page
        r = f.submit()

        users = Users.query.all()

        self.assertEqual(r.status_code, 200)
        form: UserForm = r.context['form']
        self.assertEqual(len(form.username.errors), 0)
        self.assertEqual(len(form.name.errors), 0)
        self.assertEqual(len(form.email.errors), 0)
        self.assertIn('Passwords must match!', form.password_hash.errors)
        self.assertEqual(len(form.password_confirm.errors), 0)
        self.assertIn("Error", r.flashes[0])

        self.assertEqual(r.template, 'add_user.html')
        self.assertEqual(self._get_ids(Users.query.all()),
                         self._get_ids(r.context['our_users']))

    def test_edit_user(self):
        from ..models import Users
        from ..forms.webforms import UserForm

        # variables
        user_id = 1
        url_base_path = '/update/'
        url_path = f'{url_base_path}{user_id}'

        # login
        r = self.login()

        # get the users' update page
        r: TestResponse = self.w.get(url_path)
        form_data = dict(
            username='',

        )
        user: Users = Users.query.get(2)

        f: webtest.Form = r.forms['update-user']
        f['name'] = 'me'
        f['username'] = 'narunaru'
        f['email'] = 'naru@test.tld'
        f['favorite_color'] = ''
        f['role'] = 'admin'

        # POST add user page
        r = f.submit()

        updated_user: Users = Users.query.get(1)

        self.assertEqual(r.status_code, 200)
        self.assertIn("User Updated Successfully", r.flashes[0])
        self.assertEqual(updated_user.role, 'admin')

        self.assertEqual(r.template, 'update.html')

    def test_namer(self):
        from ..forms.webforms import NamerForm

        response: TestResponse = self.w.get('/name')
        assert response.status_code == 200
        data = response.text
        assert isinstance(data, str)
        assert data.count("What's your name?") == 2
        for i in ['name', 'submit']:
            assert i in data

        self.assertEqual(response.template, 'name.html')
        self.assertIsInstance(response.context['form'], NamerForm)

        f: webtest.Form = response.forms['namer-form']

        token = f['csrf_token']
        f['name'] = 'simon'
        response = f.submit()
        response.maybe_follow()

        self.assertEqual(response.template, 'name.html')
        self.assertEqual(response.context['name'], 'simon')
        self.assertIn('Form submitted successfully', response.flashes[0])

    def test_test_pwd_empty_form(self):
        from ..models import Users
        from ..forms.webforms import PasswordForm

        r: TestResponse = self.w.get('/test_pwd')

        self.assertEqual(r.template, 'test_pwd.html')
        self.assertIsNone(r.context['email'])
        self.assertIsNone(r.context['password_hash'])
        self.assertIsNone(r.context['user_to_check'])
        self.assertIsNone(r.context['passed'])

        form = PasswordForm()

        f: webtest.Form = r.forms['pwd-form']

        # test empty form
        r = f.submit()

        self.assertIn('This field is required.',
                      r.context['form'].email.errors)
        self.assertIn('This field is required.',
                      r.context['form'].password_hash.errors)

    def test_test_pwd_wrong_email(self):
        from ..models import Users
        from ..forms.webforms import PasswordForm

        r: TestResponse = self.w.get('/test_pwd')

        self.assertEqual(r.template, 'test_pwd.html')
        self.assertIsNone(r.context['email'])
        self.assertIsNone(r.context['password_hash'])
        self.assertIsNone(r.context['user_to_check'])
        self.assertIsNone(r.context['passed'])

        form = PasswordForm()

        # invalid email
        r: TestResponse = self.w.get('/test_pwd')
        f: webtest.Form = r.forms['pwd-form']
        f['email'] = 'test@invalid.tld'
        f['password_hash'] = 'pwd'
        r = f.submit()

        self.assertFalse(r.context['passed'])
        self.assertNotIsInstance(r.context['user_to_check'], Users)

    def test_test_pwd_wrong_password(self):
        from ..models import Users
        from ..forms.webforms import PasswordForm

        # invalid email
        r: TestResponse = self.w.get('/test_pwd')
        f: webtest.Form = r.forms['pwd-form']
        f['email'] = 'test@invalid.tld'
        f['password_hash'] = 'pwd'
        r = f.submit()

        self.assertFalse(r.context['passed'])
        self.assertNotIsInstance(r.context['user_to_check'], Users)

        r: TestResponse = self.w.get('/test_pwd')
        # invalid password
        f: webtest.Form = r.forms['pwd-form']
        f['email'] = 'simon@test.com'
        f['password_hash'] = 'invalid_____password'
        r = f.submit()

        self.assertFalse(r.context['passed'])
        self.assertIsInstance(r.context['user_to_check'], Users)

        # all valid
        r: TestResponse = self.w.get('/test_pwd')
        f: webtest.Form = r.forms['pwd-form']
        f['email'] = 'simon@test.com'
        f['password_hash'] = 'p'
        r = f.submit()

        self.assertTrue(r.context['passed'])
        self.assertIsInstance(r.context['user_to_check'], Users)
        self.assertIn('Form submitted successfully', r.flashes[0])

    def test_test_pwd_all_valid(self):
        from ..models import Users

        # all valid
        r: TestResponse = self.w.get('/test_pwd')
        f: webtest.Form = r.forms['pwd-form']
        f['email'] = 'simon@test.com'
        f['password_hash'] = 'p'
        r = f.submit()

        self.assertTrue(r.context['passed'])
        self.assertIsInstance(r.context['user_to_check'], Users)
        self.assertIn('Form submitted successfully', r.flashes[0])

    def test_dashboard_update_no_upload(self):
        from ..models import Users
        r: TestResponse = self.login()

        user: Users = Users.query.get(r.session['_user_id'])

        self.assertEqual(r.template, 'dashboard.html')
        self.assertEqual(r.context['id'], user.id)
        self.assertIsInstance(r.context['name_to_update'], Users)
        self.app.config['UPLOAD_FOLDER'] = UPLOAD_PATH

        f: webtest.Form = r.forms['dashboard-form']
        f['name'] = 'simon_update'
        f['email'] = 'simon_update@invalid.tld'
        f['favorite_color'] = 'Fav color updated'
        f['username'] = 'username_updated'
        f['about_author'] = '<p>My bio updated</p>'
        r = f.submit()

        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertIsInstance(user_updated, Users)
        self.assertEqual(user.id, user_updated.id)
        self.assertIn('User Updated Successfully', r.flashes[0])

    def test_dashboard_update_upload(self):
        from ..models import Users
        r: TestResponse = self.login()

        user: Users = Users.query.get(r.session['_user_id'])
        logger.info(user.profile_picture)

        self.assertEqual(r.template, 'dashboard.html')
        self.assertEqual(r.context['id'], user.id)
        self.assertIsInstance(r.context['name_to_update'], Users)

        f: webtest.Form = r.forms['dashboard-form']

        f['profile_picture'] = webtest.Upload(
            os.path.join(FIXTURES_PATH, 'pizza-cat.jpg'))
        r = f.submit()

        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertIsInstance(user_updated, Users)
        self.assertEqual(user.id, user_updated.id)
        self.assertTrue(user.profile_picture.endswith('pizza-cat.jpg'))
        self.assertIn('User Updated Successfully', r.flashes[0])

        logger.info(user_updated.profile_picture)

        f: webtest.Form = r.forms['dashboard-form']

        f['profile_picture'] = webtest.Upload(
            os.path.join(FIXTURES_PATH, 'pizza-cat.jpg'))
        r = f.submit()

        user_updated: Users = Users.query.get(r.session['_user_id'])
        logger.info(user_updated.profile_picture)

        self.assertIsInstance(user_updated, Users)
        self.assertEqual(user.id, user_updated.id)
        self.assertTrue(user.profile_picture.endswith('pizza-cat.jpg'))

        self._clear_upload_test_folder()

    def test_dashboard__update_upload_file_not_allowed(self):
        from ..models import Users
        app.config['UPLOAD_FOLDER'] = os.path.join(
            SQL_DB_PATH_TEST, 'fixtures')

        r: TestResponse = self.login()

        user: Users = Users.query.get(r.session['_user_id'])

        self.assertEqual(r.template, 'dashboard.html')
        self.assertEqual(r.context['id'], user.id)
        self.assertIsInstance(r.context['name_to_update'], Users)

        f: webtest.Form = r.forms['dashboard-form']
        f['profile_picture'] = webtest.Upload(
            os.path.join(FIXTURES_PATH, 'posts.sql'))
        r = f.submit()

        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertEqual(user.id, user_updated.id)
        self.assertEqual(user.profile_picture, user_updated.profile_picture)
        self.assertFalse(user.profile_picture.endswith('posts.sql'))

        f: webtest.Form = r.forms['dashboard-form']
        f['profile_picture'] = webtest.Upload(
            os.path.join(FIXTURES_PATH, 'posts.json'))
        r = f.submit()
        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertEqual(user.id, user_updated.id)
        self.assertFalse(user.profile_picture.endswith('posts.json'))
        self.assertEqual(user.profile_picture, user_updated.profile_picture)

        f: webtest.Form = r.forms['dashboard-form']
        f['profile_picture'] = webtest.Upload(
            os.path.join(FIXTURES_PATH, 'testfile.txt'))
        r = f.submit()
        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertEqual(user.id, user_updated.id)
        self.assertEqual(user.profile_picture, user_updated.profile_picture)
        self.assertFalse(user.profile_picture.endswith('testfile.text'))

        f: webtest.Form = r.forms['dashboard-form']
        f['profile_picture'] = webtest.Upload(
            os.path.join(SQL_DB_PATH_TEST, 'config.py'))
        r = f.submit()
        user_updated: Users = Users.query.get(r.session['_user_id'])
        self.assertEqual(user.id, user_updated.id)
        self.assertEqual(user.profile_picture, user_updated.profile_picture)
        self.assertFalse(user.profile_picture.endswith('config.py'))

        self._clear_upload_test_folder()

    # @pytest.fixture(scope='class')
    # def make_post(self):
    #     """make_post Make a Post data

    #     :return: Post data
    #     :rtype: dict
    #     """
    #     return dict(

    #     )


########################## UTILITIES ####################

    def login(self, username=None, password=None):
        """login Login to website and get authenticated

        :return: A response
        :rtype: TestResponse
        """
        from ..forms.webforms import LoginForm

        if username is None:
            username = 'simon'
        if password is None:
            password = 'p'

        r = self.w.get('/login')
        f: webtest.Form = r.forms['login']
        csrf_token = f.fields['csrf_token']
        f['username'] = username
        f['password'] = password

        form = LoginForm()
        r: TestResponse = f.submit(form.submit_login.data)
        if r.status_code == 302:
            r = r.follow()
        return r

    def logout(self):
        self.w.post('/logout')

    def _get_posts(self):
        from ..models import Posts
        posts = Posts.query.all()

        for post in posts:
            logger.debug([post, post.id])

    def _get_ids(self, data):
        """"Get the ids of a Model"""
        return [i.id for i in data]

    def _clear_upload_test_folder(self):
        dirs = os.listdir(UPLOAD_PATH)
        print(dirs)
        for file in dirs:
            print(file)
            os.remove(os.path.join(UPLOAD_PATH, file))

    def _populate_records(self, number_of_records_to_populate: int = 500):
        import datetime
        import random
        from ..models import Posts, Users

        # with open(SQL_PATH, encoding='utf8') as f:
        # db = sqlite3.connect(
        # SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))

        faker = Faker(locale='fr')
        # lorem.Provider.words(nb=10)
        query = ''
        faker.random.seed()
        fake_data = defaultdict(list)
        for i in range(0, number_of_records_to_populate):
            query_u = """INSERT INTO posts (
                    title,
                    content,
                    date_posted,
                    slug,
                    poster_id
                )
                VALUES ("""
            content = ''
            i = 0
            has_words = random.randint(0, 100)
            if has_words > 60:
                words = ['post', 'posts', 'new post',
                         'dolor consectetur elite']
                where = int(has_words/10)
            else:
                words = []
                where = -1
            cnt = random.randint(1, 5)
            # print('cnt', cnt, where)
            done = False
            while i <= cnt:
                content += "<p>"
                if not done and where > 7 and i != 0 and i != cnt-1:
                    content += words[random.randint(0, len(words)-1)]
                    content += faker.paragraph(nb_sentences=1,
                                               variable_nb_sentences=True)
                    content += words[random.randint(0, len(words)-1)]
                    content += faker.paragraph(nb_sentences=1,
                                               variable_nb_sentences=True)
                    done = True
                else:
                    content += faker.paragraph(nb_sentences=5,
                                               variable_nb_sentences=True)
                content += "</p>"

                # print(content)

                i += 1

            content = content.replace("'", "''")
            # content = f'<p>{content}</p>'
            title = faker.sentence(nb_words=10, variable_nb_words=True)
            poster_id = random.randint(1, 2)
            slug = sluglify_title(title)
            f = faker.date_time()
            date_posted = f + \
                datetime.timedelta(milliseconds=random.randint(0, 999))

            post = Posts(title=title,
                         content=content,
                         poster_id=poster_id,
                         )
            self.db.session.add(post)
            self.db.session.commit()

            # if post.slug:
            #     pass
            # else:
            # tzinfo=datetime.timezone.utc)
            # .strftime(
            # '%Y-%m-%d %H:%M:%S.%f')[:-3].replace('000',  str(random.randint(0, 999)))
            """
                title,
                    content,
                    date_posted,
                    slug,
                    poster_id
                    """

            # query_u += f"'{title}',"
            # query_u += f"'{content}',"
            # query_u += f"'{date_posted}',"
            # query_u += f"'{slug}',"
            # query_u += f"'{poster_id}'"
            # query_u += f');'
            # # print(query_u)
            # db.execute(query_u)

            print(i)
            # query += query_u + ',')

    def _get_skipped_page(self, page_id: int) -> datetime:
        from ..models import Posts
        skipped_paged_query = Posts.query.with_entities(Posts.date_posted, Posts.id).order_by(
            Posts.date_posted, Posts.id).limit(1).offset(page_id)
        logger.info(str(skipped_paged_query))
        return skipped_paged_query.all()[0][0]


def sluglify_title(title: str) -> str:
    import datetime
    my_date = datetime.datetime.now()
    # print('slugified slug',
    #   f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}")
    return f"{slugify(title)}-{my_date.year}-{my_date.month:02d}-{my_date.day:02d}"
