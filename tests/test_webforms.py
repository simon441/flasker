# from __future__ import with_statement

# import unittest

# from flask import Flask, render_template, jsonify, Request, request
# from werkzeug.test import EnvironBuilder
# from wtforms import StringField, HiddenField, SubmitField, FieldList
# from wtforms.validators import Required

# from flask_wtf import Form, html5, files

# from ..app import app as a

# from werkzeug.test import Client

# # @pytest.fixture
# import os
# import tempfile
# import flask
# from flask_sqlalchemy import SQLAlchemy
# import pytest

# from tests.config import ROOT_DIR_TEST


# def get_sql() -> str:
#     # read in SQL for populating test data
#     with open(os.path.join(ROOT_DIR_TEST, 'db',  "sqlschema_sqlite.sql"), "rb") as f:
#         _data_sql = f.read().decode("utf8")
#         return _data_sql


# def app():
#     """Create and configure a new app instance for each test."""
#     # create a temporary file to isolate the database for each test
#     db_fd, db_path = tempfile.mkstemp()
#     # create the app with common test config
#     app = flask.Flask(__name__)
#     app.config.update({"TESTING": True})
#     db = SQLAlchemy(app)
#     # create the database and load test data
#     with app.app_context():
#         db.engine.execute(get_sql())

#     yield app

#     # close and remove the temporary database
#     os.close(db_fd)
#     os.unlink(db_path)


# @pytest.fixture
# def client(app: flask.Flask):
#     """A test client for the app."""
#     app.testing = True
#     client = app.test_client()
#     return client


# def test_login_form(client: Client):
#     from ..webforms import LoginForm
#     form = LoginForm()

from unittest import TestCase

from ..app import app
from ..forms.webforms import LoginForm


# class TestWebforms(TestCase):
#     def setUp(self) -> None:
#         app.testing = True
#         app.config['SECRET_KEY']="secret key"

#         return super().setUp()

#     def tearDown(self) -> None:
#         return super().tearDown()

#     def test_login_form(self):
#         with app.test_client() as client:
#             with app.app_context():
#                 form = LoginForm()
#                 form.username = 'me'
#                 form.password = 'pass'
#                 form.validate()
