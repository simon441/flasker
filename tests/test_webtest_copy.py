from .config import ROOT_DIR_TEST, SQL_DB_PATH_TEST
import json
import os
import sqlite3
import sys
from unittest import TestCase
from urllib.parse import urlencode
import flask_login
import pytest
from werkzeug.urls import url_encode
from flask_webtest import TestApp, TestRequest, TestResponse
import webtest
import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import flask
pytestmark = pytest.mark.skip("all tests still WIP")

# from flask_fixtures import FixturesMixin, load_fixtures, load_fixtures_from_file

FIXTURES_PATH = os.path.join(SQL_DB_PATH_TEST, 'fixtures')
SQL_PATH = os.path.join(ROOT_DIR_TEST, 'db',  "sqlschema_sqlite.sql")

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger()

path = SQL_DB_PATH_TEST
# SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(FIXTURES_PATH, "test.db")
# "sqlite:///" + os.path.join(path, 'fixtures', "db_test.sqlite3")
SQLALCHEMY_DATABASE_URI_ = f"sqlite:///{SQL_DB_PATH_TEST}/db_test_2.sqlite3"

SQL_DB_PATH = SQL_DB_PATH_TEST

logger.debug(dict(SQL_DB_PATH=SQL_DB_PATH))


def remove_db_files():
    pass
# for i in os.listdir(SQL_DB_PATH_TEST):
#     if i.endswith('_test.sqlite3'):
#         os.remove(os.path.join(SQL_DB_PATH_TEST, i))


if True:

    # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    #     os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))

    os.environ.setdefault('test', 'True')
    os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI_)
    SQL_DATABASE_ALCHEMY = SQLALCHEMY_DATABASE_URI_

    from ..app import app, gettext

    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI_
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['FIXTURES_DIRS'] = FIXTURES_PATH
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['DEBUG_TB_ENABLED'] = False

    from ..app import db


@pytest.fixture(autouse=True)
def set_up():
    app.testing = True
    # remove_db_files()
    # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    #     os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
    with open(SQL_PATH, encoding='utf8') as f:
        db_u = sqlite3.connect(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
        db_u.executescript(f.read())
        db_u.commit()
        db_u.close()
    # create_db(app)
    w = TestApp(app)
    app_context = app.app_context()
    app_context.push()
    yield w
    db.session.remove()
    # db.drop_all()
    db.engine.dispose()

    # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    remove_db_files()


class ExampleTest(TestCase):
    TESTING = True

    # Specify the fixtures file(s) you want to load.
    # Change the list below to ['authors.yaml'] if you created your fixtures
    # file using YAML instead of JSON.
    fixtures = ['users.json', 'posts.json']

    def create_app(self):
        app = Flask(__name__)
       # app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
        app.config['FIXTURES_DIRS'] = FIXTURES_PATH
        return app

    # def create_db(self, app: Flask):
    #     self.db = SQLAlchemy(app)

    #     self.db.create_all()

    #     u = True
    #     if u:
    #         # SQLALCHEMY_DATABASE_URI = self.SQLALCHEMY_DATABASE_URI
    #         # os.environ.setdefault('SQL_DATABASE_ALCHEMY', SQLALCHEMY_DATABASE_URI)
    #         # from ..app import db
    #         from ..models import Posts, Users
    #         # self.db = db
    #     return self.db

    def populate_db(self):
        """Populate the Database"""
        # load_fixtures(self.db, os.path.join(FIXTURES_PATH, 'users.json'))
        # load_fixtures(slf.db, os.path.join(FIXTURES_PATH, 'posts.json'))
        # with open(os.path.join(FIXTURES_PATH, 'users.sql'), encoding='utf8') as f:
        #     self.db.engine.execute(f.read())
        # logger.debug(db.engine.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall())
        # db.engine.execute('DELETE FROM users;')
        # db.engine.execute('DELETE FROM posts;')

        # with open(os.path.join(FIXTURES_PATH, 'users.sql'), encoding='utf8') as f:
        #     db.engine.execute(f.read())
        # with open(os.path.join(FIXTURES_PATH, 'posts.sql'), encoding='utf8') as f:
        #     db.engine.execute(f.read())
        with open(SQL_PATH, encoding='utf8') as f:
            db_u = sqlite3.connect(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
            db_u.executescript(f.read())
            db_u.commit()
            db_u.close()

    # def setUp(self):
    #     # self.app = self.create_app()
    #     self.app = app
    #     # remove_db_files()
    #     # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    #     #     os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))
    #     self.populate_db()
    #     # self.create_db(self.app)
    #     self.w = TestApp(self.app)
    #     self.app_context = app.app_context()
    #     self.app_context.push()

    #     # db.create_all()

    # def tearDown(self):
    #     db.session.remove()
    #     # db.drop_all()
    #     db.engine.dispose()

    #     # if os.path.exists(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')):
    #     remove_db_files()
    #     # os.remove(SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))

    def test_index(self, w):
        r = w.get('/')
        # Assert there was no messages flushed:
        self.assertFalse(r.flashes)
        print(r)
        # Access and check any variable from template context...
        self.assertEqual(r.context['first_name'], 'Simon')
        self.assertEqual(r.context['favorite_pizza'], ['Pepperoni', gettext('Cheese'), gettext('Mushrooms'), 42])

        self.assertEqual(r.template, 'index.html')
        # ...and from session
        self.assertNotIn('user_id', r.session)

    def test_login_passed_with_good_credentials(self):
        logger.info(db.engine)
        logger.info(SQLALCHEMY_DATABASE_URI_)
        logger.info(path)
        logger.info(SQL_DB_PATH)
        # self.assertFalse(True)
        r = self.w.post('/login', dict(username='simon', password='p', submit_login="Log in"))
        # Assert there was no messages flushed:
        self.assertEqual([('success', 'Login successful!')], r.flashes)
        # Access and check any variable from template context...
        res = r.follow()
        self.assertIn('Dashboard', str(res))
        # ...and from session
        self.assertIn('_user_id', r.session)

    def test_login_fail_with_wrong_password(self):
        r = self.w.post('/login', dict(username='simon', password='defrfr', submit_login="Log in"))
        # Assert there was no messages flushed:
        self.assertEqual([('danger', ('Invalid credentials. Try Again'))], r.flashes)
        self.assertEqual(r.template, 'login.html')

    def test_login_fail_with_user_not_exists(self):
        r = self.w.post('/login', dict(username='idonotexist', password='defrfr', submit_login="Log in"))
        # Assert there was no messages flushed:
        self.assertEqual([('danger', ('Invalid credentials. Try Again'))], r.flashes)
        self.assertEqual(r.template, 'login.html')

    def test_logout(self):
        r = self.w.post('/login', dict(username='simon', password='p', submit_login="Log in"))
        r.follow()
        r = self.w.post('/logout')
        # Assert there was no messages flushed:
        self.assertEqual([('success', ('You have been logged out!\nThanks for stopping by...'))], r.flashes)
        res = r.follow()
        self.assertEqual(res.template, 'login.html')
        self.assertIn('Login', str(res))

    def test_posts(self):
        from ..models import Posts
        r = self.w.get('/posts')
        posts = Posts.query.order_by(Posts.date_posted)
        for post in posts:
            logger.info(post)
        for post in r.context['posts']:
            logger.info(post)
            # Same query
        self.assertEqual(str(r.context['posts']), str(posts))
        self.assertEqual(r.template, 'posts.html')

    def test_post_does_not_exists_404(self):
        r = self.w.get('/posts/0', expect_errors=True)
        self.assertEqual(404, r.status_code)

    def test_post_exists(self):
        from ..models import Posts
        r = self.w.get('/posts/1')
        post = Posts.query.get(1)
        # Same query
        self.assertEqual(str(r.context['post']), str(post))
        self.assertEqual(r.template, 'post.html')

    def test_json(self):
        r = self.w.get('/json')
        self.assertEqual(200, r.status_code)
        favorite_pizza = {
            "Jane": "Pepperonni",
            "Mary": "Cheese",
            "Stuart": "Veggie",
            "Tim": "Mushroom"
        }
        logger.info(str(r))
        logger.info(r.body.decode('UTF-8'))
        self.assertEqual(favorite_pizza, json.loads(r.body.decode('UTF-8')))
        print("SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///')",
              SQLALCHEMY_DATABASE_URI_.removeprefix('sqlite:///'))

    def test_add_post_get_failed_without_login(self):
        from ..forms.webforms import PostForm
        r: TestResponse = self.w.get('/add-post', expect_errors=True)
        logger.info(r.location)
        self.assertEqual(f'http://localhost/login?{urlencode({"next": "/add-post"})}', r.location)

        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)
        # self.assertEqual(r.template, 'add_login.html')
        # form = PostForm()
        # self.assertEqual(r.context['form'], form)

    def test_add_post_get_success_with_correct_login(self):
        from ..forms.webforms import PostForm
        r = self.login()
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        form = PostForm()
        self.assertEqual(repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_add_post_get_and_post(self):
        from ..forms.webforms import PostForm
        from ..models import Posts

        # Get all Posts
        posts: Posts = Posts.query.all()

        # The post form
        form = PostForm()

        # Login
        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        # Form content
        form_content = dict(
            title='Form title test',
            # slug='form-test-one',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        f: webtest.Form = r.forms['add-post']
        for i in f.fields:
            print(i)
        f['title'] = form_content['title']
        # f['slug'] = form_content['slug']
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        # Get the last inserted post
        post: Posts = Posts.query.filter_by(title=form_content['title']).first()

        # Check flash messages
        logger.info(f'location type: {r.location}')
        self.assertTrue(r.flashes)
        # There is a redirect to the @post page
        self.assertEqual(r.status_code, 302)

        # Follow redirect
        r = r.follow()
        # Post Page
        self.assertEqual(r.status_code, 200)
        self.assertFalse(r.flashes)
        self.assertIn('Blog Post Submitted Successfully!', r.text)

        # Get all new Posts
        new_posts = Posts.query.all()

        # The is one more new post
        self.assertEqual(len(posts)+1, len(new_posts))

        self.assertIn(form_content['title'], r.text)

    def test_add_post_no_all_fields_ok(self):
        """Try to post a form with missing content returns
        an error message
         """
        from ..forms.webforms import PostForm

        #  Post Form
        form = PostForm()

        # Login
        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get('/add-post', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        # Form content
        form_content = dict(
            title='Form title test',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        logger.info(r.text)

        # Missing content
        f: webtest.Form = r.forms['add-post']
        f['title'] = form_content['title']
        # f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        self.assertEqual(r.context['form'].content.data, '')
        self.assertEqual(r.context['form'].title.data, form_content['title'])

        self.assertIn('There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check the title is back in the form
        self.assertIn(form_content['title'], r.text)

        # Missing title
        f: webtest.Form = r.forms['add-post']
        f['slug'] = ''
        f['title'] = ''
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'add_post.html')

        self.assertEqual(r.context['form'].content.data, form_content['content'])
        self.assertEqual(r.context['form'].title.data, '')
        self.assertGreater(len(r.context['form'].title.errors), 0)

        self.assertIn('There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check error field on title
        # self.assertIn('"validationTitleFeedback" class=" invalid-feedback"', r.text)

    def test_edit_post_get_failed_without_login(self):
        from ..forms.webforms import PostForm
        r: TestResponse = self.w.get('/posts/edit/1', expect_errors=True)
        logger.info(r.location)
        logger.info(r)
        self.assertEqual(f'http://localhost/login?{urlencode({"next": "/posts/edit/1"})}', r.location)

        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)
        # self.assertEqual(r.template, 'edit_login.html')
        # form = PostForm()
        # self.assertEqual(r.context['form'], form)

    def test_edit_post_get_success_with_correct_login_and_correct_user(self):
        from ..models import Posts
        from ..forms.webforms import PostForm
        logger.debug('test_edit_post_get_success_with_correct_login_and_correct_user')
        self._get_posts()
        r = self.login(username='tim', password='password')
        r: TestResponse = self.w.get('/posts/edit/2', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        form = PostForm()
        post: Posts = Posts.query.get(2)
        form.slug.data = post.slug
        form.title.data = post.title
        form.content.data = post.content
        # print(app.login_manager)
        # logger.info(flask.session)
        # assert app.login_manager.current_user.id == post.poster_id
        self.assertEqual(repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_edit_post_get_success_with_correct_login_user_id_admin(self):
        from ..models import Posts
        from ..forms.webforms import PostForm
        r = self.login()
        r: TestResponse = self.w.get('/posts/edit/4', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        form = PostForm()
        post: Posts = Posts.query.get(4)
        form.slug.data = post.slug
        form.title.data = post.title
        form.content.data = post.content
        self.assertEqual(repr(r.context['form'].content.data), repr(form.content.data))
        self.assertEqual(repr(r.context['form'].slug.data), repr(form.slug.data))
        self.assertEqual(repr(r.context['form'].title.data), repr(form.title.data))
        self.assertEqual(repr(r.context['form'].submit.data), repr(form.submit.data))

    def test_edit_post_does_not_exists_404(self):
        from ..models import Posts
        from ..forms.webforms import PostForm
        r = self.login()
        r: TestResponse = self.w.get('/posts/edit/0', status=404)
        self.assertEqual(404, r.status_code)

    def test_edit_post_get_and_post(self):
        from ..forms.webforms import PostForm
        from ..models import Posts

        # Get all Posts
        post: Posts = Posts.query.get(2)

        # The post form
        form = PostForm()

        # Login
        r = self.login(username='tim', password='password')

        # Add a post (GET)
        r: TestResponse = self.w.get('/posts/edit/2', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        # Form content
        form_content_replace = dict(
            id=post.id,
            title='Form title test',
            slug='form-test-one',
            content="""Test post <p>Content</p>""",
            submit=form.submit.data,
        )

        # Get the post form on the page
        f: webtest.Form = r.forms['edit-post']
        for i in f.fields:
            print(i)
        f['title'] = form_content_replace['title']
        f['slug'] = form_content_replace['slug']
        f['content'] = form_content_replace['content']

        r: TestResponse = f.submit(form.submit.name)

        # Get the last inserted post
        # post: Posts = Posts.query.filter_by(title=form_content['title']).first()

        # Check flash messages
        logger.info(f'location type: {r.location}')
        self.assertTrue(r.flashes)
        # There is a redirect to the @post page
        self.assertEqual(r.status_code, 302)

        # Follow redirect
        r = r.follow()
        # Post Page
        self.assertEqual(r.status_code, 200)
        self.assertFalse(r.flashes)
        self.assertIn('Post Has Been Updated!', r.text)

        # Get all new Posts
        new_post: Posts = Posts.query.get(2)

        # The is one more new post
        self.assertEqual(form_content_replace['content'], new_post.content)
        self.assertEqual(form_content_replace['slug'], new_post.slug)
        self.assertEqual(form_content_replace['title'], new_post.title)
        # self.assertEqual(post.date_posted, new_post.date_posted)
        self.assertEqual(post.id, new_post.id)

        self.assertIn(form_content_replace['title'], r.text)
        self.assertIn(form_content_replace['content'], r.text)
        # self.assertIn(form_content_replace['slug'], r.text)
        self.assertIn(f"/posts/edit/{form_content_replace['id']}", r.text)

    def test_edit_post_no_all_fields_ok(self):
        """Try to post a form with missing content returns
        an error message
         """
        from ..forms.webforms import PostForm
        from ..models import Posts

        # Get all Posts
        post: Posts = Posts.query.get(1)

        #  Post Form
        form = PostForm()

        # Login
        r = self.login()

        # Add a post (GET)
        r: TestResponse = self.w.get('/posts/edit/1', status=200)
        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        # Form content
        form_content = dict(
            title='Form title test',
            content="""Test post <p>Content</p>""",
            slug="my-slug-test",
            submit=form.submit.data,
        )

        # Get the post form on the page
        logger.info(r.text)

        # Missing content
        f: webtest.Form = r.forms['edit-post']
        f['title'] = form_content['title']
        f['slug'] = form_content['slug']
        f['content'] = ''

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        self.assertEqual(r.context['form'].content.data, '')
        self.assertEqual(r.context['form'].title.data, form_content['title'])
        self.assertEqual(r.context['form'].slug.data, form_content['slug'])

        self.assertIn('There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check the title is back in the form
        self.assertIn(form_content['title'], r.text)

        # Missing title
        f: webtest.Form = r.forms['edit-post']
        f['slug'] = 'test-meemmdrggd'
        f['title'] = ''
        f['content'] = form_content['content']

        r: TestResponse = f.submit(form.submit.name)

        self.assertEqual(200, r.status_code)
        self.assertEqual(r.template, 'edit_post.html')

        self.assertEqual(r.context['form'].content.data, form_content['content'])
        self.assertEqual(r.context['form'].title.data, '')
        self.assertGreater(len(r.context['form'].title.errors), 0)

        self.assertIn('There are errors in the form. Please correct them before submitting again.', r.text)

        # Check flash messages
        self.assertTrue(r.flashes)
        # Check error field on title
        # self.assertIn('"validationTitleFeedback" class=" invalid-feedback"', r.text)

    def test_delete_post_get_failed_without_login(self):
        r: TestResponse = self.w.post('/posts/delete/1', expect_errors=True)
        logger.info(r.location)
        logger.info(r)
        self.assertEqual(f'http://localhost/login?{urlencode({"next": "/posts/delete/1"})}', r.location)

        # self.assertIn([('danger', flask_login.LOGIN_MESSAGE)], r.flashes)
        self.assertEqual(302, r.status_code)
        r = r.follow()
        self.assertEqual(r.template, 'login.html')
        self.assertIn(flask_login.LOGIN_MESSAGE, r.text)

    def test_delete_post_get_success_with_correct_login_and_correct_user(self):
        from ..models import Posts
        r = self.login(username='tim', password='password')
        posts = Posts.query.all()

        for post in posts:
            logger.debug([post, post.id])

        post: Posts = Posts.query.get(2)
        r: TestResponse = self.w.post('/posts/delete/2', status=302)

        self.assertEqual(302, r.status_code)
        self.assertEqual('"/posts"', r.text)
        self.assertIn('Blog Post Has Been Deleted!', r.flashes)
        posts = Posts.query.all()

        for post in posts:
            logger.debug([post, post.id])

        db.session.add(post)
        db.session.commit()
        self.populate_db()

    # def test_delete_post_get_success_with_correct_login_user_id_admin(self):
    #     from ..models import Posts
    #     from ..webforms import PostForm
    #     r = self.login()
    #     r: TestResponse = self.w.post('/posts/delete/2', status=200)
    #     self.assertEqual(200, r.status_code)
    #     self.assertIn('Blog Post Has Been Deleted!', r.flashes)

    def test_delete_post_does_not_exists_404(self):
        from ..models import Posts
        from ..forms.webforms import PostForm
        r = self.login()
        # r:TestResponse = self.w.get('/post/1')

        r: TestResponse = self.w.post('/posts/delete/0', status=404)
        self.assertEqual(404, r.status_code)

    # def test_edit_post_get_is_404(self):
    #     r = self.login()
    #     r: TestResponse = self.w.get('/posts/edit/1')

    # def test_delete_post_get_is_404(self):
    #     r = self.login()
    #     r: TestResponse = self.w.get('/posts/delete/1')

    # @pytest.fixture(scope='class')
    # def make_post(self):
    #     """make_post Make a Post data

    #     :return: Post data
    #     :rtype: dict
    #     """
    #     return dict(

    #     )

    def login(self, username=None, password=None):
        """login Login to website and get authenticated

        :return: A response
        :rtype: TestResponse
        """
        if username is None:
            username = 'simon'
        if password is None:
            password = 'p'
        r: TestResponse = self.w.post('/login', dict(username=username, password=password, submit_login="Log in"))
        if r.status_code == 302:
            r = r.follow()
        return r

    def logout(self):
        self.w.post('/logout')

    def _get_posts(self):
        from ..models import Posts
        posts = Posts.query.all()

        for post in posts:
            logger.debug([post, post.id])
