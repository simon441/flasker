import os
import pathlib

ROOT_DIR_TEST = pathlib.Path().resolve()
SQL_DB_PATH_TEST = os.path.join(ROOT_DIR_TEST, 'tests')
print('tests dirs', SQL_DB_PATH_TEST, ROOT_DIR_TEST)

# Clear all sql files in test directory
files = os.listdir(SQL_DB_PATH_TEST)
for file in files:
    if not file.startswith('flasker') and (file.endswith('.db') or file.endswith('.sqlite3')):
        os.remove(os.path.join(SQL_DB_PATH_TEST, file))
        print(f'Deleted file {os.path.join(SQL_DB_PATH_TEST, file)}')
