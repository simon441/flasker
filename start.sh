#!/bin/bash

if [[ $1 != "" ]]; then
    export FLASK_ENV=development
else
    export FLASK_ENV=production
fi
# echo ${FLASK_ENV}
export FLASK_APP=app.py
flask run