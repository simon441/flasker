import sys
from click import Abort, echo
import click
from flask import current_app
from flask.cli import AppGroup, with_appcontext


@click.command('create-database')
@with_appcontext
def create_database(name):
    # try:
    value = click.prompt('Database name', 'flask_users', confirmation_prompt=True, show_default=True)
    echo(value)
    # except Abort:
    #     sys.exit(1)


@click.command('init env')
@with_appcontext
def init_env():
    # try:
    value = click.prompt('Database name', 'flask_users', confirmation_prompt=True, show_default=True)
    echo(value)
    # except Abort:
    #     sys.exit(1)


# current_app.cli.add_command(create_database)
