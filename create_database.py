import datetime
import os
import sys
from copy import copy
from os import listdir
from pathlib import Path

import pyodbc
import sqlalchemy as sa
import sqlalchemy_utils
from sqlalchemy.engine.url import make_url
from sqlalchemy_utils.functions import quote
from sqlalchemy_utils.functions.database import database_exists

from env import Configuration

# PyODBC uses internal pooling by default,
# which means connections will be longer lived than they are
# within SQLAlchemy itself.
# As SQLAlchemy has its own pooling behavior,
# it is often preferable to disable this behavior.
# This behavior can only be disabled globally at the PyODBC module
# level, before any connections are made
# @see https://docs.sqlalchemy.org/en/13/dialects/mssql.html#module-sqlalchemy.dialects.mssql.pyodbc
if Configuration.DB_URI.startswith('msssql'):
    pyodbc.pooling = False

print(Configuration.DB_URI, type(Configuration.DB_URI))
# connection = pyodbc.connect(
#     "DRIVER={ODBC Driver 17 for SQL Server};SERVER=(localdb)\MSSQLLocalDB;UID=dbo"
# )


def yes_no(answer: str) -> bool:
    """Test yes/no and return True or False depending on answer

    Args:
        answer (str): question to the user

    Returns:
        bool: Yes=True, No=False
    """
    yes = set(['yes', 'y', 'ye', ''])
    no = set(['no', 'n'])

    while True:
        choice = input(answer).lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'yes' or 'no'\n")


def _set_url_database(url: sa.engine.url.URL, database):
    """Set the database of an engine URL.

    :param url: A SQLAlchemy engine URL.
    :param database: New database to set.

    """
    if hasattr(sa.engine, 'URL'):
        if database:
            ret = sa.engine.URL.create(
                drivername=url.drivername,
                username=url.username,
                password=url.password,
                host=url.host,
                port=url.port,
                database=database,
                query=url.query
            )
        else:
            ret = sa.engine.URL.create(
                drivername=url.drivername,
                username=url.username,
                password=url.password,
                host=url.host,
                port=url.port,
                query=url.query
            )
    else:  # SQLAlchemy <1.4
        url.database = database
        ret = url
    assert ret.database == database, ret
    return ret


def create_database(url, encoding='utf8', template=None, create_db_extra_text: str = None):
    """Issue the appropriate CREATE DATABASE statement.

    :param url: A SQLAlchemy engine URL.
    :param encoding: The encoding to create the database as.
    :param template:
        The name of the template from which to create the new database. At the
        moment only supported by PostgreSQL driver.

    To create a database, you can pass a simple URL that would have
    been passed to ``create_engine``. ::

        create_database('postgresql://postgres@localhost/name')

    You may also pass the url from an existing engine. ::

        create_database(engine.url)

    Has full support for mysql, postgres, and sqlite. In theory,
    other database engines should be supported.
    """

    url = copy(make_url(url))
    database = url.database
    dialect_name = url.get_dialect().name
    dialect_driver = url.get_dialect().driver

    if dialect_name == 'postgresql':
        url = _set_url_database(url, database="postgres")
    elif dialect_name == 'mssql':
        url = _set_url_database(url, database="master")
    elif not dialect_name == 'sqlite':
        url = _set_url_database(url, database=None)
    if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
            or (dialect_name == 'postgresql' and dialect_driver in {
            'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
        engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
    else:
        engine = sa.create_engine(url)

    if dialect_name == 'postgresql':
        if not template:
            template = 'template1'

        text = "CREATE DATABASE {0} WITH ENCODING '{1}' TEMPLATE {2}".format(
            quote(engine, database),
            encoding,
            quote(engine, template)
        )

        with engine.connect() as connection:
            connection.execute(text)

    elif dialect_name == 'mysql':
        if create_db_extra_text:
            create_db_extra_text = "COLLATE '{0}'".format(create_db_extra_text)
        text = "CREATE DATABASE {0} CHARACTER SET '{1}' {2}".format(
            quote(engine, database),
            encoding,
            create_db_extra_text,
        )

        with engine.connect() as connection:
            connection.execute(text)

    elif dialect_name == 'sqlite' and database != ':memory:':
        if database:
            with engine.connect() as connection:
                connection.execute("CREATE TABLE DB(id int);")
                connection.execute("DROP TABLE DB;")

    elif dialect_name == 'mssql':
        text = 'CREATE DATABASE {0}'.format(quote(engine, database))
        with engine.connect() as connection:
            connection.execute('USE master;')
            connection.execute(text)

    else:
        text = 'CREATE DATABASE {0}'.format(quote(engine, database))
        with engine.connect() as connection:
            connection.execute(text)

    engine.dispose()


def connect_to_database(url) -> sa.engine.Engine:

    url = copy(make_url(url))
    database = url.database
    dialect_name = url.get_dialect().name
    dialect_driver = url.get_dialect().driver

    if dialect_name == 'postgresql':
        url = _set_url_database(url, database="postgres")
    elif dialect_name == 'mssql':
        url = _set_url_database(url, database="master")
        url = str(url).split("DATABASE%3D")[0]
    elif not dialect_name == 'sqlite':
        url = _set_url_database(url, database=None)

    if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
            or (dialect_name == 'postgresql' and dialect_driver in {
            'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
        engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
    else:
        engine = sa.create_engine(url)
    return engine


def disconnect_from_database(engine: sa.engine.Engine):
    """Remove database connection

    Args:
        engine (sa.engine.Engine): Database connection
    """
    engine.dispose()


def get_driver_specs(url: str) -> list[str]:
    """Returns the Driver specifications

    Args:
        url (str): Connection String

    Returns:
        list[str]: Driver Description
    """
    def get_charset(charsets: list, charset: str) -> set[str]:
        """Get the MySql/MariaDB available character set if found

        Args:
            charsets (list): Character sets available
            charset (str): Character set to find

        Returns:
            set[str]: The character set data if found else an empty dictionary
        """
        for c in charsets:
            # print(c, charset, charset == c[0])

            if charset == c[0]:
                # print('ok')
                # print(c)
                return c
            else:
                continue
                # print('fail')
        return {}

    charset_found = []

    url = copy(make_url(url))
    print(url)
    database = url.database
    dialect_name = url.get_dialect().name
    dialect_driver = url.get_dialect().driver

    print(url.get_dialect(), url)
    print(dir(url.get_dialect()))
    if dialect_name == 'postgresql':
        url = _set_url_database(url, database="postgres")
    elif dialect_name == 'mssql':
        if database is None:
            database = "master"
        url = _set_url_database(url, database="master")
        # Remove the DATABASE name from the URI
        url = str(url).split("DATABASE%3D")[0]  # + 'DATABASE%3D"master"%3D'

    elif not dialect_name == 'sqlite':
        url = _set_url_database(url, database=None)
    if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
            or (dialect_name == 'postgresql' and dialect_driver in {
            'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
        engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
    else:
        engine = sa.create_engine(url)
    conn = engine.connect()
    # print(conn.connection)
    # print(conn.connection.driver_connection)
    # print(dir(conn.connection))
    # print(dir(conn.connection.driver_connection))

    if dialect_driver == 'mysql':
        # print(url.get_dialect().is_mariadb, url.get_dialect().description_encoding)

        # print(conn.connection.driver_connection.server_charset)
        # print(conn.connection.driver_connection.get_host_info())
        # print(conn.connection.driver_connection.encoding)
        # print(conn.connection.driver_connection.charset)
        # print(conn.execute("SHOW VARIABLES").fetchall())
        charsets = conn.execute("SHOW CHARACTER SET;").fetchall()
        try:
            # Get the charset passed in the Connection String
            charset = db_driver.split('charset=')[-1]
        except IndexError:
            charset = 'utf8'
        charset_found = get_charset(charsets, charset)
        # found a charset
        if len(charset_found) != 4:
            charset_found = ['utf8', 'utf8']
    else:
        pass
    conn.close()
    return charset_found


db_driver = Configuration.DB_URI
# print(db_driver)

print('connected to "{0}"'.format(db_driver))

encoding = 'utf8'
extra_text = ''
print(type(db_driver), db_driver, repr(db_driver))
# Mysql driver type
if db_driver.startswith('mysql'):
    specs = get_driver_specs(db_driver)
    encoding = 'utf8mb4' if len(specs) < 1 else specs[0]
    extra_text = 'utf8mb4_general_ci' if len(specs) < 2 else specs[1]
    print(encoding, extra_text)
else:
    # Other drivers
    specs = get_driver_specs(db_driver)
    print(specs)

# Database already exists?
try:
    db_exists = sqlalchemy_utils.database_exists(db_driver)
except sa.exc.InterfaceError:
    db_exists = False

# Create the database?
create_db = True
# print('dbexists', db_exists)
# Override, DROP and CREATE Database
if db_exists:
    create_db = yes_no('Drop database and create it again? ')
    if create_db:
        # MS SQl Server
        if db_driver.startswith('mssql'):
            engine = connect_to_database(db_driver)
            database = db_driver.split('DATABASE=')[1]
            engine.connect()
            with engine.begin() as conn:
                conn.execute('DROP DATABASE {0}'.format(database))
        else:
            sqlalchemy_utils.drop_database(db_driver)
        print('Database {0} DROP successfull'.format(db_driver))

# User wants to CREATE database or no Database found
if create_db:
    # MS Sql Server
    if db_driver.startswith('mssql'):
        engine = connect_to_database(db_driver)
        database = db_driver.split('DATABASE=')[1]
        engine.connect()
        with engine.begin() as conn:
            conn.execute('CREATE DATABASE {0}'.format(database))
    else:
        engine = create_database(db_driver, encoding=encoding, create_db_extra_text=extra_text)

    # Could not create database
    if not database_exists(db_driver):
        print('Could not create database')
        sys.exit(-1)

print('flask_users Database ' + ('created' if db_exists else 'already exists'))
# Does the user want to create the Database Tables
create_tables = True

# db = engine.connect()

# Get a list of the available database tables
t = sa.MetaData(db_driver)
# print(t)
# print("----------------------------------")
# for i in t.sorted_tables:
#     print(i)
# print('########')

# Is there a table?


def is_table(t):
    # List tables
    for table in t:
        print(table, type(table))
        if type(table) == sa.schema.Table:
            return (True, table)
    return (False, [])


print(t.tables.items(), t)
# Creating tables
if db_exists or len(t.sorted_tables) > 0:  # is_table(t.tables):
    create_tables = yes_no('Create all (yes/no)? ')
    # Want to create tables then remove then"
    if create_tables:
        t.drop_all()
        print('DROP all Tables')

# User want to CREATE tables or no tables found
if create_tables:
    # Get the last migration from the migrations directory
    from sqlalchemy.orm import declarative_base
    direct = Path.joinpath(Path(__file__).parent.resolve(), 'migrations', 'versions').__str__()
    # Migration files
    files = listdir(direct)
    print(files)
    # File is found?
    found = False
    # Files content
    file_data = []
    # Get the most recent file and the migration creation date
    most_recent_file = ['', None]
    # Loop through to files
    for f in files:
        # Migration file is a python file
        if f.endswith('.py'):
            s = os.stat(os.path.join(direct, f))
            # print(f, s)
            print(datetime.datetime.fromtimestamp(s.st_ctime))
            # Find the migration creation date
            with open(os.path.join(direct, f), 'r', encoding='utf8') as fi:
                content = ''.join(fi.readlines())
                datet0 = content.split('Create Date:')[1]
                datetfinal = datet0.split('\n')[0]
                print(datetfinal)
                datetfinaliso = datetime.datetime.fromisoformat(datetfinal.strip())
                # First pass
                if most_recent_file[1] is None:
                    most_recent_file[0] = f
                    most_recent_file[1] = datetfinaliso
                # Compare migration dates
                elif datetfinaliso > most_recent_file[1]:
                    most_recent_file[0] = f
                    most_recent_file[1] = datetfinaliso
                print('most_recent_file', most_recent_file)
            print('----------')
            # Add to the files
            file_data.append(f)

    # Connect to Database
    engine = sa.create_engine(Configuration.DB_URI)

    # Database Meta Data
    meta = sa.MetaData(engine)

    # Create alembic version table
    version_table = "alembic_version"

    version_table_schema = "version_table_schema"

    _version = sa.Table(
        version_table,
        meta,
        sa.Column("version_num", sa.String(32), nullable=False),
        # schema=version_table_schema,
    )
    _version.append_constraint(
        sa.PrimaryKeyConstraint(
            "version_num", name="{0}_pkc".format(version_table)
        )
    )

    # CREATE TABLE alembic_version
    meta.create_all()

    # Remove all data from alembic_version
    engine.execute(_version.delete())

    # INSERT last migration version INTO alembic_version
    _inse = _version.insert().values(
        version_num=most_recent_file[0].split('_')[0]
    )
    engine.execute(_inse)

    # Get all Models
    Base = sa.orm.declarative_base()
    from models import Posts, Users

    # CREATE tables from Models
    Base.metadata.create_all(engine)

    # Remove Database Connection
    engine.dispose()

    print('Migrated all tables and schemas to databases.\nYou can now start using the app.')
