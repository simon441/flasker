# Create A Flask Blog - Flask Friday

by Codemy.com

Playlist: [https://youtube.com/playlist?list=PLCC34OHNcOtolz2Vd9ZSeSXWc8Bq23yEz]

1. Intro To Flask - Flask Fridays #1 [https://youtu.be/0Qxtt4veJIc]
2. Jinja2 Template Language and Filters - Flask Fridays #2 [https://youtu.be/4yaG-jFfePc]
3. Custom Error Pages and Version Control - Flask Fridays #3 [https://youtu.be/3O4ZmH5aolg]
4. Templates, Bootstrap Navbars, and Links - Flask Fridays #4 [https://youtu.be/y62Dvo2Ml7o]
5. Web Forms With WTF! - Flask Fridays #5 [https://youtu.be/GbJPqu0ff9A]
6. How To Use Messages With Flask - Flask Fridays #6
7. How to Use CSS Javascript and Images With Flask Static Files - Flask Fridays #7 [https://youtu.be/w54WLGm4OrE]
8. How to Use Databases With SQLAlchemy - Flask Fridays #8
9. How To Use MySQL Database With Flask - Flask Fridays #9
10. How To Update A Record In The Database - Flask Fridays #10
11. How To Migrate Database With Flask - Flask Fridays #11
12. Delete Database Records With Flask - Flask Fridays #12
13. Hashing Passwords With Werkzeug - Flask Fridays #13
14. Using Hashed Passwords For Registration - Flask Fridays #14
15. Comparing Hashed Passwords To Plaintext Passwords - Flask Fridays #15
16. How To Return JSON With Flask For an API - Flask Fridays #16
17. Add A Blog Post Model and Form - Flask Fridays #17
18. Show Blog Posts Page - Flask Fridays #18Individual
19. Blog Post Pages - Flask Fridays #19
20. Edit Blog Posts - Flask Fridays #20
21. Delete Blog Posts - Flask Fridays #21
22. User Login with Flask_Login - Flask Fridays #22
23. Create A User Dashboard - Flask Fridays #23
24. Two Ways To Lock Down Your Flask App - Flask Fridays #24
25. Update User Profile From Dashboard - Flask Fridays #25
26. Clean Up Our Flask Code! - Flask Fridays #26
27. Update Correct User - Flask Fridays #27
28. Create One To Many Database Relationship - Flask Fridays #28
29. Only Allow Correct User To Delete Posts - Flask Fridays #29
30. Only Allow Correct User To Edit Posts - Flask Fridays #30
31. Search Blog Posts From Navbar - Flask Fridays #31
32. How To Add A Rich Text Editor - Flask Fridays #32
33. Edit Blog Posts With Rich Text Editor - Flask Fridays #33
34. Basic Admin Page - Flask Fridays #34
35. Format Search Results - Flask Fridays #35
36. Set Default Profile Pic - Flask Fridays #36
37. About Author Section - Flask Fridays #37
38. Upload Profile Picture - Flask Fridays #38
39. Deploy Flask App With Database On Heroku For Webhosting - Flask Fridays #39
40. Only Delete Correct User - Flask Fridays #40
41. Fix And Show Profile Picture - Flask Fridays #41
42. Null Profile Pic? - Flask Fridays #42
43. Add Profile Pic To Blog Posts - Flask Fridays #43
44. Designate Admin To Delete Posts - Flask Fridays #44

## Usage

Install the server you want to use and start it except sqlite (it's a file).

Create a virtual environment: `python python -m venv .env`
Activate the environment:
    - on Windows: powershell: `powershell ./.env/Scripts/activate.ps1` command: `.\.env\Scripts\activate.bat`
    - on unix-like: `./.env/Scripts/activate`
Install dependencies: `pip install -r requirements.txt`

Rename or copy and rename `env.py.dist` as `env.py`.
Change the DB_URI to suit your credentials in the brackets []:

- mysql: `mysql+pymysql://[your_user_name]:[you_password]`
- mariaDB: `mariadb+pymysql://[your_user_name]:[you_password]@localhost/flask_users?charset=utf8mb4`
- Microsoft Sql Server: `mssql+pyodbc://?odbc_connect=DRIVER={ODBC Driver 17 for SQL Server};SERVER=(localdb)\MSSQLLocalDB;DATABASE=flask_users;` for LocalDB
- Postgresql: `postgresql+psycopg2://[your_user_name]:[you_password]@localhost/flask_users` or default user is `postgres:postgres`
- Sqlite:`sqlite+pysqlite:///db/flasker.sqlite3`

Change the `SECRET_KEY` to a new and more secure secret. For example use the result of the following command: `python -c 'import os; print(os.urandom(16))'`

Create the database (flask_users): `python create_database.py`
The script will create the database and also create all tables from the models (in the `models.py` file) using SqlAlchemy.

Start the server: Run `start.sh` script (linux like systems), `start.cmd` for Windows environments. For development use `./start.sh dev` or `start.cmd dev`, for production-like environnement run `./start.sh` or `start.cmd`

## flask commands added

Flask commands:

- `flask app dev`: development server (development mode)
- `flask app prod`: development server (production mode)
- `flask app start` or `flask app wsgi`: production ready WSGI server (default host is 0.0.0.0, default port: 80)
- `flask app test`: test the app (edit `SQL_DB_PATH` in the `env.py` file first following the instruction below)

## Deployment

Usage with the Waitress Module: [https://docs.pylonsproject.org/projects/waitress/en/latest/index.html]
For command-line option:

```bash
waitress-serve  --listen "*:3000"  --trusted-proxy '*'  --trusted-proxy-headers 'x-forwarded-for x-forwarded-proto x-forwarded-port'  --log-untrusted-proxy-headers  --clear-untrusted-proxy-headers  --threads 4  app:app
```

To deploy you can use the flasker.wsgi script with the command ```python flasker.wsgi``` which will start the `waitress` wsgi module. You can the visit the website at the address wanted (don't forget to edit the WSGI options in the `env.py` file to the one you have on your deployed host.)
To deploy to Heroku, there is a Proc file ready.

## Translations

The website was entirely translated to french.
Select box on the right side of the navbar can be used to change language. This choice is save in session.

to get all text to translate: `pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot .`.
PoEdit was used to extract the french text file from the messages.pot (`lc_messages/fr.po`).
PoEdit compiled the file to `fr.mo` copied into`translations/LC_MESSAGES/fr` as `messages.mo`.


## Testing

change `SQL_DB_PATH`  in your `env.py` file from `absolute/path/to/base/dir` to the absolute path to the root directory of this project

then run `flask app test`
