from pathlib import Path
from waitress import serve
from app import app, Configuration

import sys
import logging

logging.basicConfig(stream=sys.stderr)
# activate_this = str(Path.joinpath(Path.cwd(), 'venv', 'Scripts', 'activate'))
# with open(activate_this) as file_:
#     exec(file_.read(), dict(__file__=activate_this))
# print(str(Path.cwd()))
# sys.path.insert(
#     0, 'l:/programs/laragon_full/www/labs/codemy.com/flasker')

config = Configuration.SERVER_WSGI

if Configuration.SERVER_OPTIONS.get('log') == True:
    logger = logging.getLogger('waitress')
    logger.setLevel(logging.INFO)

if __name__ == '__main__':
    serve(app, **config)
