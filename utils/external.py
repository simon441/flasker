
import enum
from typing import Dict

from flask_babelplus import lazy_gettext

# Global configuration variables and constants
# File upload management
UPLOAD_FOLDER = 'static/images/uploads'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

LANGUAGES = [('en', lazy_gettext('English')), ('fr', lazy_gettext('French'))]


class Roles(enum.Enum):
    """"Users Roles"""
    admin = 'admin'
    user = 'user'

    @staticmethod
    def get_roles() -> Dict[str, str]:
        return dict((role.name, role.value) for role in Roles)

    @staticmethod
    def get_role(role: str) -> str:
        return Roles.get_roles().get(role, Roles.user.value)
