import re
from datetime import datetime
from typing import Dict

from flask import request, session
from flask_login import current_user
from jinja2 import escape, evalcontextfilter
from markupsafe import Markup

from ..app import app
from ..utils.external import UPLOAD_FOLDER, Roles
from ..utils.functions import is_admin, get_languages_from_files
from ..forms.webforms import LanguageSelectForm, SearchForm


@app.context_processor
def base() -> dict:
    """Pass stuff to base template"""
    form_search = SearchForm()

    # Selected language
    language_select = LanguageSelectForm()
    # Show the default language on the page
    language_select.language_selection.data = session.get('lang')

    return dict(form_search=form_search, is_admin=is_admin(current_user=current_user), language_select_form=language_select)


@evalcontextfilter
def nl2br(eval_ctx, value) -> str:
    """Convert new lines to <br> tag

    Args:
        eval_ctx ():context
        value (): Value to convert

    Returns:
        str: String with <br> tags intead of new lines (e.g.: \n or \r\n)
    """
    br = "<br/>\n"

    if eval_ctx.autoescape:
        value = escape(value)
        br = Markup(br)

    result = "\n\n".join(
        f"<p>{br.join(p.splitlines())}</p>"
        for p in re.split(r"(?:\r\n|\r(?!\n)|\n){2,}", value)
    )
    return Markup(result) if eval_ctx.autoescape else result


@app.template_filter('formatdatetime')
def format_datetime(value: datetime, format="%d %b %Y %I:%M %p") -> str:
    """Format a date time to (Default): d Mon YYYY HH:MM P"""
    if value is None:
        return ""
    return value.strftime(format)


@app.context_processor
def upload_dir() -> dict[str, str]:
    """Upload directory path"""
    return dict(upload_dir=UPLOAD_FOLDER.removeprefix('static/')+'/')


"""
Djaneiro filters:
safe
capitalize
lower
upper
title
trim
striptags
"""

# Jinja (others)


@app.context_processor
def get_role() -> Dict[str, str]:
    """Get All Users roles

    Returns:
        dict: Users Roles [(Role.name, Role.value)]
    """
    roles = dict([(role.name, str(role.value)) for role in Roles])
    # print(roles, type(roles), file=sys.stdout)
    return roles


@app.template_filter('form_error_class')
def form_error_class(klass: str, element_type: str, form_errors: dict, request_method: str) -> str:
    """form_error_class Get the class form form validation

    :param form_errors: Current Field Form errors
    :type form_errors: dict
    :param klass: Class name
    :type klass: str
    :return: class name with validation if there is one needed (POST form)
    :rtype: str
    """
    print(klass, form_errors, request_method)
    # input, select, textarea,...
    if element_type == 'element':
        if request_method == 'POST':
            # klass += ' is-valid' if len(form_errors)==0 else ' is-invalid'
            if len(form_errors) > 0:
                klass += ' is-invalid'
        # {% set help_class = 'invalid-feedback' %}
            else:
                klass += ' is-valid'
        return klass

    # valid/invalid text help
    if element_type == 'help':
        if request_method == 'POST':
            # klass += ' is-valid' if len(form_errors)==0 else ' is-invalid'
            if len(form_errors) > 0:
                klass += ' invalid-feedback'
        # {% set help_class = 'invalid-feedback' %}
            else:
                klass += ' valid-feedback'
        return klass
    return klass


@app.template_filter('highlight_word2')
def highlight_word(text: str, word: str, tag: str = '') -> str:
    word = word.strip()
    tag_start = '<span class="highlight_word">'
    tag_end = '<span>'

    result = text.replace(word, f'{tag_start}{word}{tag_end}')
    m = Markup(result)
    print(m)
    return m
