from logging import Logger
import os
from pathlib import Path
from typing import Dict

from flask import json, request, session
import jinja2
from flask_login.utils import LocalProxy
from werkzeug.http import dump_cookie, parse_cookie

from ..app import app
from ..utils.external import ALLOWED_EXTENSIONS


def is_admin(current_user: LocalProxy) -> bool:
    """Check if the current user is an admin"""
    try:
        return current_user.id == 1
    except AttributeError as e:
        return False


def allowed_file(filename: str) -> bool:
    """Has this file an allowed extension?"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def get_languages_from_files(default_language='en') -> list[set]:
    """Returns a list of all the locales translations exist for.  The
    list returned will be filled with actual locale objects and not just
    strings.

    .. versionadded:: 0.6
    """
    print('hello')
    # XXX: Wouldn't it be better to list the locales from the domain?
    dirname = os.path.join(
        str(Path(__file__).parent.resolve()), 'translations')
    print(dirname)
    result = []

    result.append(default_language)
    if not os.path.isdir(dirname):
        return result
    for folder in os.listdir(dirname):
        locale_dir = os.path.join(dirname, folder, 'LC_MESSAGES')
        if not os.path.isdir(locale_dir):
            continue
        p = os.path.basename(locale_dir)
        result.append(p)
        # if filter(lambda x: x.endswith('.mo'), os.listdir(locale_dir)):
        # #    result.append()
    # if not result:
    # result.append(Locale.parse(self.default_locale))
    return result


def add_cookie(response, value):
    """Add lang to flask cookie"""
    response.headers['X-Foo'] = 'Parachute'
    # response.set_cookie('flasker', 'lang')
    try:
        decoder = json.JSONDecoder()
        values: dict = decoder.decode(request.headers['Cookie'])['flasker']
        values.update(value)
    except Exception:
        values = value
    encoder = json.JSONEncoder()
    l = encoder.encode(values)
    cookie = dump_cookie('flasker', l, httponly=True, samesite='Lax')
    response.headers['Set-Cookie'] = cookie
    return response


def get_lang():
    """Get the current language in the session or in the cookie if not available"""
    # print('gggggggggggggggggggggggggggg')
    # print(request.cookies, dir(request), request.headers)
    # print(request.headers['Cookie'])
    try:
        # del(session['lang'])
        decoder = json.JSONDecoder()
        if request.headers['Cookie']:
            # print('cookieeeeeeeeeeeeeee')
            try:
                cookie = parse_cookie(request.headers['Cookie'])['flasker']
                print('cookie', cookie)
            except KeyError:
                cookie = {'lang': 'en'}
            lang = decoder.decode(cookie)['lang']

            # print('lllllllllll', cookie, lang['lang'])
            session['lang'] = lang if lang is not None else 'en'
            # print("session['lang']", session['lang'])
        else:
            raise Exception('')
    except Exception as e:
        print(e)
        print('no lang found switch back to en')
        if not 'lang' in session:
            session['lang'] = 'en'


def get_current_lang_in_session() -> str:
    """Get the current language selected in the session
    Returns:
        The language as a ISO-formatted string of two characters
    """
    return session['lang'] if 'lang' in session else 'en'


def get_meta_for_form() -> dict[str, list[str]]:
    """get_meta_for_form Get the Meta language for the form

    Returns:
        A list of languages
    """
    return {'locales': [get_current_lang_in_session()]}

# class Cookie(object):

#     def __init__(self, request, response) -> None:
#         self.request = request
#         self.response = response

#     def set_cookie():
#            try:
#                 decoder = json.JSONDecoder()
#                 values: dict = decoder.decode(flaskrequest.headers['Cookie'])['flasker']
#                 values.update(value)
#             except Exception:
#                 values = value
#             encoder = json.JSONEncoder()
#             l = encoder.encode(values)
#             cookie = dump_cookie('flasker', l, httponly=True, samesite='Lax')

#         set_cookie(cookie_name,
#                             value=data,
#                             expires=expires,
#                             domain=domain,
#                             path=path,
#                             secure=secure,
#                             httponly=httponly)


def get_logger() -> Logger:
    import logging
    logging.basicConfig(level=logging.INFO)
    logging = logging.getLogger()
    return logging


# def transform_posts_as_json(posts: Dict[Posts]) -> Dict[Posts]:
#     for post in posts:
#         post.content = transform_content(post.content)
#     return posts


def transform_content(content: str) -> str:
    return jinja2.filters.do_mark_safe(content)
