import datetime
import os
import sys
from copy import copy
from os import listdir
from pathlib import Path

import pyodbc
import sqlalchemy as sa
from sqlalchemy.engine.url import make_url
from sqlalchemy_utils.functions import quote


class DatabaseManager(object):
    def __init__(self, database_uri: str) -> None:
        self.db_driver = database_uri

    def _set_url_database(self, url: sa.engine.url.URL, database):
        """Set the database of an engine URL.

        :param url: A SQLAlchemy engine URL.
        :param database: New database to set.

        """
        if hasattr(sa.engine, 'URL'):
            if database:
                ret = sa.engine.URL.create(
                    drivername=url.drivername,
                    username=url.username,
                    password=url.password,
                    host=url.host,
                    port=url.port,
                    database=database,
                    query=url.query
                )
            else:
                ret = sa.engine.URL.create(
                    drivername=url.drivername,
                    username=url.username,
                    password=url.password,
                    host=url.host,
                    port=url.port,
                    query=url.query
                )
        else:  # SQLAlchemy <1.4
            url.database = database
            ret = url
        assert ret.database == database, ret
        return ret

    def create_database(self, url, encoding='utf8', template=None, create_db_extra_text: str = None):
        """Issue the appropriate CREATE DATABASE statement.

        :param url: A SQLAlchemy engine URL.
        :param encoding: The encoding to create the database as.
        :param template:
            The name of the template from which to create the new database. At the
            moment only supported by PostgreSQL driver.

        To create a database, you can pass a simple URL that would have
        been passed to ``create_engine``. ::

            create_database('postgresql://postgres@localhost/name')

        You may also pass the url from an existing engine. ::

            create_database(engine.url)

        Has full support for mysql, postgres, and sqlite. In theory,
        other database engines should be supported.
        """

        url = copy(make_url(url))
        database = url.database
        dialect_name = url.get_dialect().name
        dialect_driver = url.get_dialect().driver

        if dialect_name == 'postgresql':
            url = self._set_url_database(url, database="postgres")
        elif dialect_name == 'mssql':
            url = self._set_url_database(url, database="master")
        elif not dialect_name == 'sqlite':
            url = self._set_url_database(url, database=None)
        if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
                or (dialect_name == 'postgresql' and dialect_driver in {
                'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
            engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
        else:
            engine = sa.create_engine(url)

        if dialect_name == 'postgresql':
            if not template:
                template = 'template1'

            text = "CREATE DATABASE {0} WITH ENCODING '{1}' TEMPLATE {2}".format(
                quote(engine, database),
                encoding,
                quote(engine, template)
            )

            with engine.connect() as connection:
                connection.execute(text)

        elif dialect_name == 'mysql':
            if create_db_extra_text:
                create_db_extra_text = "COLLATE '{0}'".format(create_db_extra_text)
            text = "CREATE DATABASE {0} CHARACTER SET '{1}' {2}".format(
                quote(engine, database),
                encoding,
                create_db_extra_text,
            )

            with engine.connect() as connection:
                connection.execute(text)

        elif dialect_name == 'sqlite' and database != ':memory:':
            if database:
                with engine.connect() as connection:
                    connection.execute("CREATE TABLE DB(id int);")
                    connection.execute("DROP TABLE DB;")

        elif dialect_name == 'mssql':
            text = 'CREATE DATABASE {0}'.format(quote(engine, database))
            with engine.connect() as connection:
                connection.execute('USE master;')
                connection.execute(text)

        else:
            text = 'CREATE DATABASE {0}'.format(quote(engine, database))
            with engine.connect() as connection:
                connection.execute(text)

        engine.dispose()

    def connect_to_database(self, url) -> sa.engine.Engine:

        url = copy(make_url(url))
        database = url.database
        dialect_name = url.get_dialect().name
        dialect_driver = url.get_dialect().driver

        if dialect_name == 'postgresql':
            url = self._set_url_database(url, database="postgres")
        elif dialect_name == 'mssql':
            url = self._set_url_database(url, database="master")
            url = str(url).split("DATABASE%3D")[0]
        elif not dialect_name == 'sqlite':
            url = self._set_url_database(url, database=None)

        if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
                or (dialect_name == 'postgresql' and dialect_driver in {
                'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
            engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
        else:
            engine = sa.create_engine(url)
        return engine

    def disconnect_from_database(self, engine: sa.engine.Engine):
        """Remove database connection

        Args:
            engine (sa.engine.Engine): Database connection
        """
        engine.dispose()

    def get_driver_specs(self, url: str) -> list[str]:
        """Returns the Driver specifications

        Args:
            url (str): Connection String

        Returns:
            list[str]: Driver Description
        """
        def get_charset(charsets: list, charset: str) -> set[str]:
            """Get the MySql/MariaDB available character set if found

            Args:
                charsets (list): Character sets available
                charset (str): Character set to find

            Returns:
                set[str]: The character set data if found else an empty dictionary
            """
            for c in charsets:
                # print(c, charset, charset == c[0])

                if charset == c[0]:
                    # print('ok')
                    # print(c)
                    return c
                else:
                    continue
                    # print('fail')
            return {}

        charset_found = []

        url = copy(make_url(url))
        print(url)
        database = url.database
        dialect_name = url.get_dialect().name
        dialect_driver = url.get_dialect().driver

        print(url.get_dialect(), url)
        print(dir(url.get_dialect()))
        if dialect_name == 'postgresql':
            url = self._set_url_database(url, database="postgres")
        elif dialect_name == 'mssql':
            if database is None:
                database = "master"
            url = self. _set_url_database(url, database="master")
            # Remove the DATABASE name from the URI
            url = str(url).split("DATABASE%3D")[0]  # + 'DATABASE%3D"master"%3D'

        elif not dialect_name == 'sqlite':
            url = self._set_url_database(url, database=None)
        if (dialect_name == 'mssql' and dialect_driver in {'pymssql', 'pyodbc'}) \
                or (dialect_name == 'postgresql' and dialect_driver in {
                'asyncpg', 'pg8000', 'psycopg2', 'psycopg2cffi'}):
            engine = sa.create_engine(url, isolation_level='AUTOCOMMIT')
        else:
            engine = sa.create_engine(url)
        conn = engine.connect()
        # print(conn.connection)
        # print(conn.connection.driver_connection)
        # print(dir(conn.connection))
        # print(dir(conn.connection.driver_connection))

        if dialect_driver == 'mysql':
            # print(url.get_dialect().is_mariadb, url.get_dialect().description_encoding)

            # print(conn.connection.driver_connection.server_charset)
            # print(conn.connection.driver_connection.get_host_info())
            # print(conn.connection.driver_connection.encoding)
            # print(conn.connection.driver_connection.charset)
            # print(conn.execute("SHOW VARIABLES").fetchall())
            charsets = conn.execute("SHOW CHARACTER SET;").fetchall()
            try:
                # Get the charset passed in the Connection String
                charset = self.db_driver.split('charset=')[-1]
            except IndexError:
                charset = 'utf8'
            charset_found = get_charset(charsets, charset)
            # found a charset
            if len(charset_found) != 4:
                charset_found = ['utf8', 'utf8']
        else:
            pass
        conn.close()
        return charset_found
