import datetime
import os
import sys
from copy import copy
from os import listdir
from pathlib import Path
from typing import Union
import click

import pyodbc
import sqlalchemy as sa
import sqlalchemy_utils
from sqlalchemy.engine.url import make_url
from sqlalchemy_utils.functions import quote
from .database_manager import DatabaseManager

from ...env import Configuration


class CreateDatabase(object):
    DEFAULT_DATABASE_ENCODING = 'utf8'
    DEFAULT_DATABASE_EXTRA_OPTIONS = ''

    def __init__(self, app_root_path: str, database_uri: str, db_manager: DatabaseManager) -> None:
        """__init__ 

        :param app_root_path: Flask app root dir
        :type app_root_path: str
        :param database_uri: Database URI to connect to
        :type database_uri: str
        :param db_manager: Database Manager
        :type db_manager: DatabaseManager
        """
        self.app_root_path = app_root_path
        self.db_driver: str = database_uri
        self.db_manager = db_manager
        self.specs = []  # list of database specifications

        self.create_the_database: bool = True  # User wants to create the database
        # Does the users want to create the database tables
        self.create_the_tables: bool = True
        self.most_recent_file = []  # the most recent migration file

        self.configure()

    def configure(self) -> None:
        """configure Configure tha database options
        """
        self.encoding = self.DEFAULT_DATABASE_ENCODING
        self.extra_text = self.DEFAULT_DATABASE_EXTRA_OPTIONS
        print(type(self.db_driver), self.db_driver, repr(self.db_driver))
        # Mysql driver type
        if self.db_driver.startswith('mysql'):
            specs = self.db_manager.get_driver_specs(self.db_driver)
            self.encoding = 'utf8mb4' if len(specs) < 1 else specs[0]
            self.extra_text = 'utf8mb4_general_ci' if len(
                specs) < 2 else specs[1]
            print(self.encoding, self.extra_text)
        else:
            # Other drivers
            specs = self.db_manager.get_driver_specs(self.db_driver)
            print(specs)
        self.specs = specs

    def database_exists(self) -> bool:
        # Does the database already exist?
        try:
            db_exists = sqlalchemy_utils.database_exists(self.db_driver)
        except sa.exc.InterfaceError:
            db_exists = False
        return db_exists

    def input_database(self):
        # Create the database?
        self._create_db = True
        # print('dbexists', db_exists)
        # Override, DROP and CREATE Database
        if self.database_exists():
            create_db = click.confirm(
                'Drop database and create it again?', default=False)
            if create_db:
                # MS SQl Server
                if self.db_driver.startswith('mssql'):
                    engine = self.db_manager.connect_to_database(
                        self.db_driver)
                    database = self.db_driver.split('DATABASE=')[1]
                    engine.connect()
                    with engine.begin() as conn:
                        conn.execute('DROP DATABASE {0}'.format(database))
                else:
                    sqlalchemy_utils.drop_database(self.db_driver)
                print('Database {0} DROP successfull'.format(self.db_driver))

    def create_database(self, extra_text: str = "") -> Union[bool, None]:
        """create_database Create the database

            Returns: 
                description: database creation status (False/True) if success or failure, None if no action was performed

        """
        # User does not want to CREATE database or  Database found
        if not self.create_the_database:
            return None

        # Check if database exists
        exists = self.database_exists()
        # User wants to CREATE database or no Database found
        if exists:
            # MS Sql Server
            if self.db_driver.startswith('mssql'):
                engine = self.db_manager.connect_to_database(self.db_driver)
                database = self.db_driver.split('DATABASE=')[1]
                engine.connect()
                with engine.begin() as conn:
                    conn.execute('CREATE DATABASE {0}'.format(database))
            else:
                self.db_manager.create_database(
                    self.db_driver, encoding=self.encoding, create_db_extra_text=extra_text)

            # Could not create database
            if not self.database_exists():
                print('Could not create database')
                return False
            print('flask_users Database ' +
                  ('created' if self.database_exists() else 'already exists'))
            return True
        return None

    def tables_exists(self) -> bool:
        # Get a list of the available database tables
        t = sa.MetaData(self.db_driver)

        return len(t.sorted_tables) > 0

    def confirm_create_tables(self) -> bool:
        """"Get confirmation for creating the tables"""
        t = sa.MetaData(self.db_driver)
        self.create_the_tables = True

        if self.database_exists() or len(t.sorted_tables) > 0:
            self.create_the_tables = click.confirm('Create all (yes/no)? ')
            # Want to create tables then must remove them first
            if self.create_the_tables:
                t.drop_all()
                print('DROP all Tables')
        return self.create_the_tables

    def seek_last_migration(self):
        """Get the last migration from the migrations directory"""

        direct = Path.joinpath(
            Path(self.app_root_path), 'migrations', 'versions').__str__()
        # Migration files
        files = listdir(direct)
        # print(files)
        # File is found?
        found = False
        # Files content
        file_data = []
        # Get the most recent file and the migration creation date
        self.most_recent_file = ['', None]
        # Loop through to files
        for f in files:
            # Migration file is a python file
            if f.endswith('.py'):
                s = os.stat(os.path.join(direct, f))
                # print(f, s)
                # print(datetime.datetime.fromtimestamp(s.st_ctime))
                # Find the migration creation date
                with open(os.path.join(direct, f), 'r', encoding='utf8') as fi:
                    content = ''.join(fi.readlines())
                    datet0 = content.split('Create Date:')[1]
                    datetfinal = datet0.split('\n')[0]
                    # print(datetfinal)
                    datetfinaliso = datetime.datetime.fromisoformat(
                        datetfinal.strip())
                    # First pass
                    if self.most_recent_file[1] is None:
                        self.most_recent_file[0] = f
                        self.most_recent_file[1] = datetfinaliso
                    # Compare migration dates
                    elif datetfinaliso > self.most_recent_file[1]:
                        self.most_recent_file[0] = f
                        self.most_recent_file[1] = datetfinaliso
                    print('self.most_recent_file', self.most_recent_file)
                print('----------')
                # Add to the files
                file_data.append(f)

    def create_tables(self):
        """Create the tables"""
        # Connect to Database
        engine = sa.create_engine(self.db_driver)
        # Database Meta Data
        meta = sa.MetaData(engine)

        # Create alembic version table
        version_table = "alembic_version"

        # version_table_schema = "version_table_schema"

        _version = sa.Table(
            version_table,
            meta,
            sa.Column("version_num", sa.String(32), nullable=False),
            # schema=version_table_schema,
        )
        _version.append_constraint(
            sa.PrimaryKeyConstraint(
                "version_num", name="{0}_pkc".format(version_table)
            )
        )

        # CREATE TABLE alembic_version
        meta.create_all()

        # Remove all data from alembic_version
        engine.execute(_version.delete())

        # INSERT last migration version INTO alembic_version
        _inse = _version.insert().values(
            version_num=self.most_recent_file[0].split('_')[0]
        )
        engine.execute(_inse)

        # Get all Models
        # Base = sa.orm.declarative_base()
        from ...app import app
        from ...models.models import Posts, Users, Base

        # CREATE tables from Models
        Base.metadata.create_all(engine)

        # Remove Database Connection
        engine.dispose()
        from ...app import db
        db.create_engine(sa_url=self.db_driver, engine_opts={})
        db.create_all()
        print('Migrated all tables and schemas to databases.\nYou can now start using the app.')

    # @property.getter
    # def get_create_db(self, prop):
    #     return self._create_db

    # @property.setter
    # def set_create_db(self, prop, value):
    #     AttributeError('Cannot assign to create_db')


def create_database(app_root_path: str, db_driver: str) -> None:
    """create_database Create a database from its URI

    :param app_root_path: Flask app root dir path
    :type app_root_path: str
    :param db_driver: Driver URI
    :type db_driver: str
    """

    db_manager = DatabaseManager(db_driver)
    # print(db_driver)

    print('connected to "{0}"'.format(db_driver))

    create_db = CreateDatabase(app_root_path, db_driver, db_manager)
    # Create database
    create_db.input_database()
    status = create_db.create_database()
    if status == False:
        sys.exit(-1)
     # Create tables
    create_tables = create_db.confirm_create_tables()
    print('create_tables', create_tables)
    if create_tables:
        create_db.seek_last_migration()
        create_db.create_tables()

    return

    encoding = 'utf8'
    extra_text = ''
    print(type(db_driver), db_driver, repr(db_driver))
    # Mysql driver type
    if db_driver.startswith('mysql'):
        specs = get_driver_specs(db_driver)
        encoding = 'utf8mb4' if len(specs) < 1 else specs[0]
        extra_text = 'utf8mb4_general_ci' if len(specs) < 2 else specs[1]
        print(encoding, extra_text)
    else:
        # Other drivers
        specs = get_driver_specs(db_driver)
        print(specs)

    # Database already exists?
    try:
        db_exists = sqlalchemy_utils.database_exists(db_driver)
    except sa.exc.InterfaceError:
        db_exists = False

    # Create the database?
    create_db = True
    # print('dbexists', db_exists)
    # Override, DROP and CREATE Database
    if db_exists:
        create_db = click.confirm(
            'Drop database and create it again?', default=False)
        if create_db:
            # MS SQl Server
            if db_driver.startswith('mssql'):
                engine = connect_to_database(db_driver)
                database = db_driver.split('DATABASE=')[1]
                engine.connect()
                with engine.begin() as conn:
                    conn.execute('DROP DATABASE {0}'.format(database))
            else:
                sqlalchemy_utils.drop_database(db_driver)
            print('Database {0} DROP successfull'.format(db_driver))

    # User wants to CREATE database or no Database found
    if create_db:
        # MS Sql Server
        if db_driver.startswith('mssql'):
            engine = connect_to_database(db_driver)
            database = db_driver.split('DATABASE=')[1]
            engine.connect()
            with engine.begin() as conn:
                conn.execute('CREATE DATABASE {0}'.format(database))
        else:
            engine = create_database(
                db_driver, encoding=encoding, create_db_extra_text=extra_text)

        # Could not create database
        if not database_exists(db_driver):
            print('Could not create database')
            sys.exit(-1)

    print('flask_users Database ' + ('created' if db_exists else 'already exists'))
    # Does the user want to create the Database Tables
    create_tables = True

    # db = engine.connect()

    # Get a list of the available database tables
    t = sa.MetaData(db_driver)

    # Creating tables if exists
    if db_exists or len(t.sorted_tables) > 0:
        create_tables = click.confirm('Create all (yes/no)? ')
        # Want to create tables then must remove them first
        if create_tables:
            t.drop_all()
            print('DROP all Tables')

    # User wants to CREATE tables or no tables found
    if create_tables:
        # Get the last migration from the migrations directory
        direct = Path.joinpath(
            Path(__file__).parent.resolve(), 'migrations', 'versions').__str__()
        # Migration files
        files = listdir(direct)
        # print(files)
        # File is found?
        found = False
        # Files content
        file_data = []
        # Get the most recent file and the migration creation date
        most_recent_file = ['', None]
        # Loop through to files
        for f in files:
            # Migration file is a python file
            if f.endswith('.py'):
                s = os.stat(os.path.join(direct, f))
                # print(f, s)
                # print(datetime.datetime.fromtimestamp(s.st_ctime))
                # Find the migration creation date
                with open(os.path.join(direct, f), 'r', encoding='utf8') as fi:
                    content = ''.join(fi.readlines())
                    datet0 = content.split('Create Date:')[1]
                    datetfinal = datet0.split('\n')[0]
                    # print(datetfinal)
                    datetfinaliso = datetime.datetime.fromisoformat(
                        datetfinal.strip())
                    # First pass
                    if most_recent_file[1] is None:
                        most_recent_file[0] = f
                        most_recent_file[1] = datetfinaliso
                    # Compare migration dates
                    elif datetfinaliso > most_recent_file[1]:
                        most_recent_file[0] = f
                        most_recent_file[1] = datetfinaliso
                    print('most_recent_file', most_recent_file)
                print('----------')
                # Add to the files
                file_data.append(f)

        # Connect to Database
        engine = sa.create_engine(Configuration.DB_URI)

        # Database Meta Data
        meta = sa.MetaData(engine)

        # Create alembic version table
        version_table = "alembic_version"

        # version_table_schema = "version_table_schema"

        _version = sa.Table(
            version_table,
            meta,
            sa.Column("version_num", sa.String(32), nullable=False),
            # schema=version_table_schema,
        )
        _version.append_constraint(
            sa.PrimaryKeyConstraint(
                "version_num", name="{0}_pkc".format(version_table)
            )
        )

        # CREATE TABLE alembic_version
        meta.create_all()

        # Remove all data from alembic_version
        engine.execute(_version.delete())

        # INSERT last migration version INTO alembic_version
        _inse = _version.insert().values(
            version_num=most_recent_file[0].split('_')[0]
        )
        engine.execute(_inse)

        # Get all Models
        # Base = sa.orm.declarative_base()
        from models import Posts, Users, Base

        # CREATE tables from Models
        Base.metadata.create_all(engine)

        # Remove Database Connection
        engine.dispose()

        print('Migrated all tables and schemas to databases.\nYou can now start using the app.')
