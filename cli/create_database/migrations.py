from datetime import datetime
from os import listdir
import os
from pathlib import Path


def last_migrations():
    # Get the last migration from the migrations directory
    direct = Path.joinpath(
        Path(__file__).parent.resolve(), 'migrations', 'versions').__str__()
    # Migration files
    files = listdir(direct)
    # print(files)
    # File is found?
    found = False
    # Files content
    file_data = []
    # Get the most recent file and the migration creation date
    most_recent_file = ['', None]
    # Loop through to files
    for f in files:
        # Migration file is a python file
        if f.endswith('.py'):
            s = os.stat(os.path.join(direct, f))
            # print(f, s)
            # print(datetime.datetime.fromtimestamp(s.st_ctime))
            # Find the migration creation date
            with open(os.path.join(direct, f), 'r', encoding='utf8') as fi:
                content = ''.join(fi.readlines())
                datet0 = content.split('Create Date:')[1]
                datetfinal = datet0.split('\n')[0]
                # print(datetfinal)
                datetfinaliso = datetime.datetime.fromisoformat(
                    datetfinal.strip())
                # First pass
                if most_recent_file[1] is None:
                    most_recent_file[0] = f
                    most_recent_file[1] = datetfinaliso
                # Compare migration dates
                elif datetfinaliso > most_recent_file[1]:
                    most_recent_file[0] = f
                    most_recent_file[1] = datetfinaliso
                print('most_recent_file', most_recent_file)
            print('----------')
            # Add to the files
            file_data.append(f)
