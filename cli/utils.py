def yes_no(answer: str) -> bool:
    """Test yes/no and return True or False depending on answer

    Args:
        answer (str): question to the user

    Returns:
        bool: Yes=True, No=False
    """
    yes = set(['yes', 'y', 'ye', ''])
    no = set(['no', 'n'])

    while True:
        choice = input(answer).lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'yes' or 'no'\n")
