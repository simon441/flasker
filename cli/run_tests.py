
import subprocess
from typing import List


def run_tests(name: str, *args:List) -> None:
    if name:
        subprocess.run('pytest --log-cli-level=10 -s' + name)
    else:
        subprocess.run('pytest --log-cli-level=10 -s')
