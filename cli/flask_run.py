import os
import subprocess
from flask.cli import with_appcontext


@with_appcontext
def app_run(debug: bool = True, env: str = 'development') -> None:
    """app_run Run the Flask App

    :param debug: Debug Mode, defaults to True
    :type debug: bool, optional
    :param env: Environment to run in, defaults to 'development'
    :type env: str, optional
    """
    os.environ.setdefault('FLASK_ENV', env)
    os.environ.setdefault('FLASK_APP', 'app.py')
    subprocess.run('flask run')
