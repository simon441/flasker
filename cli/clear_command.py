import os
from pathlib import Path
import sys


def cli_clear(root_dir: Path, db_path: str) -> None:
    files = os.listdir(root_dir)
    for file in files:
        if not file.startswith('flasker') and (file.endswith('test.db') or (file.endswith('.sqlite3') and file.find('test') != -1)):
            os.remove(os.path.join(root_dir, file))
            print(f'Deleted file {os.path.join(root_dir, file)}')

    for dirpath, dirname, files in os.walk(db_path):
        for file in files:
            if not file.startswith('flasker') and (file.endswith('.db') or file.endswith('.sqlite3')):
                os.remove(os.path.join(dirpath, file))
                print(f'Deleted file {os.path.join(dirpath, file)}')


def clear_pycache(app_root_path: str) -> None:
    """clear_pycache Clear all __pycache__ directories

    :param app_root_path: Application root path
    :type app_root_path: str
    """
    exclude = []
    if app_root_path.lower() in sys.executable:
        exclude_path = sys.executable.replace(app_root_path.lower(), '').replace('\\', '\\')
        exclude_path = exclude_path.strip('\\').split('\\')[0]
        print('1', exclude_path)
        exclude_path = app_root_path + '\\' + exclude_path
        print('2', exclude_path)

        exclude.append(exclude_path)

        # exclude.index()
        # print('dedzed')

    folders_to_scan = []
    for filenames in os.listdir(app_root_path):
        current_path: str = os.path.join(app_root_path, filenames)
        # print(filenames, app_root_path+filenames, os.path.isdir(current_path))
        if os.path.isdir(current_path):
            if filenames.startswith('.'):
                continue
            if current_path in exclude:
                continue
            folders_to_scan.append(current_path)
    print(folders_to_scan)

    for dirs in folders_to_scan:
        for dirpath, dirname, files in os.walk(dirs):
            # print(dirpath, dirname, files)
            if dirpath.endswith('__pycache__'):
                for file in files:
                    if file.endswith('.pyc'):
                        os.remove(os.path.join(dirpath, file))
                        print(f'Deleted file {os.path.join(dirpath, file)}')
