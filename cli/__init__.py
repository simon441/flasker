import os
import subprocess
import sys
from distutils import sysconfig
from distutils.util import execute

import click
from click import Abort, Choice, echo
from flask import current_app
from flask.cli import (AppGroup, FlaskGroup, platform, run_command,
                       with_appcontext)
from ..app import Configuration, app
from .clear_command import clear_pycache, cli_clear
from .create_database.create_database import create_database
from .environement import generate_env
from .flask_run import app_run
from .run_tests import run_tests

################# Function ##########


################# Commands ##########


me = AppGroup('app')
app.cli.add_command(me)


@me.command('create-database', short_help='Create the database and the tables based on DB_URI.')
@with_appcontext
def create_database_cmd():
    create_database(app.root_path, Configuration.DB_URI)


init = AppGroup('init')


@init.command('env')
@with_appcontext
def init_env():

    generate_env()
    return

    import secrets

    var = """
    from .app import app
from .config import Configuration as BaseConfiguration


class Configuration(BaseConfiguration):
    \"""Override default Flask configuration parameters

    Args:
        BaseConfiguration(MetaFlaskEnv): Flask Configuration variables
        This class has access to the Flask app(already initialized)
    \"""

    # DEBUG = True
    PORT = 5000
    SECRET_KEY = $$SECRET_KEY$$
    SERVER_WSGI = {'host': $$HOST$$, 'port': $$PORT$$}  # serve() options
    SERVER_OPTIONS = {'log': True}  # log: logging enabled?

    if app.debug == True:
        # Enable Debug toolbar
        DEBUG_TB_ENABLED = True

    """
    # try:
    # Database name
    database_name = click.prompt(
        'Database name', 'flask_users', confirmation_prompt=True, show_default=True)
    echo(database_name)

    # Database Types
    names: list[str] = sorted(
        ['SqlServer', 'MySql', 'MariaDB', 'Sqlite', 'PostGresql', '.exit'])
    servers_choice: list[dict] = []
    servers_list: list[str] = []
    while True:
        choices = click.Choice(choices=names, case_sensitive=False)
        server_value = click.prompt('Choose server type (.exit to quit): ', None, show_choices=True,
                                    type=choices, confirmation_prompt=True)
        print(server_value)
        if server_value.lower() in ['mysql', 'mariadb']:
            host = click.prompt('db host: ', '127.0.0.1')
            port = click.prompt('db port: ', 3036)
            username = click.prompt('db user: ', 'root')
            password = click.prompt('db password: ', 'root', hide_input=True)
            charset = click.prompt('db charset: ', 'utf8mb4')
            servers_choice.append({'server': server_value, 'host': host, 'port': port,
                                   'username': username, 'password': password})
            driver = 'mysql+pymysql://'
            if server_value.lower() == 'mariadb':
                driver = 'mariadb+pymysql://'
            s = f'{driver}{username}:{password}@{host}:{port}/{database_name}?charset={charset}'

        elif server_value.lower() in ['postgresql']:
            host = click.prompt('db host: ', 'localhost1')
            port = click.prompt('db port: ', '')
            username = click.prompt('db user: ', 'postgres')
            password = click.prompt(
                'db password: ', 'postgres', hide_input=True)
            servers_choice.append({'server': server_value, 'host': host, 'port': port,
                                   'username': username, 'password': password})
            s = DB_URI = f'postgresql+psycopg2://{username}:{password}@{host}/{database_name}'

        elif server_value.lower() in ['sqlserver']:
            # '?odbc_connect=DRIVER={ODBC Driver 17 for SQL Server};SERVER=(localdb)\MSSQLLocalDB;DATABASE=flask_users;'
            host = click.prompt('db host: ', '(localdb)\MSSQLLocalDB')
            username = click.prompt('db user: ', '')
            password = click.prompt('db password: ', '', hide_input=True)
            servers_choice.append({'server': server_value, 'host': host, 'port': port,
                                   'username': username, 'password': password})
            driver = '{ODBC Driver 17 for SQL Server}'
            s = f"DRIVER={driver};SERVER={driver};DATABASE={database_name}"
            if username:
                s += ';UID='+username
            if password:
                if s.endswith(';'):
                    s += 'PWD='+password
                else:
                    s += ';PWD='+password
        elif server_value.lower() in ['sqlite']:
            path: str = click.prompt('db path: ', '', hide_input=True)
            servers_choice.append({'server': server_value, 'path': path})
            if path != '':
                path = path + '/'
            s = DB_URI = f'sqlite+pysqlite:///{path}{database_name}'
        elif server_value.lower() == '.exit':
            if click.confirm('Do you want to exit the database selection(y/n)?', True):
                break
        else:
            continue
        s = f"#{server_value}\n{s}\n\n"
        servers_list.append(s)
    # servers: set = set(servers_choice)

    # Port
    port = click.prompt('Port', 5000, confirmation_prompt=True,
                        show_default=True, type=int)
    echo(port)

    # Autogenerate secret key
    secret_key = secrets.token_hex()

    # WSGI
    wsgi_host = click.prompt('WSGI Host', '0.0.0.0',
                             confirmation_prompt=True, show_default=True)
    wsgi_port = click.prompt(
        'WSGI Port', 80, confirmation_prompt=True, show_default=True, type=int)
    SERVER_WSGI = {'host': wsgi_host, 'port': wsgi_port}  # serve() options
    SERVER_OPTIONS = {'log': True}  # log: logging enabled?

    env_file = var.replace('$$PORT$$', str(port))
    env_file = env_file.replace('$$SECRET_KEY$$', str(secret_key))
    env_file = env_file.replace('$$HOST$$', str(wsgi_host))
    env_file = env_file.replace('$$DB_URI$$', '\n'.join(servers_list))
    # db_values = []
    # for server in servers_list:
    #     print(server)
    #     # db_values[] = '#' + server

    #     # if server.lower() == 'mysql':
    #     #     db_values.append(f"""DB_URI='mysql+pymysql://{}:root@localhost/flask_users?charset=utf8mb4'""")
    # sys.exit(0)

    with open('env.init.py', 'w', encoding='utf8', newline="\n") as f:
        f.write(env_file)
        echo('Successfully written environment configuration to "env.init.py".\nPlease rename the file to ".env" before using the app.')

# run_me = AppGroup('app')


@me.command('start')
@me.command('wsgi')
@me.command('run')
@with_appcontext
def run():
    import logging

    from waitress import serve
    logging.basicConfig(stream=sys.stderr)

    config = Configuration.SERVER_WSGI
    print(config)
    config.setdefault('host', '0.0.0.0')

    if Configuration.SERVER_OPTIONS.get('log') == True:
        logger = logging.getLogger('waitress')
        logger.setLevel(logging.INFO)

    serve(app, port=int(config['port']),  host='0.0.0.0')


@me.command('dev')
@with_appcontext
def run_dev():
    app_run(True, 'development')
    # return
    # if os.name == 'posix':
    #     subprocess.run(['./start.sh'])
    # elif os.name == 'nt':
    #     subprocess.run(['./start.cmd'])
    # else:
    #     raise new Exception('no valid os name')


@me.command('prod')
@with_appcontext
def run_prod():
    app_run(False, 'production')


@me.command('run', short_help='Run the Flask server. With no arguments, run the wsgi server.')
@click.option("--wsgi/--no-wsgi", "-w", default=False, help="The WSGI server.", show_default=True)
@click.option("--dev/--no-dev", "-d", default=False, help="The development server in development mode.", show_default=True)
@click.option("--prod/--no-prod", "-p", default=False, help="The development server in production mode.", show_default=True)
@with_appcontext
def run_me(wsgi, dev, prod):
    if wsgi:
        run()
    elif dev:
        run_dev()
    elif prod:
        run_prod()
    else:
        run()


@me.command('translate', short_help='Translate the app.')
@click.option("--name",
              "-n",
              default="messages",
              help=('File name for text messages export (default "messages")'
                    ))
def trans(name):
    name = name + ".pot"
    path = app.instance_path.removesuffix('instance')
    path = path + app.name.removesuffix('.app')
    echo(path)
    if os.path.exists(path):
        echo('path exists')
        cmd = f'pybabel extract -F babel.cfg -k lazy_gettext -o "{name}" "{path}"'
        # echo(cmd)
        # sys.exit(0)
        subprocess.run(cmd)


@me.command('test', short_help='Run pytest')
@click.argument('name',
                default=""
                )
@click.option('-o', '--option', is_flag=True, default=False, is_eager=True)
@click.argument('value', nargs=-1)
@click.pass_context
def test(ctx, name, option, value):
    # print('ctx',ctx)
    # print('args', click.get_os_args())
    # print('current_context', click.get_current_context())
    from ..tests.config import ROOT_DIR_TEST, SQL_DB_PATH_TEST
    os.environ.setdefault('FLASK_ENV', 'testing')
    # print(os.environ.items())
    if option and value != ():
        run_tests(name, option)
    else:
        run_tests(name)
    cli_clear(ROOT_DIR_TEST, SQL_DB_PATH_TEST)


@me.command('clear-test', short_help='Clear pytest files')
def test_clear():
    from ..tests.config import ROOT_DIR_TEST, SQL_DB_PATH_TEST
    cli_clear(ROOT_DIR_TEST, SQL_DB_PATH_TEST)


@me.command('clear', short_help='Clear')
@click.option("--test/--no-test",
              "-t",
              default=None,
              help=('Delete all trash test files'
                    ))
@click.option("--pycache/--no-pycache",
              "-p",
              default=None,
              help=('Delete all pycache files'
                    ))
def clear(test, pycache):
    from ..tests.config import ROOT_DIR_TEST
    if pycache is not None:
        print(sys.pycache_prefix, app.root_path, sysconfig.get_config_vars(), sysconfig.get_python_lib(), sys.executable)
        print(sys.executable.replace(app.root_path.lower(), ''))
        print(app.root_path.lower() in sys.executable)
        clear_pycache(app.root_path)


# click.group(cls=FlaskGroup, run=run)
app.cli.add_command(cmd=init)
app.cli.add_command(create_database_cmd)
app.cli.add_command(init)
app.cli.add_command(trans)
app.cli.add_command(test)
