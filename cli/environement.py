import secrets
from typing import List
import click
from click import echo


class ConfigureEnv(object):
    DEFAULT_GENERATED_FILE_NAME = 'env.init.py'
    SERVER_NAMES: list[str] = sorted(
        ['SqlServer', 'MySql', 'MariaDB', 'Sqlite', 'PostGresql', '.exit'])
    DEFAULT_DATABASE_NAME = 'flask_users'
    DEFAULT_PORT = 5000
    DEFAULT_WSGI_HOST = '0.0.0.0'
    DEFAULT_WSGI_PORT = 80
    ENV_FILE_TEMPLATE = \
        """from .app import app
from .config import Configuration as BaseConfiguration


class Configuration(BaseConfiguration):
    \"""Override default Flask configuration parameters

    Args:
        BaseConfiguration(MetaFlaskEnv): Flask Configuration variables
        This class has access to the Flask app(already initialized)
    \"""

    # DEBUG = True
    PORT = 5000
    $$HOST$$
    SECRET_KEY = $$SECRET_KEY$$
    SERVER_WSGI = {'host': '$$HOST$$', 'port': $$PORT$$}  # serve() options
    SERVER_OPTIONS = {'log': True}  # log: logging enabled?

    if app.debug == True:
        # Enable Debug toolbar
        DEBUG_TB_ENABLED = True

    """

    def __init__(self, file_name: str = '') -> None:
        self.database_name = self.DEFAULT_DATABASE_NAME  # the name of the database (default to 'flask_users')
        self.servers_list: List[str] = []  # all the selected servers
        self.port = self.DEFAULT_PORT
        self.wsgi_host = self.DEFAULT_WSGI_HOST
        self.wsgi_port = self.DEFAULT_WSGI_PORT
        self.env_contents = ''
        self.file_name = file_name if file_name else self.DEFAULT_GENERATED_FILE_NAME
        self.generate_secret_key()

    def generate_secret_key(self):
        # Autogenerate secret key
        self.secret_key = secrets.token_hex()
        return self.secret_key

    def configure_env(self) -> str:
        SERVER_WSGI = {'host': self.wsgi_host, 'port': self.wsgi_port}  # serve() options
        SERVER_OPTIONS = {'log': True}  # log: logging enabled?

        env_file = self.ENV_FILE_TEMPLATE.replace('$$PORT$$', str(self.port))
        env_file = env_file.replace('$$SECRET_KEY$$', self.secret_key)
        env_file = env_file.replace('$$HOST$$', str(self.wsgi_host))
        self.env_contents = env_file
        return env_file

    def input_database(self):
        # try:
        # Database name
        self.database_name = click.prompt(
            'Database name', self.DEFAULT_DATABASE_NAME, confirmation_prompt=True, show_default=True)
        click.echo(self.database_name)

    def choose_server(self) -> List[str]:
        servers_choice: list[dict] = []
        self.servers_list: list[str] = []
        while True:
            choices = click.Choice(choices=[i.lower() for i in self.SERVER_NAMES], case_sensitive=False)
            server_value = click.prompt('Choose server type (.exit to quit): ', None, show_choices=True,
                                        type=choices, confirmation_prompt=True)
            print(server_value)
            s = ''
            if server_value.lower() in ['mysql', 'mariadb']:
                host = click.prompt('db host: ', '127.0.0.1')
                port = click.prompt('db port: ', 3036)
                username = click.prompt('db user: ', 'root')
                password = click.prompt('db password: ', 'root', hide_input=True)
                charset = click.prompt('db charset: ', 'utf8mb4')
                servers_choice.append({'server': server_value, 'host': host, 'port': port,
                                       'username': username, 'password': password})
                driver = 'mysql+pymysql://'
                if server_value.lower() == 'mariadb':
                    driver = 'mariadb+pymysql://'
                s = f'{driver}{username}:{password}@{host}:{port}/{self.database_name}?charset={charset}'

            elif server_value.lower() in ['postgresql']:
                host = click.prompt('db host: ', 'localhost')
                port = click.prompt('db port: ', '')
                username = click.prompt('db user: ', 'postgres')
                password = click.prompt(
                    'db password: ', 'postgres', hide_input=True)
                servers_choice.append({'server': server_value, 'host': host, 'port': port,
                                       'username': username, 'password': password})
                s = DB_URI = f'postgresql+psycopg2://{username}:{password}@{host}/{self.database_name}'

            elif server_value.lower() in ['sqlserver']:
                # '?odbc_connect=DRIVER={ODBC Driver 17 for SQL Server};SERVER=(localdb)\MSSQLLocalDB;DATABASE=flask_users;'
                host = click.prompt('db host: ', """(localdb)\\MSSQLLocalDB""")
                username = click.prompt('db user: ', '')
                password = click.prompt('db password: ', '', hide_input=True)
                servers_choice.append({'server': server_value, 'host': host,  # 'port': port,
                                       'username': username, 'password': password})
                driver = '{ODBC Driver 17 for SQL Server}'
                s = f"DRIVER={driver};SERVER={driver};DATABASE={self.database_name}"
                if username:
                    s += ';UID='+username
                if password:
                    if s.endswith(';'):
                        s += 'PWD='+password
                    else:
                        s += ';PWD='+password
            elif server_value.lower() in ['sqlite']:
                path: str = click.prompt('db path: ', '', hide_input=True)
                servers_choice.append({'server': server_value, 'path': path})
                if path != '':
                    path = path + '/'
                s = DB_URI = f'sqlite+pysqlite:///{path}{self.database_name}'
            elif server_value.lower() == '.exit':
                if click.confirm('Do you want to exit the database selection(y/n)?', True):
                    break
            else:
                continue
            s = f"#{server_value}\n{s}\n\n"
            self.servers_list.append(s)
        # servers: set = set(servers_choice)
        return self.servers_list

    def input_port(self):
        # Port
        self.port = click.prompt('Port', 5000, confirmation_prompt=True,
                                 show_default=True, type=int)
        click.echo(self.port)

    def input_wsgi(self):
        # WSGI
        self.wsgi_host = click.prompt('WSGI Host', self.DEFAULT_WSGI_HOST,
                                      confirmation_prompt=True, show_default=True, type=str)
        self.wsgi_port = click.prompt('WSGI Port', self.DEFAULT_WSGI_PORT,
                                      confirmation_prompt=True, show_default=True, type=int)

    def write_to_file(self):
        echo(self.env_contents)
        with open(self.file_name, 'w', encoding='utf8', newline="\n") as f:
            f.write(self.env_contents)
            click.echo(
                'Successfully written environment configuration to "env.init.py".\nPlease rename the file to ".env" before using the app.')


def generate_env(filename: str = '') -> None:
    """generate_env Generate environment file
    """
    env = ConfigureEnv(filename)
    env.input_database()
    env.choose_server()
    env.input_port()
    env.input_wsgi()
    env.configure_env()
    env.write_to_file()
