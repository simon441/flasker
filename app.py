import logging
import os

from flask import Flask, globals, session
from flask_babel_js import BabelJS
from flask_babelplus import Babel, lazy_gettext
from flask_babel import Babel as Babel2
from flask_ckeditor import CKEditor
from flask_login import LOGIN_MESSAGE, LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_webtest import get_scopefunc

# from .external import UPLOAD_FOLDER

from .database import get_db

u = os.environ.get('test') is None
u = True

# Create a Flask Instance
app = Flask(__name__)
print('u', u)
# if u:
#     import click
#     from flask.cli import with_appcontext
#     from click import echo

#     @app.cli.command('create-database')
#     @with_appcontext
#     def create_database(name):
#         # try:
#         value = click.prompt('Database name', 'flask_users', confirmation_prompt=True, show_default=True)
#         echo(value)
#         # except Abort:
#         #     sys.exit(1)

#     app.cli.add_command(create_database)

# Import Configuration
if u:
    from .env import Configuration

    # Configuration from env.py
    app.config.from_object(Configuration)
    app.config['SQLALCHEMY_DATABASE_URI'] = app.config['DB_URI']
    if os.environ.get('SQL_DATABASE_ALCHEMY'):
        print("os.environ.get('SQL_DATABASE_ALCHEMY')", os.environ.get('SQL_DATABASE_ALCHEMY'))
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQL_DATABASE_ALCHEMY')

# else:
#     app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQL_DATABASE_ALCHEMY')

# # Upload files configuration
# app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# # maximum allowed payload to 16 megabytes else raise a RequestEntityTooLarge
# app.config['MAX_CONTENT_LENGTH'] = 16 * 1000 * 1000
# # app.config['SESSION_COOKIE_SAMESITE'] = True
# # app.config['SESSION_COOKIE_SECURE'] = True

# # Disable Flask Event System
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Add Debug tool bar
if app.debug == True:
    # from werkzeug.middleware.profiler import ProfilerMiddleware
    # app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[5], profile_dir='./profile')
    from flask_debugtoolbar import DebugToolbarExtension
    from flask_sqlalchemy import get_debug_queries

    # app.config['SQLALCHEMY_RECORD_QUERIES'] = True

    @app.after_request
    def after_request(response):
        for query in get_debug_queries():
            if query.duration >= 0:
                print(query.statement, query.parameters, query.duration, query.context)
        return response

    # activate toolbar
    toolbar = DebugToolbarExtension(app)

    # log to console
    logger = logging.basicConfig(level=logging.DEBUG)

# Load functions
if u:
    from .utils import get_lang

# Get the language when the Request is initialized
    app.before_request(get_lang)
    print('lang')

# CKEditor
ckeditor = CKEditor(app)

# # Add Database
# # SQlite DB
# # app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.sqlite3'
# # MySQL
# # engine = create_engine("mysql+pymsql://user:pass@some_host/dbname?charset=utf8mb4")
# app.config['SQLALCHEMY_DATABASE_URI'] = app.config['DB_URI']

# Add Flask secret key (done in the Configuration class in env.py)

# Initialize the Database

# session_options = {}
# if app.testing:
#     app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQL_DATABASE_ALCHEMY')
#     session_options['scopefunc'] = get_scopefunc()
# db = SQLAlchemy(app, session_options=session_options)
# if app.config['SQLALCHEMY_DATABASE_URI'] == ''.startswith('sqlite'):
#     # Batch mode for sqlite3
#     migrate = Migrate(app, db, render_as_batch=True)
# else:
#     migrate = Migrate(app, db, render_as_batch=True)

db = get_db(app)

# Flask_Login Management initialization
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'danger'
login_manager.login_message = lazy_gettext(LOGIN_MESSAGE)


@login_manager.user_loader
def load_user(user_id):
    """ Load user when we log in """
    return Users.query.get(int(user_id))


# Babel
babel = Babel(app)
# BabelJS
babelJS = BabelJS(app)
# babel2 = Babel2(app)

app.config['WTF_I18N_ENABLED'] = False


@babel.localeselector
def get_locale():
    # if a user is logged in, use the locale from the user settings
    if session:
        if session.get('lang') in ['en', 'fr']:
            return session.get('lang')
    user = getattr(globals, 'user', None)
    if user is not None:
        session['lang'] = user.locale
        return user.locale
    # otherwise try to guess the language from the user accept
    # header the browser transmits.  We support de/fr/en in this
    # example.  The best match wins.
    best_language_match = request.accept_languages.best_match(['fr', 'en'])
    session['lang'] = best_language_match

    return best_language_match


@babel.timezoneselector
def get_timezone():
    user = getattr(globals, 'user', None)
    if user is not None:
        return user.timezone


######### jinja #####
if u:
    from .utils.jinja_helpers import *

############# Models ############
    from .models import Users

############# Routes ############

    from .routes import *

# if __name__ == "__main__":
    # app.debug = True
    # app.run()
    # serve(app, host='flasker.nyanya', port=80)
