from .utils.external import UPLOAD_FOLDER
from .app import app
from flask_env import MetaFlaskEnv


class BaseConfiguration(MetaFlaskEnv):
    """App configuration class regrouping all App Configuration
    Modify it or add to it with your values

    Args:
        BaseConfiguration (flask.config.Config): Configuration variables in the env (not shown)
    """

    UPLOAD_FOLDER = UPLOAD_FOLDER
    MAX_CONTENT_LENGTH = 16 * 1000 * 1000
    # SESSION_COOKIE_SAMESITE = True
    # SESSION_COOKIE_SECURE = True

    # Disable Flask Event System
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ROWS_PER_PAGE = 1000

    # @property
    # def SQLALCHEMY_DATABASE_URI(self):
    #     return self.DB_URI
    # SQLALCHEMY_DATABASE_URI = app.config.get('DB_URI')

    # Add Debug tool bar
    if app.debug == True:
        # from werkzeug.middleware.profiler import ProfilerMiddleware
        # app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[5], profile_dir='./profile')

        # Query recording
        SQLALCHEMY_RECORD_QUERIES = True
        # SQLAlchemy will log all the statements issued to stderr
        SQLALCHEMY_ECHO = True

        # Enable Debug toolbar
        DEBUG_TB_ENABLED = True
